package com.india.flipclip.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * ARPITA AKBARI
 **/

public class DatabaseHandler extends SQLiteOpenHelper {
    private static String DBNAME = "FlipClip";
    private static final String DBTABLE = "dbtable";
    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "_video";
    private static int VERSION = 1;
    private SQLiteDatabase db = getWritableDatabase();

    public DatabaseHandler(Context context) {
        super(context, DBNAME, null, VERSION);
    }

    public boolean CheckIsDataAlreadyInDBorNot(String fieldValue) {
        String Query = "Select * from " + DBTABLE + " where " + KEY_NAME + " = " + fieldValue;
        Cursor cursor = db.rawQuery(Query, null);
        if (cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public boolean isExists(String path) {
        boolean result = false;
        db = getReadableDatabase();
        try {
            Cursor cursor = db.query(DBNAME, null, 1 + " = ? ", new String[]{path}, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
//                int isFav = cursor.getInt(cursor.getColumnIndex(DbTableColumn.MEDIA_MODE));
                String paths = cursor.getString(cursor.getColumnIndex(String.valueOf(1)));
                //Log.d(TAG, "value -->" + String.valueOf(isFav));
                //Log.d(TAG, "path -- > "+paths);
                result = paths != null ;
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
        return result;
    }

    public void deleteEntry(String str) {
        SQLiteDatabase sQLiteDatabase = this.db;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DELETE FROM dbtable WHERE _video= '");
        stringBuilder.append(str);
        stringBuilder.append("'");
        Log.e("DELETE",stringBuilder.toString());
        sQLiteDatabase.execSQL(stringBuilder.toString());
    }

    public boolean deleteTitle(String name)
    {
        return db.delete(DBTABLE, KEY_NAME + "=" + name, null) > 0;
    }

    public int getContactsCount() {
        Cursor rawQuery = getReadableDatabase().rawQuery("SELECT  * FROM dbtable", null);
        rawQuery.close();
        return rawQuery.getCount();
    }

    public void insertData(VideoLikeModel rowItems) {
        SQLiteDatabase writableDatabase = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_NAME, rowItems.getVideoid());
//        contentValues.put(KEY_NUMBER, rowItems.getNumber());
//        contentValues.put(KEY_IMAGE, rowItems.getPath());
//        contentValues.put(KEY_VIDEO_PATH, rowItems.getVideoPath());
        writableDatabase.insert(DBTABLE, null, contentValues);
        writableDatabase.close();
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        db.disableWriteAheadLogging();
    }

    public List<VideoLikeModel> loadAll() {
        ArrayList arrayList = new ArrayList();
        Cursor rawQuery = getWritableDatabase().rawQuery("SELECT  * FROM dbtable", null);
        if (rawQuery.moveToFirst()) {
            do {
                VideoLikeModel rowItems = new VideoLikeModel("");
                rowItems.setVideoid(rawQuery.getString(1));
//                rowItems.setNumber(rawQuery.getString(2));
//                rowItems.setPath(rawQuery.getString(3));
//                rowItems.setVideoPath(rawQuery.getString(4));
                arrayList.add(rowItems);
            } while (rawQuery.moveToNext());
        }
        return arrayList;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE dbtable(_id INTEGER PRIMARY KEY,_video TEXT)");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS dbtable");
        onCreate(sQLiteDatabase);
    }

    public boolean updateByPrimaryKey(int i, String str, String str2, byte[] bArr, String str3) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_NAME, str);
//        contentValues.put(KEY_NUMBER, str2);
//        contentValues.put(KEY_IMAGE, bArr);
//        contentValues.put(KEY_VIDEO_PATH, str3);
        SQLiteDatabase sQLiteDatabase = this.db;
        str2 = DBTABLE;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("_id=");
        stringBuilder.append(i);
        sQLiteDatabase.update(str2, contentValues, stringBuilder.toString(), null);
        return true;
    }

}
