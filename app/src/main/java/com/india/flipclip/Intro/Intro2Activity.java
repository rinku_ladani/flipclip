package com.india.flipclip.Intro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.india.flipclip.Model.IntroDataModel;
import com.india.flipclip.Model.IntroDataModel1;
import com.india.flipclip.R;
import com.india.flipclip.Splash.SplashActivity;
import com.india.flipclip.Util.AppPrefs;

import java.util.ArrayList;

public class Intro2Activity extends AppCompatActivity {

    ImageView img_skip;
    RelativeLayout img_bgmain;
    AppPrefs appPrefs;
    int flag = 0;
    ArrayList<IntroDataModel1> introDataModels1 = new ArrayList<>();
    IntroDataAdapter1 introDataAdapter1;
    RecyclerView recyclist_lan;
    ImageView img_continue1;
    private boolean val;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro2);

        appPrefs = new AppPrefs(this);


        if (appPrefs.getisFirstTime().equalsIgnoreCase("TRUE")) {
            startActivity(new Intent(this, SplashActivity.class));
            finish();
            return;
        }
        flag = 1;

        img_continue1 = findViewById(R.id.img_continue1);
        recyclist_lan = findViewById(R.id.recyclist_lan);
        img_skip = findViewById(R.id.img_skip);
        img_bgmain = findViewById(R.id.img_bgmain);
        recyclist_lan.setHasFixedSize(true);
        recyclist_lan.setLayoutManager(new GridLayoutManager(this, 2));

        introDataModels1.add(new IntroDataModel1(R.drawable.l_1));
        introDataModels1.add(new IntroDataModel1(R.drawable.l_2));
        introDataModels1.add(new IntroDataModel1(R.drawable.l_3));
        introDataModels1.add(new IntroDataModel1(R.drawable.l_4));
        introDataModels1.add(new IntroDataModel1(R.drawable.l_5));
        introDataModels1.add(new IntroDataModel1(R.drawable.l_6));
        introDataModels1.add(new IntroDataModel1(R.drawable.l_7));
        introDataModels1.add(new IntroDataModel1(R.drawable.l_8));
        introDataModels1.add(new IntroDataModel1(R.drawable.l_11));
        introDataModels1.add(new IntroDataModel1(R.drawable.l_10));

        introDataAdapter1 = new IntroDataAdapter1(this, introDataModels1, new IntroDataAdapter1.OnItemClickListener() {
            @Override
            public void onItemClick(IntroDataModel1 item, int position, View view) {
                ImageView img_check = view.findViewById(R.id.img_check);

                if (img_check.getVisibility() == View.VISIBLE) {
                    img_check.setVisibility(View.GONE);
                    val = false;
                    img_continue1.setBackground(getResources().getDrawable(R.drawable.intro_continue_light));
                } else {
                    val = true;
                    img_check.setVisibility(View.VISIBLE);
                    img_continue1.setBackground(getResources().getDrawable(R.drawable.intro_continue_dark));
                }

            }
        });
        recyclist_lan.setAdapter(introDataAdapter1);

        img_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intro2Activity.this, SplashActivity.class));
                appPrefs.setisFirstTime("TRUE");
                finish();
            }
        });

        img_continue1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (val) {
                    startActivity(new Intent(Intro2Activity.this, SplashActivity.class));
                    appPrefs.setisFirstTime("TRUE");
                    finish();
                } else {
                    Toast.makeText(Intro2Activity.this, "Please select language", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}

