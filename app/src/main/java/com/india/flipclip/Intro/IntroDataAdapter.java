package com.india.flipclip.Intro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.india.flipclip.Model.IntroDataModel;
import com.india.flipclip.R;

import java.util.ArrayList;

/**
 * ARPITA AKBARI
 **/

public class IntroDataAdapter extends RecyclerView.Adapter<IntroDataAdapter.MyHolder> {

    OnItemClickListener listener1;
    Context context;
    ArrayList<IntroDataModel> dataModels;

    public IntroDataAdapter(Context context, ArrayList<IntroDataModel> dataModels, OnItemClickListener listener) {
        this.context = context;
        this.dataModels = dataModels;
        listener1 = listener;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.intro_item, parent, false);
        return new MyHolder(view);
    }

    public interface OnItemClickListener {
        void onItemClick(IntroDataModel item, int position, View view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {

        IntroDataModel introDataModel = dataModels.get(position);

        holder.img_get.setBackgroundResource(introDataModel.getImage());

        holder.img_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener1.onItemClick(introDataModel,position,view);

//                introDataModel.setSelected(!introDataModel.isSelected());
//                if (holder.img_check.getVisibility() == View.VISIBLE) {
//                    holder.img_check.setVisibility(View.GONE);
//                } else {
//                    holder.img_check.setVisibility(View.VISIBLE);
//                }
//                holder.img_check.setImageResource(introDataModel.isSelected() ? Color.CYAN : Color.WHITE);

            }
        });
    }


    @Override
    public int getItemCount() {
        return dataModels.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        RelativeLayout img_get;
        RelativeLayout img_click;
        ImageView img_check;

        public MyHolder(@NonNull View itemView) {
            super(itemView);

            img_get = itemView.findViewById(R.id.img_get);
            img_click = itemView.findViewById(R.id.img_click);
            img_check = itemView.findViewById(R.id.img_check);
        }
    }
}
