package com.india.flipclip;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.india.flipclip.Home.Botttom.Chat.InboxFragment;
import com.india.flipclip.Home.Botttom.Home.HomeFragment;
import com.india.flipclip.Home.Botttom.Login.ProfileFragment;
import com.india.flipclip.Home.Botttom.Search.SearchFragment;
import com.india.flipclip.Home.Botttom.VideoUpload.VideoUploadFragment;
import com.india.flipclip.Home.Botttom.VideoUpload.VideoUploadingActivity;
import com.india.flipclip.Update.GooglePlayStoreAppVersionNameLoader;
import com.india.flipclip.Update.WSCallerVersionListener;
import com.india.flipclip.Util.AppPrefs;
import com.india.flipclip.Util.PrefManager;

import java.io.File;

public class DemoActivity extends AppCompatActivity implements WSCallerVersionListener {

    private boolean isForceUpdate = true;
    private boolean isPermitted;
    public static File folder;

    AppPrefs appPrefs;
    PrefManager prefManager;

    LinearLayout liner_home, liner_inbox, liner_profile, liner_search, img_videocreation;
    FrameLayout frame_main;
    private boolean checkpre;

    @Override
    protected void onStart() {
        super.onStart();

        appPrefs = new AppPrefs(this);
        new GooglePlayStoreAppVersionNameLoader(getApplicationContext(), DemoActivity.this).execute();
        checkpre = checkRunTimePermission();

        prefManager = new PrefManager(this);
        folder = new File(Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.app_name));
        boolean success = true;
        if (!folder.exists()) {
            //Toast.makeText(MainActivity.this, "Directory Does Not Exist, Create It", Toast.LENGTH_SHORT).show();
            success = folder.mkdir();
        }
        if (success) {
            //Toast.makeText(MainActivity.this, "Directory Created", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(MainActivity.this, "Failed - Error", Toast.LENGTH_SHORT).show();
        }

        if (folder.isDirectory()) {
            File[] contents = folder.listFiles();
            if (contents == null) {
                Log.e("If", "if");
            } else if (contents.length == 0) {
                Log.e("Else", "elif");
            } else {
                String[] children = folder.list();
                for (int i = 0; i < children.length; i++) {
                    new File(folder, children[i]).delete();
                }
            }
        }


        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.replace(R.id.framelayout, new HomeFragment());
        beginTransaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);



        img_videocreation = findViewById(R.id.img_videocreation);
        liner_search = findViewById(R.id.liner_search);
        liner_profile = findViewById(R.id.liner_profile);
        liner_inbox = findViewById(R.id.liner_inbox);
        liner_home = findViewById(R.id.liner_home);
        frame_main = findViewById(R.id.framelayout);

        img_videocreation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkpre && !prefManager.getUserName().isEmpty()) {
                    Intent intent = new Intent(DemoActivity.this, VideoUploadingActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(DemoActivity.this, "Please Check Login User", Toast.LENGTH_SHORT).show();
                }

            }
        });

        liner_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction beginTransaction3 = getSupportFragmentManager().beginTransaction();
                beginTransaction3.replace(R.id.framelayout, new ProfileFragment());
                beginTransaction3.commit();
            }
        });

        liner_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction beginTransaction1 = getSupportFragmentManager().beginTransaction();
                beginTransaction1.replace(R.id.framelayout, new SearchFragment());
                beginTransaction1.commit();
//                }
            }
        });

        liner_inbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction beginTransaction4 = getSupportFragmentManager().beginTransaction();
                beginTransaction4.replace(R.id.framelayout, new InboxFragment());
                beginTransaction4.commit();
            }
        });

        liner_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
                beginTransaction.replace(R.id.framelayout, new HomeFragment());
                beginTransaction.commit();
            }
        });
    }

    @Override
    public void onGetResponse(boolean isUpdateAvailable) {
        Log.e("ResultAPPMAIN", String.valueOf(isUpdateAvailable));
        if (isUpdateAvailable) {
            showUpdateDialog();
        }
    }

    public Boolean checkRunTimePermission() {
        String[] permissionArrays = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_PHONE_STATE, Manifest.permission.MODIFY_AUDIO_SETTINGS, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissionArrays, 11111);
            return true;
        } else {
            return false;
            // if already permition granted
            // PUT YOUR ACTION (Like Open cemara etc..)
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean openActivityOnce = true;
        boolean openDialogOnce = true;
        if (requestCode == 11111) {
            for (int i = 0; i < grantResults.length; i++) {
                String permission = permissions[i];

                isPermitted = grantResults[i] == PackageManager.PERMISSION_GRANTED;

                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    // user rejected the permission
                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {
                        //execute when 'never Ask Again' tick and permission dialog not show
                    } else {
                        if (openDialogOnce) {
                            alertView();
                        }
                    }
                }
            }

            if (isPermitted) {

            }
//                if (isPermissionFromGallery)
//                    openGalleryFragment();
        }
    }

    private void alertView() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getApplicationContext(), R.style.MyAlertDialogStyle);

        dialog.setTitle("Permission Denied")
                .setInverseBackgroundForced(true)
                //.setIcon(R.drawable.ic_info_black_24dp)
                .setMessage("Without those permission the app is unable to save your profile. App needs to save profile image in your external storage and also need to get profile image from camera or external storage.Are you sure you want to deny this permission?")

                .setNegativeButton("I'M SURE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                    }
                })
                .setPositiveButton("RE-TRY", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                        checkRunTimePermission();

                    }
                }).show();
    }


    public void showUpdateDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle(getString(R.string.app_name));
        alertDialogBuilder.setMessage(getString(R.string.update_message));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(R.string.update_now, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                dialog.cancel();
            }
        });
        alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (isForceUpdate) {
                    finish();
                }
                dialog.dismiss();
            }
        });
        alertDialogBuilder.show();
    }

}

