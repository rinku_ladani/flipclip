package com.india.flipclip.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Example {

    @SerializedName("followers_count")
    @Expose
    private int followersCount;
    @SerializedName("following_count")
    @Expose
    private int followingCount;
    @SerializedName("like_count")
    @Expose
    private int likeCount;
    @SerializedName("videocountlike")
    @Expose
    private int videocountlike;
    @SerializedName("videocount")
    @Expose
    private int videocount;
    @SerializedName("customer")
    @Expose
    private ArrayList<Customer> customer = null;
    @SerializedName("success")
    @Expose
    private Integer success;

    public int getFollowingCount() {
        return followingCount;
    }

    public void setFollowingCount(int followingCount) {
        this.followingCount = followingCount;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }


    public int getVideocountlike() {
        return videocountlike;
    }

    public void setVideocountlike(int videocountlike) {
        this.videocountlike = videocountlike;
    }

    public int getVideocount() {
        return videocount;
    }

    public void setVideocount(int videocount) {
        this.videocount = videocount;
    }

    public ArrayList<Customer> getCustomer() {
        return customer;
    }

    public void setCustomer(ArrayList<Customer> customer) {
        this.customer = customer;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public int getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(int followersCount) {
        this.followersCount = followersCount;
    }
}
