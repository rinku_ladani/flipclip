package com.india.flipclip.Model;

/**
 * ARPITA AKBARI
 **/

public class IntroDataModel {

    int image;
    private boolean isSelected = false;

    public IntroDataModel(int image1) {
        image = image1;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


    public boolean isSelected() {
        return isSelected;
    }
}

