package com.india.flipclip.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Customer implements Serializable, Parcelable {

    Boolean isFavourite;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("profile")
    @Expose
    private String profile;
    @SerializedName("user_videolist_id")
    @Expose
    private String userVideolistId;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("likes")
    @Expose
    private String likes;
    @SerializedName("videoid")
    @Expose
    private String videoid;
    @SerializedName("uname")
    @Expose
    private String uname;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_registation_id")
    @Expose
    private String userRegistationId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("emailid")
    @Expose
    private String emailid;
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("videolink")
    @Expose
    private String videolink;

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public Boolean IsFavourite() {
        return isFavourite;
    }

    public void setFavourite(Boolean favourite) {
        isFavourite = favourite;
    }

    protected Customer(Parcel in) {
        profilePic = in.readString();
        comment = in.readString();
        likes = in.readString();
        videoid = in.readString();
        uname = in.readString();
        id = in.readString();
        userRegistationId = in.readString();
        username = in.readString();
        emailid = in.readString();
        response = in.readString();
        description = in.readString();
        videolink = in.readString();
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserVideolistId() {
        return userVideolistId;
    }

    public void setUserVideolistId(String userVideolistId) {
        this.userVideolistId = userVideolistId;
    }

    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }


    public String getVideoid() {
        return videoid;
    }

    public void setVideoid(String videoid) {
        this.videoid = videoid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserRegistationId() {
        return userRegistationId;
    }

    public void setUserRegistationId(String userRegistationId) {
        this.userRegistationId = userRegistationId;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideolink() {
        return videolink;
    }

    public void setVideolink(String videolink) {
        this.videolink = videolink;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(profilePic);
        parcel.writeString(comment);
        parcel.writeString(likes);
        parcel.writeString(videoid);
        parcel.writeString(uname);
        parcel.writeString(id);
        parcel.writeString(userRegistationId);
        parcel.writeString(username);
        parcel.writeString(emailid);
        parcel.writeString(response);
        parcel.writeString(description);
        parcel.writeString(videolink);
    }
}
