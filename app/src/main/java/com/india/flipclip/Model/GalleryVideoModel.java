package com.india.flipclip.Model;

/**
 * ARPITA AKBARI
 **/

public class GalleryVideoModel {
    String thumb_path;
    long video_duration_ms;
    String video_path;
    String video_time;

    public String getThumb_path() {
        return thumb_path;
    }

    public void setThumb_path(String thumb_path) {
        this.thumb_path = thumb_path;
    }

    public long getVideo_duration_ms() {
        return video_duration_ms;
    }

    public void setVideo_duration_ms(long video_duration_ms) {
        this.video_duration_ms = video_duration_ms;
    }

    public String getVideo_path() {
        return video_path;
    }

    public void setVideo_path(String video_path) {
        this.video_path = video_path;
    }

    public String getVideo_time() {
        return video_time;
    }

    public void setVideo_time(String video_time) {
        this.video_time = video_time;
    }
}
