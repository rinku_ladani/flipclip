package com.india.flipclip.Util;

import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;

/**
 * ARPITA AKBARI
 **/

public abstract class CountDownTimerWithPause {
    private static final int MSG = 1;
    /* access modifiers changed from: private */
    public final long mCountdownInterval;
    private Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            synchronized (CountDownTimerWithPause.this) {
                long timeLeft = CountDownTimerWithPause.this.timeLeft();
                if (timeLeft <= 0) {
                    CountDownTimerWithPause.this.cancel();
                    CountDownTimerWithPause.this.onFinish();
                } else if (timeLeft < CountDownTimerWithPause.this.mCountdownInterval) {
                    sendMessageDelayed(obtainMessage(1), timeLeft);
                } else {
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    CountDownTimerWithPause.this.onTick(timeLeft);
                    long access$000 = CountDownTimerWithPause.this.mCountdownInterval - (SystemClock.elapsedRealtime() - elapsedRealtime);
                    while (access$000 < 0) {
                        access$000 += CountDownTimerWithPause.this.mCountdownInterval;
                    }
                    sendMessageDelayed(obtainMessage(1), access$000);
                }
            }
        }
    };
    private long mMillisInFuture;
    private long mPauseTimeRemaining;
    private boolean mRunAtStart;
    private long mStopTimeInFuture;
    private final long mTotalCountdown;

    public abstract void onFinish();

    public abstract void onTick(long j);

    public CountDownTimerWithPause(long j, long j2, boolean z) {
        this.mMillisInFuture = j;
        this.mTotalCountdown = this.mMillisInFuture;
        this.mCountdownInterval = j2;
        this.mRunAtStart = z;
    }

    public final void cancel() {
        this.mHandler.removeMessages(1);
    }

    public final synchronized CountDownTimerWithPause create() {
        if (this.mMillisInFuture <= 0) {
            onFinish();
        } else {
            this.mPauseTimeRemaining = this.mMillisInFuture;
        }
        if (this.mRunAtStart) {
            resume();
        }
        return this;
    }

    public void pause() {
        if (isRunning()) {
            this.mPauseTimeRemaining = timeLeft();
            cancel();
        }
    }

    public void resume() {
        if (isPaused()) {
            this.mMillisInFuture = this.mPauseTimeRemaining;
            this.mStopTimeInFuture = SystemClock.elapsedRealtime() + this.mMillisInFuture;
            this.mHandler.sendMessage(this.mHandler.obtainMessage(1));
            this.mPauseTimeRemaining = 0;
        }
    }

    public boolean isPaused() {
        return this.mPauseTimeRemaining > 0;
    }

    public boolean isRunning() {
        return !isPaused();
    }

    public long timeLeft() {
        if (isPaused()) {
            return this.mPauseTimeRemaining;
        }
        long elapsedRealtime = this.mStopTimeInFuture - SystemClock.elapsedRealtime();
        if (elapsedRealtime < 0) {
            return 0;
        }
        return elapsedRealtime;
    }

    public long totalCountdown() {
        return this.mTotalCountdown;
    }

    public long timePassed() {
        return this.mTotalCountdown - timeLeft();
    }

    public boolean hasBeenStarted() {
        return this.mPauseTimeRemaining <= this.mMillisInFuture;
    }
}

