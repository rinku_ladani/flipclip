package com.india.flipclip.Util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * ARPITA AKBARI
 **/

public class AppPrefs {
    public static final String USER_PREFS = "USER PREFS";
    public SharedPreferences appSharedPref;
    public String isFirstTime = "isFirstTime";
    public SharedPreferences.Editor prefEditor;
    public String AlterNetInter = "AlterNetInter";


    public String getisFirstTime() {
        return this.appSharedPref.getString(this.isFirstTime, "");
    }

    public void setisFirstTime(String str) {
        this.prefEditor.putString(this.isFirstTime, str).commit();
    }

    public AppPrefs(Context context) {
        this.appSharedPref = context.getSharedPreferences(USER_PREFS, 0);
        this.prefEditor = this.appSharedPref.edit();
    }

    public String getAlterNetInter() {
        return this.appSharedPref.getString(this.AlterNetInter, "");
    }

    public void setAlterNetInter(String str) {
        this.prefEditor.putString(this.AlterNetInter, str).commit();
    }
}
