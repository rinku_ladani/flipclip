package com.india.flipclip.Util;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;

import java.util.ArrayList;
import java.util.List;

/**
 * ARPITA AKBARI
 **/

public class SegmentedProgressBar extends View {
    private static final int FPS_IN_MILLI = 16;
    private static final String TAG = "SegmentedProgressBar";
    private float cornerRadius;
    private CountDownTimerWithPause countDownTimerWithPause;
    private int dividerCount = 0;
    private Paint dividerPaint = new Paint();
    private List<Float> dividerPositions;
    private float dividerWidth = 1.0f;
    /* access modifiers changed from: private */
    public int[] gradientColors = new int[3];
    private boolean isDividerEnabled;
    private float lastDividerPosition;
    ProgressBarListener listener;
    /* access modifiers changed from: private */
    public long maxTimeInMillis;
    private float percentCompleted;
    /* access modifiers changed from: private */
    public int progressBarWidth;
    /* access modifiers changed from: private */
    public Paint progressPaint = new Paint();

    public SegmentedProgressBar(Context context) {
        super(context);
        init();
    }

    public SegmentedProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public SegmentedProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRoundRect(new RectF(0.0f, 0.0f, this.percentCompleted, (float) getHeight()), this.cornerRadius, this.cornerRadius, this.progressPaint);
        if (this.dividerCount > 0 && this.isDividerEnabled) {
            for (int i = 0; i < this.dividerCount; i++) {
                float floatValue = ((Float) this.dividerPositions.get(i)).floatValue();
                canvas.drawRect(floatValue, 0.0f, floatValue + this.dividerWidth, (float) getHeight(), this.dividerPaint);
            }
        }
    }

    private void init() {
        this.dividerPositions = new ArrayList();
        this.cornerRadius = 0.0f;
        ViewTreeObserver viewTreeObserver = getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    SegmentedProgressBar.this.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    SegmentedProgressBar.this.progressBarWidth = SegmentedProgressBar.this.getWidth();
                    String str = SegmentedProgressBar.TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("setShader: progressBarWidth : ");
                    sb.append(SegmentedProgressBar.this.progressBarWidth);
                    Log.d(str, sb.toString());
                    if (SegmentedProgressBar.this.gradientColors.length > 0) {
                        LinearGradient linearGradient = new LinearGradient(0.0f, 0.0f, (float) SegmentedProgressBar.this.progressBarWidth, (float) SegmentedProgressBar.this.getHeight(), SegmentedProgressBar.this.gradientColors, null, Shader.TileMode.MIRROR);
                        SegmentedProgressBar.this.progressPaint.setShader(linearGradient);
                    }
                }
            });
        }
    }

    public void SetListener(ProgressBarListener progressBarListener) {
        this.listener = progressBarListener;
    }

    /* access modifiers changed from: private */
    public void updateProgress(long j) {
        this.listener.TimeinMill(j);
        this.percentCompleted = (((float) this.progressBarWidth) * ((float) j)) / ((float) this.maxTimeInMillis);
        invalidate();
    }

    private void updateProgress(float f) {
        this.percentCompleted = ((float) this.progressBarWidth) * f;
        invalidate();
    }

    public float GetPercentComplete() {
        return this.percentCompleted;
    }

    public void pause() {
        if (this.countDownTimerWithPause == null) {
            Log.e(TAG, "pause: Auto progress is not initialized. Use \"enableAutoProgressView\" to initialize the progress bar.");
        } else {
            this.countDownTimerWithPause.pause();
        }
    }

    public void resume() {
        if (this.countDownTimerWithPause == null) {
            Log.e(TAG, "resume: Auto progress is not initialized. Use \"enableAutoProgressView\" to initialize the progress bar.");
        } else {
            this.countDownTimerWithPause.resume();
        }
    }

    public void reset() {
        this.countDownTimerWithPause.cancel();
        enableAutoProgressView(this.maxTimeInMillis);
        this.dividerPositions.removeAll(this.dividerPositions);
        this.percentCompleted = 0.0f;
        this.lastDividerPosition = 0.0f;
        this.dividerCount = 0;
        invalidate();
    }

    public void cancel() {
        if (this.countDownTimerWithPause == null) {
            Log.e(TAG, "cancel: Auto progress is not initialized. Use \"enableAutoProgressView\" to initialize the progress bar.");
        } else {
            this.countDownTimerWithPause.cancel();
        }
    }

    public void setShader(int[] iArr) {
        this.gradientColors = iArr;
        if (this.progressBarWidth > 0) {
            LinearGradient linearGradient = new LinearGradient(0.0f, 0.0f, (float) this.progressBarWidth, (float) getHeight(), iArr, null, Shader.TileMode.MIRROR);
            this.progressPaint.setShader(linearGradient);
        }
    }

    public void setProgressColor(int i) {
        this.progressPaint.setColor(i);
    }

    public void setDividerColor(int i) {
        this.dividerPaint.setColor(i);
    }

    public void setDividerWidth(float f) {
        if (f < 0.0f) {
            Log.w(TAG, "setDividerWidth: Divider width can not be negative");
        } else {
            this.dividerWidth = f;
        }
    }

    public void enableAutoProgressView(long j) {
        if (j < 0) {
            Log.w(TAG, "enableAutoProgressView: Time can not be in negative");
            return;
        }
        this.maxTimeInMillis = j;
        CountDownTimerWithPause r0 = new CountDownTimerWithPause(this.maxTimeInMillis, 16, false) {
            public void onTick(long j) {
                SegmentedProgressBar.this.updateProgress(SegmentedProgressBar.this.maxTimeInMillis - j);
            }

            public void onFinish() {
                SegmentedProgressBar.this.updateProgress(SegmentedProgressBar.this.maxTimeInMillis);
            }
        };
        this.countDownTimerWithPause = r0.create();
    }

    public void setDividerEnabled(boolean z) {
        this.isDividerEnabled = z;
    }

    public void publishProgress(float f) {
        if (f < 0.0f || f > 1.0f) {
            Log.w(TAG, "publishProgress: Progress value can only be in between 0 and 1");
        } else {
            updateProgress(f);
        }
    }

    public void addDivider() {
        if (this.lastDividerPosition != this.percentCompleted) {
            this.lastDividerPosition = this.percentCompleted;
            this.dividerCount++;
            this.dividerPositions.add(Float.valueOf(this.percentCompleted));
            invalidate();
            return;
        }
        Log.w(TAG, "addDivider: Divider already added to current position");
    }

    public void setCornerRadius(float f) {
        this.cornerRadius = f;
    }
}

