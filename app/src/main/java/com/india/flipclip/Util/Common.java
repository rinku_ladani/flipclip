package com.india.flipclip.Util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.FadingCircle;
import com.github.ybq.android.spinkit.style.Wave;
import com.india.flipclip.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Common {

    private static Toast toast;
    public static final String PREF_NAME = "Cafe";
    private static ProgressDialog progressDialog;
    private static final boolean IS_LOG = true;
    private final static Pattern emailPattern = Pattern.compile("([\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Za-z]{2,4})");
    private static Dialog dialog;
    private static ProgressBar progress_bar;

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService("input_method");
        View currentFocus = activity.getCurrentFocus();
        if (currentFocus == null) {
            currentFocus = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
    }


    public static void errorLog(String tag, String msg) {
        if (IS_LOG)
            Log.e(tag, msg);
    }

    public static void showProgressDialog(Context mContext) {
        dismissDialog();
        dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.progree_bar);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawableResource(17170445);
        dialog.show();

        progress_bar = dialog.findViewById(R.id.progress_bar);
        Sprite doubleBounce = new Wave();
        progress_bar.setIndeterminateDrawable(doubleBounce);
//        progressDialog = new ProgressDialog(mContext);
//        progressDialog.setMessage("loading...");
//        progressDialog.setCancelable(false);
//        progressDialog.show();
    }

    public static void dismissDialog() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    public static void requestPermission(Context context, String permission, int reqCode) {
        ActivityCompat.requestPermissions((Activity) context, new String[]{permission}, reqCode);
    }

    public static void showToast(Context mContext, String msg) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(mContext, msg, Toast.LENGTH_SHORT);
        toast.show();
    }


    public static boolean isConnectingToInternet(Context mContext) {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected() && networkInfo.isAvailable();
    }


    public static boolean isNotNullEditTextBox(EditText editText) {
        if (editText == null)
            return false;
        if (editText.getText().toString().trim().isEmpty())
            return false;
        if (editText.getText().length() < 0)
            return false;

        return true;
    }


    public static boolean isNotNullEditTextBox(String editText) {
        if (editText == null)
            return false;
        if (editText.trim().isEmpty())
            return false;
        if (editText.length() < 0)
            return false;

        return true;
    }

    public static boolean isValidName(String email) {
        String NAME_PATTERN = "^[A-Za-z]{3,10}$";
        Pattern pattern = Pattern.compile(NAME_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isValidateEmail(String email) {
        email = email.trim();
        // If email is empty
        if (TextUtils.isEmpty(email)) {
            return false;
        }
        Matcher mMatcher = emailPattern.matcher(email);
        return mMatcher.matches();
    }

    public static boolean isValidMobile(String mobile) {
        Pattern p = Pattern.compile("(0/91)?[7-9][0-9]{9}");
        Matcher m = p.matcher(mobile);
        return (m.find() && m.group().equals(mobile));

       /* String phoneValidationUSCandaRegex = "^\\+?\\(?[0-9]{1,3}\\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})?";
        return Pattern.matches(phoneValidationUSCandaRegex, mobile);*/
    }

    public static void Show_Alert(Context context, String str, String str2) {
        new AlertDialog.Builder(context).setTitle(str).setMessage(str2).setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show();
    }

}
