package com.india.flipclip.Util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.coremedia.iso.IsoFile;
import com.coremedia.iso.boxes.Container;
import com.google.android.exoplayer2.metadata.id3.InternalFrame;
import com.googlecode.mp4parser.FileDataSourceImpl;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AACTrackImpl;
import com.googlecode.mp4parser.authoring.tracks.CroppedTrack;
//import com.india.flipclip.Home.DemoActivity;
import com.india.flipclip.DemoActivity;
import com.india.flipclip.Glob;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

public class Merge_Video_Audio extends AsyncTask<String, String, String> {
    String audio;
    Context context;
    String output;
    ProgressDialog progressDialog;
    public Runnable runnable = new Runnable() {
        public void run() {
            Merge_Video_Audio merge_Video_Audio;
            try {
                Movie build = MovieCreator.build(Merge_Video_Audio.this.video);
                ArrayList arrayList = new ArrayList();
                for (Track track : build.getTracks()) {
                    if (!"soun".equals(track.getHandler())) {
                        arrayList.add(track);
                    }
                }
                arrayList.add(Merge_Video_Audio.this.CropAudio(Merge_Video_Audio.this.video, new AACTrackImpl(new FileDataSourceImpl(Merge_Video_Audio.this.audio))));
                build.setTracks(arrayList);
                Container build2 = new DefaultMp4Builder().build(build);
                FileChannel channel = new FileOutputStream(new File(Merge_Video_Audio.this.output)).getChannel();
                build2.writeContainer(channel);
                channel.close();
                try {
                    Merge_Video_Audio.this.progressDialog.dismiss();
                    merge_Video_Audio = Merge_Video_Audio.this;
                } catch (Exception e) {
                    Log.d("resp", e.toString());
                    merge_Video_Audio = Merge_Video_Audio.this;
                }
                merge_Video_Audio.Go_To_preview_Activity();
            } catch (IOException e2) {
                e2.printStackTrace();
                Log.d("resp", e2.toString());
            } catch (Throwable th) {
                Merge_Video_Audio.this.Go_To_preview_Activity();
                throw th;
            }
        }
    };
    String video;

    public Merge_Video_Audio(Context context2) {
        this.context = context2;
        this.progressDialog = new ProgressDialog(context2);
        this.progressDialog.setMessage("Please Wait...");
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
    }

    public String doInBackground(String... strArr) {
        try {
            this.progressDialog.show();
        } catch (Exception unused) {
        }
        this.audio = strArr[0];
        this.video = strArr[1];
        this.output = strArr[2];
        StringBuilder sb = new StringBuilder();
        sb.append(this.audio);
        sb.append(InternalFrame.ID);
        sb.append(this.video);
        sb.append("-----");
        sb.append(this.output);
        Log.d("resp", sb.toString());
        new Thread(this.runnable).start();
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String str) {
        super.onPostExecute(str);
    }

    public void Go_To_preview_Activity() {
        Intent intent = new Intent(this.context, DemoActivity.class);
        StringBuilder sb = new StringBuilder();
        sb.append(Glob.root);
        sb.append("/output2.mp4");
        intent.putExtra("path", sb.toString());
        this.context.startActivity(intent);
    }

    public Track CropAudio(String str, Track track) {
        try {
            IsoFile isoFile = new IsoFile(str);
            double duration = ((double) isoFile.getMovieBox().getMovieHeaderBox().getDuration()) / ((double) isoFile.getMovieBox().getMovieHeaderBox().getTimescale());
            int i = 0;
            long j = -1;
            long j2 = 0;
            double d = -1.0d;
            long j3 = -1;
            double d2 = 0.0d;
            while (i < track.getSampleDurations().length) {
                long j4 = track.getSampleDurations()[i];
                int i2 = (d2 > d ? 1 : (d2 == d ? 0 : -1));
                if (i2 > 0) {
                    if (d2 <= 0.0d) {
                        j3 = j2;
                    }
                }
                if (i2 > 0 && d2 <= duration) {
                    j = j2;
                }
                j2++;
                i++;
                d = d2;
                d2 = (((double) j4) / ((double) track.getTrackMetaData().getTimescale())) + d2;
            }
            CroppedTrack croppedTrack = new CroppedTrack(track, j3, j);
            return croppedTrack;
        } catch (IOException e) {
            e.printStackTrace();
            return track;
        }
    }
}
