package com.india.flipclip.Util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.india.flipclip.Model.Customer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class PrefManager {

    private static final String FAVORITES = "Product_Favorite";
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    public static final String PREF_NAME = "TikTok";

    public PrefManager(Context mContext) {
        if (pref == null)
            pref = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public void setUserData(String fName, String lName, String email, String regId, String dob, String pass, String image) {
        if (pref != null) {
            editor = pref.edit();
            if (!fName.equals(""))
                editor.putString("uName", fName);
            if (!lName.equals(""))
                editor.putString("gender", lName);
            if (!email.equals(""))
                editor.putString("email", email);
            if (!regId.equals(""))
                editor.putString("regId", regId);
            if (!dob.equals(""))
                editor.putString("dob", dob);
            if (!pass.equals(""))
                editor.putString("pass", pass);
            if (!image.equals(""))
                editor.putString("image", image);
            editor.apply();
        }
    }


    public void SetUserFav(String Videoid) {
        if (pref != null) {
            editor = pref.edit();
            if (!Videoid.equals(""))
                editor.putString("Videoid", Videoid);
            editor.apply();
        }
    }

    public void SetFollwingUser(String userid) {
        if (pref != null) {
            editor = pref.edit();
            if (!userid.equals(""))
                editor.putString("Following_id", userid);
            editor.apply();
        }
    }

    public String getFollwingUser() {
        String var = "";
        if (pref != null) {
            var = pref.getString("Following_id", "");
        }
        return var;
    }

    public void ClearUserFollwing() {
        if (pref != null) {
            editor = pref.edit();
            editor.remove("Following_id");
            editor.commit();
            editor.apply();
        }
    }


    public String getUserFav() {
        String var = "";
        if (pref != null) {
            var = pref.getString("Videoid", "");
        }
        return var;
    }


    public void ClearUserFav() {
        if (pref != null) {
            editor = pref.edit();
            editor.remove("Videoid");
            editor.commit();
            editor.apply();
        }
    }


    public void setAllUserData(String fName, String regId) {
        if (pref != null) {
            editor = pref.edit();
            if (!fName.equals(""))
                editor.putString("uName", fName);
            if (!regId.equals(""))
                editor.putString("regId", regId);
            editor.apply();
        }
    }


    public static void setvaflist(String videid, String userid) {
        if (pref != null) {
            editor = pref.edit();
            if (!videid.equals(""))
                editor.putString("videid", videid);
            if (!userid.equals(""))
                editor.putString("userid", userid);
            editor.apply();
        }
    }


    public static void deletefavitem() {
        if (pref != null) {
            editor = pref.edit();
            editor.remove("videid");
            editor.remove("userid");
            editor.apply();
        }

    }

    public static Boolean getfavlist() {
        String var = "";
        if (pref != null) {
            var = pref.getString("videid", "");
        }
        if (pref != null) {
            var = pref.getString("userid", "");
        }
        return true;
    }


    public void setUserData(Customer user) {
        if (pref != null) {
            editor = pref.edit();
            if (user.getUsername() != null && !user.getUsername().equals(""))
                editor.putString("uName", user.getUsername());

            if (user.getGender() != null && !user.getGender().equals(""))
                editor.putString("gender", user.getGender());

            if (user.getEmailid() != null && !user.getEmailid().equals(""))
                editor.putString("email", user.getEmailid());

            if (user.getId() != null && !user.getId().toString().equals(""))
                editor.putString("regId", user.getId().toString());

            if (user.getDob() != null && !user.getDob().toString().equals(""))
                editor.putString("dob", user.getDob().toString());
            editor.apply();
        }
    }

    public void clearUserDetail() {
        if (pref != null) {
            editor = pref.edit();
            editor.remove("uName");
            editor.remove("gender");
            editor.remove("email");
            editor.remove("dob");
            editor.remove("pass");
            editor.remove("image");
            editor.clear();
            editor.commit();
            editor.apply();
        }
    }

    public String getUserName() {
        String var = "";
        if (pref != null) {
            var = pref.getString("uName", "");
        }
        return var;
    }

    public String getGender() {
        String var = "";
        if (pref != null) {
            var = pref.getString("gender", "");
        }
        return var;
    }


    public String getUserEmail() {
        String var = "";
        if (pref != null) {
            var = pref.getString("email", "");
        }
        return var;
    }


    public String getRegId() {
        String var = "";
        if (pref != null) {
            var = pref.getString("regId", "");
        }
        return var;
    }

    public String getdob() {
        String var = "";
        if (pref != null) {
            var = pref.getString("dob", "");
        }
        return var;
    }

    public String getpassword() {
        String var = "";
        if (pref != null) {
            var = pref.getString("pass", "");
        }
        return var;
    }

    public String getuserpic() {
        String var = "";
        if (pref != null) {
            var = pref.getString("image", "");
        }
        return var;
    }


    public void userLogout() {
        clearUserDetail();
    }
}
