package com.india.flipclip;

import android.app.Dialog;
import android.content.Context;
import android.os.Environment;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.Wave;
import com.googlecode.mp4parser.authoring.Track;

import java.util.Arrays;

/**
 * ARPITA AKBARI
 **/

public class Glob {
    public static final String FILE_UPLOAD_URL = "http://paperpoint.co.in/Tiktok/API/user_uploadvideo.php";
    public static final String IMAGE_UPLOAD_URL = "http://paperpoint.co.in/Tiktok/API/user_signup.php";
    public static final String USERPROFILEDIT_UPLOAD_URL = "http://paperpoint.co.in/Tiktok/API/user_profile_insert.php";
    public static Dialog indeterminant_dialog;


    public static String gallery_resize_video = null;
    public static String gallery_trimed_video = null;
    public static Dialog determinant_dialog;
    public static ProgressBar determinant_progress;
    private static TextView tv_count;
    public static String root = Environment.getExternalStorageDirectory().toString();
    public static String SelectedAudio = "SelectedAudio.aac";
    public static String output_filter_file = null;
    public static String compress_path = null;
    public static String outputfile = null;
    public static String outputfile2 = null;
    public static String Selected_sound_id = "null";
    private static ProgressBar determinant_progress1;


    static {
        StringBuilder sb = new StringBuilder();
        sb.append(DemoActivity.folder);
        sb.append("/output.mp4");
        outputfile = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(DemoActivity.folder);
        sb2.append("/output2.mp4");
        outputfile2 = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(DemoActivity.folder);
        sb3.append("/");
        sb3.append(System.currentTimeMillis());
        sb3.append(".mp4");
        output_filter_file = sb3.toString();
        StringBuilder arpita = new StringBuilder();
        arpita.append(DemoActivity.folder);
        arpita.append("/output_compress.mp4");
        compress_path = arpita.toString();
        StringBuilder sb4 = new StringBuilder();
        sb4.append(DemoActivity.folder);
        sb4.append("/gallery_trimed_video.mp4");
        gallery_trimed_video = sb4.toString();
        StringBuilder sb5 = new StringBuilder();
        sb5.append(DemoActivity.folder);
        sb5.append("/gallery_resize_video.mp4");
        gallery_resize_video = sb5.toString();
        StringBuilder sb6 = new StringBuilder();
//        sb6.append(content_domain);
        sb6.append("/upload/video/");
//        video_base_url = sb6.toString();
        StringBuilder sb7 = new StringBuilder();
//        sb7.append(content_domain);
        sb7.append("/upload/thum/");
//        thum_base_url = sb7.toString();
        StringBuilder sb8 = new StringBuilder();
//        sb8.append(content_domain);
        sb8.append("/upload/gif/");
//        gif_base_url = sb8.toString();
        StringBuilder sb9 = new StringBuilder();
//        sb9.append(domain);
        sb9.append("signup");
//        SignUp = sb9.toString();
        StringBuilder sb10 = new StringBuilder();
//        sb10.append(domain);
        sb10.append("showAllVideos");
//        showAllVideos = sb10.toString();
        StringBuilder sb11 = new StringBuilder();
//        sb11.append(content_domain);
        sb11.append("/version/");
//        showAllCachedVideos = sb11.toString();
        StringBuilder sb12 = new StringBuilder();
//        sb12.append(domain);
        sb12.append("showMyAllVideos");
//        showMyAllVideos = sb12.toString();
        StringBuilder sb13 = new StringBuilder();
//        sb13.append(cdn_res_domain);
        sb13.append("showMyAllVideos");
//        showMyAllVideosCDN = sb13.toString();
        StringBuilder sb14 = new StringBuilder();
//        sb14.append(domain);
        sb14.append("likeDislikeVideo");
//        likeDislikeVideo = sb14.toString();
        StringBuilder sb15 = new StringBuilder();
//        sb15.append(domain);
        sb15.append("uploadVideoMetadata");
//        uploadVideoMetadata = sb15.toString();
        StringBuilder sb16 = new StringBuilder();
//        sb16.append(domain);
        sb16.append("updateVideoView");
//        updateVideoView = sb16.toString();
        StringBuilder sb17 = new StringBuilder();
//        sb17.append(domain);
        sb17.append("allSounds");
//        allSounds = sb17.toString();
        StringBuilder sb18 = new StringBuilder();
//        sb18.append(domain);
        sb18.append("fav_sound");
//        fav_sound = sb18.toString();
        StringBuilder sb19 = new StringBuilder();
//        sb19.append(domain);
        sb19.append("my_FavSound");
//        my_FavSound = sb19.toString();
        StringBuilder sb20 = new StringBuilder();
//        sb20.append(domain);
        sb20.append("my_liked_video");
//        my_liked_video = sb20.toString();
        StringBuilder sb21 = new StringBuilder();
//        sb21.append(domain);
        sb21.append("follow_users");
//        follow_users = sb21.toString();
        StringBuilder sb22 = new StringBuilder();
//        sb22.append(content_domain);
        sb22.append("/discover.json");
//        discover = sb22.toString();
        StringBuilder sb23 = new StringBuilder();
//        sb23.append(cdn_res_domain);
        sb23.append("showVideoComments");
//        showVideoComments = sb23.toString();
        StringBuilder sb24 = new StringBuilder();
//        sb24.append(domain);
        sb24.append("postComment");
//        postComment = sb24.toString();
        StringBuilder sb25 = new StringBuilder();
//        sb25.append(domain);
        sb25.append("edit_profile");
//        edit_profile = sb25.toString();
        StringBuilder sb26 = new StringBuilder();
//        sb26.append(domain);
        sb26.append("get_user_data");
//        get_user_data = sb26.toString();
        StringBuilder sb27 = new StringBuilder();
//        sb27.append(domain);
        sb27.append("get_followers");
//        get_followers = sb27.toString();
        StringBuilder sb28 = new StringBuilder();
//        sb28.append(domain);
        sb28.append("get_followings");
//        get_followings = sb28.toString();
        StringBuilder sb29 = new StringBuilder();
//        sb29.append(domain);
        sb29.append("SearchByHashTag");
//        SearchByHashTag = sb29.toString();
        StringBuilder sb30 = new StringBuilder();
//        sb30.append(domain);
        sb30.append("sendPushNotification");
//        sendPushNotification = sb30.toString();
        StringBuilder sb31 = new StringBuilder();
//        sb31.append(domain);
        sb31.append("uploadImage");
//        uploadImage = sb31.toString();
        StringBuilder sb32 = new StringBuilder();
//        sb32.append(domain);
        sb32.append("DeleteVideo");
//        DeleteVideo = sb32.toString();
    }


    public static void Show_indeterminent_loader(Context context, boolean z, boolean z2) {
        indeterminant_dialog = new Dialog(context);
//        indeterminant_dialog.requestWindowFeature(1);
        indeterminant_dialog.setContentView(R.layout.item_indeterminant_progress_layout);
        determinant_progress1 = (ProgressBar) indeterminant_dialog.findViewById(R.id.pbar);
        Sprite doubleBounce = new Wave();
        determinant_progress1.setIndeterminateDrawable(doubleBounce);
        //        indeterminant_dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.d_round_white_background));
        if (!z) {
            indeterminant_dialog.setCanceledOnTouchOutside(false);
        }
        if (!z2) {
            indeterminant_dialog.setCancelable(false);
        }
        indeterminant_dialog.show();
    }


    public static void Show_determinent_loader(Context context, boolean z, boolean z2) {
        determinant_dialog = new Dialog(context);
//        determinant_dialog.requestWindowFeature(1);
        determinant_dialog.setContentView(R.layout.item_determinant_progress_layout);
//        determinant_dialog.getWindow().setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.d_round_white_background));
        determinant_progress = (ProgressBar) determinant_dialog.findViewById(R.id.pbar);
        Sprite doubleBounce = new Wave();
        determinant_progress.setIndeterminateDrawable(doubleBounce);


        if (!z) {
            determinant_dialog.setCanceledOnTouchOutside(false);
        }
        if (!z2) {
            determinant_dialog.setCancelable(false);
        }
        determinant_dialog.show();
    }


    public static void Show_loading_progress(int i) {
        if (determinant_progress != null) {
            determinant_progress.setProgress(i);
//            StringBuilder stringBuilder = new StringBuilder();
//            stringBuilder.append("Please Wait");
//            stringBuilder.append(" ");
//            stringBuilder.append(s);
//            tv_count.setText(stringBuilder.toString());
        }
    }

    public static void cancel_determinent_loader() {
        if (determinant_dialog != null) {
            determinant_progress = null;
            determinant_dialog.cancel();
        }
    }

    public static double correctTimeToSyncSample(Track track, double d, boolean z) {
        double[] dArr = new double[track.getSyncSamples().length];
        int i = 0;
        double d2 = 0.0d;
        double d3 = 0.0d;
        long j = 0;
        for (long j2 : track.getSampleDurations()) {
            j++;
            if (Arrays.binarySearch(track.getSyncSamples(), j) >= 0) {
                dArr[Arrays.binarySearch(track.getSyncSamples(), j)] = d3;
            }
            d3 += ((double) j2) / ((double) track.getTrackMetaData().getTimescale());
        }
        int length = dArr.length;
        while (i < length) {
            double d4 = dArr[i];
            if (d4 > d) {
                return z ? d4 : d2;
            }
            i++;
            d2 = d4;
        }
        return dArr[dArr.length - 1];
    }

    public static void cancel_indeterminent_loader() {
        if (indeterminant_dialog != null) {
            indeterminant_dialog.cancel();
        }
    }


}
