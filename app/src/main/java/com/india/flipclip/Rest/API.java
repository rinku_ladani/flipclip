package com.india.flipclip.Rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class API {
    private static <T> T builder(Class<T> endpoint) {
        return new Retrofit.Builder()
                .baseUrl("http://paperpoint.co.in/Tiktok/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(endpoint);
    }

    public static Users user() {
        return builder(Users.class);
    }


//    public static Video video() {
//        return builder(Video.class);
//    }

}
