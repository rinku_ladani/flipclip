package com.india.flipclip.Rest;


import com.india.flipclip.Model.Example;
import com.india.flipclip.Model.VideoResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Users {



    @FormUrlEncoded
    @POST("API/user_login.php")
    Call<Example> getLoginResponse(@Field("username") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("API/userwise_videoget.php")
    Call<Example> getUserWisevideoget(@Field("id") String userid);

    @FormUrlEncoded
    @POST("API/user_following.php")
    Call<Example> UserFollwing(@Field("ulogin_id") String ulogin_id, @Field("ufollowing_id") String folloeing_id);

    @FormUrlEncoded
    @POST("API/user_following_delete.php")
    Call<Example> UserFollwingDelete(@Field("ulogin_id") String ulogin_id, @Field("ufollowing_id") String folloeing_id);


    @GET("API/all_videoget.php")
    Call<Example> getAllVideo();

    @FormUrlEncoded
    @POST("API/user_dislikedelete.php")
    Call<Example> userdislike_video(@Field("user_id") String userid, @Field("video_id") String videoid);

    @FormUrlEncoded
    @POST("API/user_likeinsert.php")
    Call<Example> userlike_video(@Field("user_id") String userid, @Field("video_id") String videoid);


    @FormUrlEncoded
    @POST("API/user_allcommentget.php")
    Call<Example> getUserComment(@Field("video_id") String videoid);

    @FormUrlEncoded
    @POST("API/user_likevideoget.php")
    Call<Example> getUserLikeVideo(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("API/user_profile_data.php")
    Call<Example> getUserProfile_data(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("API/profile_videocount.php")
    Call<Example> getUserProfileVideocount(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("API/user_follwing_list.php")
    Call<Example> getUserFollowinList(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("API/user_followersget.php")
    Call<Example> getUserFollowwersList(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("API/user_comment.php")
    Call<Example> Usercommentset(@Field("user_id") String user_id, @Field("video_id") String video_id, @Field("comment") String comment);

}
