package com.india.flipclip.Splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.github.ybq.android.spinkit.style.FadingCircle;
import com.github.ybq.android.spinkit.style.Wave;
import com.india.flipclip.DemoActivity;
//import com.india.flipclip.Home.HomeActivity;
import com.india.flipclip.R;


public class SplashActivity extends AppCompatActivity {

    ProgressBar progress_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        
        progress_bar = findViewById(R.id.progress_bar);
        Sprite doubleBounce = new Wave();
        progress_bar.setIndeterminateDrawable(doubleBounce);


        splash();
    }

    public void splash() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                callHomeForMainMenu();
            }
        }, 5000);
    }

    public void callHomeForMainMenu() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(SplashActivity.this, DemoActivity.class));
                finish();
            }
        }, 1000);
    }
}
