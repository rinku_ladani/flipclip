//package com.india.flipclip.Home;
//
//import android.Manifest;
//import android.annotation.SuppressLint;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.content.res.Resources;
//import android.graphics.PorterDuff;
//import android.net.Uri;
//import android.os.Build;
//import android.os.Bundle;
//import android.os.Environment;
//import android.os.Handler;
//import android.util.Log;
//import android.util.SparseArray;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import androidx.annotation.RequiresApi;
//import androidx.appcompat.app.AlertDialog;
//import androidx.core.content.ContextCompat;
//import androidx.fragment.app.Fragment;
//import androidx.fragment.app.FragmentManager;
//import androidx.fragment.app.FragmentPagerAdapter;
//import androidx.fragment.app.FragmentTransaction;
//
//import com.etebarian.meowbottomnavigation.MeowBottomNavigation;
//import com.facebook.share.internal.ShareConstants;
//import com.google.android.material.tabs.TabLayout;
//import com.india.flipclip.BackPress.OnBackPressListener;
//import com.india.flipclip.Home.Botttom.Chat.InboxFragment;
//import com.india.flipclip.Home.Botttom.Home.HomeFragment;
//import com.india.flipclip.Home.Botttom.Login.ProfileFragment;
//import com.india.flipclip.Home.Botttom.Search.SearchFragment;
//import com.india.flipclip.Home.Botttom.VideoUpload.VideoUploadFragment;
//import com.india.flipclip.Home.Botttom.VideoUpload.Video_Recoder_A;
//import com.india.flipclip.R;
//import com.india.flipclip.Update.GooglePlayStoreAppVersionNameLoader;
//import com.india.flipclip.Update.WSCallerVersionListener;
//import com.india.flipclip.Util.Custom_ViewPager;
//import com.india.flipclip.Util.PrefManager;
//
//import java.io.File;
//
///**
// * ARPITA AKBARI
// **/
//
//public class MainMenuFragment extends RootFragment implements WSCallerVersionListener, View.OnClickListener {
//
//    public static TabLayout tabLayout;
//    private ViewPagerAdapter adapter;
//    Context context;
//    protected Custom_ViewPager pager;
//
//    class ViewPagerAdapter extends FragmentPagerAdapter {
//        SparseArray<Fragment> registeredFragments = new SparseArray<>();
//
//        public int getCount() {
//            return 5;
//        }
//
//        public ViewPagerAdapter(Resources resources, FragmentManager fragmentManager) {
//            super(fragmentManager);
//        }
//
//        public Fragment getItem(int i) {
//            switch (i) {
//                case 0:
//                    return new HomeFragment();
//                case 1:
//                    return new SearchFragment();
//                case 2:
//                    return new VideoUploadFragment();
//                case 3:
//                    return new ProfileFragment();
//                case 4:
//                    return new InboxFragment();
//                default:
//                    return null;
//            }
//        }
//
//        public Object instantiateItem(ViewGroup viewGroup, int i) {
//            Fragment fragment = (Fragment) super.instantiateItem(viewGroup, i);
//            this.registeredFragments.put(i, fragment);
//            return fragment;
//        }
//
//        public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
//            this.registeredFragments.remove(i);
//            super.destroyItem(viewGroup, i, obj);
//        }
//
//        public Fragment getRegisteredFragment(int i) {
//            return (Fragment) this.registeredFragments.get(i);
//        }
//    }
//    private boolean isForceUpdate = true;
//    private boolean isPermitted;
//    public static File folder;
//
//    PrefManager prefManager;
//
//
//    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
//        View inflate = layoutInflater.inflate(R.layout.fragment_main_menu, viewGroup, false);
//
//        new GooglePlayStoreAppVersionNameLoader(getActivity(), this).execute();
//        checkRunTimePermission();
//
//        prefManager = new PrefManager(getActivity());
//        folder = new File(Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.app_name));
//        boolean success = true;
//        if (!folder.exists()) {
//            //Toast.makeText(MainActivity.this, "Directory Does Not Exist, Create It", Toast.LENGTH_SHORT).show();
//            success = folder.mkdir();
//        }
//        if (success) {
//            //Toast.makeText(MainActivity.this, "Directory Created", Toast.LENGTH_SHORT).show();
//        } else {
//            //Toast.makeText(MainActivity.this, "Failed - Error", Toast.LENGTH_SHORT).show();
//        }
//
//        this.context = getContext();
//        tabLayout = (TabLayout) inflate.findViewById(R.id.tabs);
//        this.pager = (Custom_ViewPager) inflate.findViewById(R.id.viewpager);
//        this.pager.setOffscreenPageLimit(5);
//        this.pager.setPagingEnabled(false);
//        inflate.setOnClickListener(this);
//
//        return inflate;
//
//    }
//
//    @Override
//    public void onGetResponse(boolean isUpdateAvailable) {
//        Log.e("ResultAPPMAIN", String.valueOf(isUpdateAvailable));
//        if (isUpdateAvailable) {
//            showUpdateDialog();
//        }
//    }
//
//    public void showUpdateDialog() {
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
//
//        alertDialogBuilder.setTitle(getString(R.string.app_name));
//        alertDialogBuilder.setMessage(getString(R.string.update_message));
//        alertDialogBuilder.setCancelable(false);
//        alertDialogBuilder.setPositiveButton(R.string.update_now, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getActivity().getPackageName())));
//                dialog.cancel();
//            }
//        });
//        alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                if (isForceUpdate) {
//                }
//                dialog.dismiss();
//            }
//        });
//        alertDialogBuilder.show();
//    }
//
//    public void checkRunTimePermission() {
//        String[] permissionArrays = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_PHONE_STATE, Manifest.permission.MODIFY_AUDIO_SETTINGS, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            requestPermissions(permissionArrays, 11111);
//        } else {
//            // if already permition granted
//            // PUT YOUR ACTION (Like Open cemara etc..)
//        }
//    }
//
//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        boolean openActivityOnce = true;
//        boolean openDialogOnce = true;
//        if (requestCode == 11111) {
//            for (int i = 0; i < grantResults.length; i++) {
//                String permission = permissions[i];
//
//                isPermitted = grantResults[i] == PackageManager.PERMISSION_GRANTED;
//
//                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
//                    // user rejected the permission
//                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
//                    if (!showRationale) {
//                        //execute when 'never Ask Again' tick and permission dialog not show
//                    } else {
//                        if (openDialogOnce) {
//                            alertView();
//                        }
//                    }
//                }
//            }
//
//            if (isPermitted) {
//
//            }
////                if (isPermissionFromGallery)
////                    openGalleryFragment();
//        }
//    }
//
//    private void alertView() {
//        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
//
//        dialog.setTitle("Permission Denied")
//                .setInverseBackgroundForced(true)
//                //.setIcon(R.drawable.ic_info_black_24dp)
//                .setMessage("Without those permission the app is unable to save your profile. App needs to save profile image in your external storage and also need to get profile image from camera or external storage.Are you sure you want to deny this permission?")
//
//                .setNegativeButton("I'M SURE", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialoginterface, int i) {
//                        dialoginterface.dismiss();
//                    }
//                })
//                .setPositiveButton("RE-TRY", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialoginterface, int i) {
//                        dialoginterface.dismiss();
//                        checkRunTimePermission();
//
//                    }
//                }).show();
//    }
//
//    public void onClick(View view) {
//        view.getId();
//    }
//
//    public void onActivityCreated(Bundle bundle) {
//        super.onActivityCreated(bundle);
//        this.adapter = new ViewPagerAdapter(getResources(), getChildFragmentManager());
//        this.pager.setAdapter(this.adapter);
//        tabLayout.setupWithViewPager(this.pager);
//        setupTabIcons();
//    }
//
//    public boolean onBackPressed() {
//        OnBackPressListener onBackPressListener = (OnBackPressListener) this.adapter.getRegisteredFragment(this.pager.getCurrentItem());
//        if (onBackPressListener != null) {
//            return onBackPressListener.onBackPressed();
//        }
//        return false;
//    }
//
//    private void setupTabIcons() {
//        View inflate = LayoutInflater.from(this.context).inflate(R.layout.item_tablayout, null);
//        TextView textView = (TextView) inflate.findViewById(R.id.text);
//        ((ImageView) inflate.findViewById(R.id.image)).setImageDrawable(this.context.getResources().getDrawable(R.drawable.home));
//        textView.setText("Home");
//        textView.setTextColor(this.context.getResources().getColor(R.color.white));
//        tabLayout.getTabAt(0).setCustomView(inflate);
//        View inflate2 = LayoutInflater.from(this.context).inflate(R.layout.item_tablayout, null);
//        ImageView imageView = (ImageView) inflate2.findViewById(R.id.image);
//        TextView textView2 = (TextView) inflate2.findViewById(R.id.text);
//        imageView.setImageDrawable(getResources().getDrawable(R.drawable.search));
//        imageView.setColorFilter(ContextCompat.getColor(this.context, R.color.colorwhite_50), PorterDuff.Mode.SRC_IN);
//        textView2.setText("Discover");
//        textView2.setTextColor(this.context.getResources().getColor(R.color.colorwhite_50));
//        tabLayout.getTabAt(1).setCustomView(inflate2);
//        View inflate3 = LayoutInflater.from(this.context).inflate(R.layout.item_add_tab_layout, null);
//        tabLayout.getTabAt(2).setCustomView(inflate3);
//        View inflate4 = LayoutInflater.from(this.context).inflate(R.layout.item_tablayout, null);
//        ImageView imageView2 = (ImageView) inflate4.findViewById(R.id.image);
//        TextView textView3 = (TextView) inflate4.findViewById(R.id.text);
//        imageView2.setImageDrawable(getResources().getDrawable(R.drawable.profile));
//        imageView2.setColorFilter(ContextCompat.getColor(this.context, R.color.white), PorterDuff.Mode.SRC_IN);
//        textView3.setText("Inbox");
//        textView3.setTextColor(this.context.getResources().getColor(R.color.white));
//        tabLayout.getTabAt(3).setCustomView(inflate4);
//        View inflate5 = LayoutInflater.from(this.context).inflate(R.layout.item_tablayout, null);
//        ImageView imageView3 = (ImageView) inflate5.findViewById(R.id.image);
//        TextView textView4 = (TextView) inflate5.findViewById(R.id.text);
//        imageView3.setImageDrawable(this.context.getResources().getDrawable(R.drawable.inbox));
//        imageView3.setColorFilter(ContextCompat.getColor(this.context, R.color.white), PorterDuff.Mode.SRC_IN);
//        textView4.setText("Profile");
//        textView4.setTextColor(this.context.getResources().getColor(R.color.white));
//        tabLayout.getTabAt(4).setCustomView(inflate5);
//        tabLayout.addOnTabSelectedListener((TabLayout.OnTabSelectedListener) new TabLayout.OnTabSelectedListener() {
//            public void onTabReselected(TabLayout.Tab tab) {
//            }
//
//            public void onTabSelected(TabLayout.Tab tab) {
//                View customView = tab.getCustomView();
//                ImageView imageView = (ImageView) customView.findViewById(R.id.image);
//                TextView textView = (TextView) customView.findViewById(R.id.text);
//                switch (tab.getPosition()) {
//                    case 0:
//                        MainMenuFragment.this.OnHome_Click();
//                        imageView.setImageDrawable(MainMenuFragment.this.context.getResources().getDrawable(R.drawable.ic_home_white));
//                        textView.setTextColor(MainMenuFragment.this.context.getResources().getColor(R.color.white));
//                        break;
//                    case 1:
//                        MainMenuFragment.this.Onother_Tab_Click();
//                        imageView.setImageDrawable(MainMenuFragment.this.context.getResources().getDrawable(R.drawable.ic_discover_red));
//                        imageView.setColorFilter(ContextCompat.getColor(MainMenuFragment.this.context, R.color.app_color), PorterDuff.Mode.SRC_IN);
//                        textView.setTextColor(MainMenuFragment.this.context.getResources().getColor(R.color.app_color));
//                        break;
//                    case 3:
//                        MainMenuFragment.this.Onother_Tab_Click();
//                        imageView.setImageDrawable(MainMenuFragment.this.context.getResources().getDrawable(R.drawable.ic_notification_red));
//                        imageView.setColorFilter(ContextCompat.getColor(MainMenuFragment.this.context, R.color.app_color), PorterDuff.Mode.SRC_IN);
//                        textView.setTextColor(MainMenuFragment.this.context.getResources().getColor(R.color.app_color));
//                        break;
//                    case 4:
//                        MainMenuFragment.this.Onother_Tab_Click();
//                        imageView.setImageDrawable(MainMenuFragment.this.context.getResources().getDrawable(R.drawable.ic_profile_red));
//                        imageView.setColorFilter(ContextCompat.getColor(MainMenuFragment.this.context, R.color.app_color), Mode.SRC_IN);
//                        textView.setTextColor(MainMenuFragment.this.context.getResources().getColor(R.color.app_color));
//                        break;
//                }
//                tab.setCustomView(customView);
//            }
//
//            public void onTabUnselected(TabLayout.Tab tab) {
//                View customView = tab.getCustomView();
//                ImageView imageView = (ImageView) customView.findViewById(R.id.image);
//                TextView textView = (TextView) customView.findViewById(R.id.text);
//                switch (tab.getPosition()) {
//                    case 0:
//                        imageView.setImageDrawable(MainMenuFragment.this.context.getResources().getDrawable(R.drawable.ic_home_gray));
//                        textView.setTextColor(MainMenuFragment.this.context.getResources().getColor(R.color.darkgray));
//                        break;
//                    case 1:
//                        imageView.setImageDrawable(MainMenuFragment.this.context.getResources().getDrawable(R.drawable.ic_discovery_gray));
//                        textView.setTextColor(MainMenuFragment.this.context.getResources().getColor(R.color.darkgray));
//                        break;
//                    case 3:
//                        imageView.setImageDrawable(MainMenuFragment.this.context.getResources().getDrawable(R.drawable.ic_notification_gray));
//                        textView.setTextColor(MainMenuFragment.this.context.getResources().getColor(R.color.darkgray));
//                        break;
//                    case 4:
//                        imageView.setImageDrawable(MainMenuFragment.this.context.getResources().getDrawable(R.drawable.ic_profile_gray));
//                        textView.setTextColor(MainMenuFragment.this.context.getResources().getColor(R.color.darkgray));
//                        break;
//                }
//                tab.setCustomView(customView);
//            }
//        });
//        LinearLayout linearLayout = (LinearLayout) tabLayout.getChildAt(0);
//        linearLayout.setEnabled(false);
//        linearLayout.getChildAt(2).setClickable(false);
//        inflate3.setOnClickListener(new View.OnClickListener() {
//            @SuppressLint("WrongConstant")
//            public void onClick(View view) {
//                if (!checkRunTimePermission()) {
//                    return;
//                }
//                if (Variables.sharedPreferences.getBoolean(Variables.islogin, false)) {
//                    MainMenuFragment.this.startActivity(new Intent(MainMenuFragment.this.getActivity(), Video_Recoder_A.class));
//                    MainMenuFragment.this.getActivity().overridePendingTransition(R.anim.in_from_bottom, R.anim.out_to_top);
//                    return;
//                }
//                Toast.makeText(MainMenuFragment.this.context, "You have to login First", 0).show();
//            }
//        });
//        linearLayout.getChildAt(3).setClickable(false);
//        inflate4.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                if (Variables.sharedPreferences.getBoolean(Variables.islogin, false)) {
//                    MainMenuFragment.tabLayout.getTabAt(3).select();
//                    return;
//                }
//                MainMenuFragment.this.startActivity(new Intent(MainMenuFragment.this.getActivity(), Login_A.class));
//                MainMenuFragment.this.getActivity().overridePendingTransition(R.anim.in_from_bottom, R.anim.out_to_top);
//            }
//        });
//        linearLayout.getChildAt(4).setClickable(false);
//        inflate5.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                if (Variables.sharedPreferences.getBoolean(Variables.islogin, false)) {
//                    MainMenuFragment.tabLayout.getTabAt(4).select();
//                    return;
//                }
//                MainMenuFragment.this.startActivity(new Intent(MainMenuFragment.this.getActivity(), Login_A.class));
//                MainMenuFragment.this.getActivity().overridePendingTransition(R.anim.in_from_bottom, R.anim.out_to_top);
//            }
//        });
////        if (MainMenuActivity.intent != null && MainMenuActivity.intent.hasExtra(ShareConstants.WEB_DIALOG_PARAM_ACTION_TYPE) && Variables.sharedPreferences.getBoolean(Variables.islogin, false) && MainMenuActivity.intent.getExtras().getString(ShareConstants.WEB_DIALOG_PARAM_ACTION_TYPE).equals("message")) {
////            new Handler().postDelayed(new Runnable() {
////                public void run() {
////                    MainMenuFragment.tabLayout.getTabAt(3).select();
////                }
////            }, 1500);
////            chatFragment(MainMenuActivity.intent.getExtras().getString("senderid"), MainMenuActivity.intent.getExtras().getString("title"), MainMenuActivity.intent.getExtras().getString(SettingsJsonConstants.APP_ICON_KEY));
////        }
//    }
//
//}
