package com.india.flipclip.Home.Botttom.VideoUpload;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.coremedia.iso.boxes.Container;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;
import com.india.flipclip.Glob;
import com.india.flipclip.R;
import com.india.flipclip.Util.Common;
import com.india.flipclip.Util.Merge_Video_Audio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import com.google.android.exoplayer2.source.ExtractorMediaSource.Factory;

public class GallerySelectedVideo_A extends AppCompatActivity implements View.OnClickListener, Player.EventListener {
    TextView add_sound_txt;
    MediaPlayer audio;
    String path;
    SimpleExoPlayer video_player;
    private String str;

    public /* synthetic */ void onIsPlayingChanged(boolean z) {
//        CC.$default$onIsPlayingChanged(this, z);
    }

    public void onLoadingChanged(boolean z) {
    }

    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
    }

    public /* synthetic */ void onPlaybackSuppressionReasonChanged(int i) {
//        $default$onPlaybackSuppressionReasonChanged(this, i);
    }

    public void onPlayerError(ExoPlaybackException exoPlaybackException) {
    }

    public void onPositionDiscontinuity(int i) {
    }

    public void onShuffleModeEnabledChanged(boolean z) {
    }

    public /* synthetic */ void onTimelineChanged(Timeline timeline, int i) {
//        CC.$default$onTimelineChanged(this, timeline, i);
    }

    public void onTimelineChanged(Timeline timeline, @Nullable Object obj, int i) {
    }

    public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Hide_navigation();
        setContentView((int) R.layout.activity_gallery_selected_video_);
        Intent intent = getIntent();
        if (intent != null) {
            this.path = intent.getStringExtra("video_path");
        }
        Glob.Selected_sound_id = "null";
        findViewById(R.id.Goback).setOnClickListener(this);
        this.add_sound_txt = (TextView) findViewById(R.id.add_sound_txt);
        this.add_sound_txt.setOnClickListener(this);
        findViewById(R.id.next_btn).setOnClickListener(this);
        Set_Player();
    }

    public void Set_Player() {
        this.video_player = ExoPlayerFactory.newSimpleInstance(this, new DefaultTrackSelector());
        this.video_player.prepare(new Factory(new DefaultDataSourceFactory((Context) this, Util.getUserAgent(this, "TikTok"))).createMediaSource(Uri.parse(this.path)));
        this.video_player.setRepeatMode(0);
        this.video_player.addListener(this);
        PlayerView playerView = (PlayerView) findViewById(R.id.playerview);
        playerView.setPlayer(this.video_player);
        playerView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        this.video_player.setPlayWhenReady(true);
    }

    public void onClick(View view) {
        int id = view.getId();
        if (id == R.id.Goback) {
            finish();
//            overridePendingTransition(R.anim.in_from_top, R.anim.out_from_bottom);
        } else if (id == R.id.add_sound_txt) {
            startActivityForResult(new Intent(this, SoundList_Main_A.class), VideoUploadFragment.Sounds_list_Request_code);
//            overridePendingTransition(R.anim.in_from_bottom, R.anim.out_to_top);
        } else if (id == R.id.next_btn) {
            if (this.video_player != null) {
                this.video_player.setPlayWhenReady(false);
            }
            if (this.audio != null) {
                this.audio.pause();
            }
            Go_To_preview_Activity(path);
//            append();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == VideoUploadFragment.Sounds_list_Request_code && intent != null && intent.getStringExtra("isSelected").equals("yes")) {
            this.add_sound_txt.setText(intent.getStringExtra("sound_name"));
            Glob.Selected_sound_id = intent.getStringExtra("sound_id");
            PreparedAudio();
        }
    }

    public void PreparedAudio() {
        this.video_player.setVolume(0.0f);
        StringBuilder sb = new StringBuilder();
        sb.append(Glob.root);
        sb.append("/");
        sb.append(Glob.SelectedAudio);
        if (new File(sb.toString()).exists()) {
            this.audio = new MediaPlayer();
            try {
                MediaPlayer mediaPlayer = this.audio;
                StringBuilder sb2 = new StringBuilder();
                sb2.append(Glob.root);
                sb2.append("/");
                sb2.append(Glob.SelectedAudio);
                mediaPlayer.setDataSource(sb2.toString());
                this.audio.prepare();
                this.audio.setLooping(true);
//                this.video_player.seekTo(0);
                this.video_player.setPlayWhenReady(true);
                this.audio.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean append() {
        new Thread(new Runnable() {
            public void run() {
                FileInputStream fileInputStream;
                MediaMetadataRetriever mediaMetadataRetriever;
                GallerySelectedVideo_A.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Common.showProgressDialog(GallerySelectedVideo_A.this);
                    }
                });
                ArrayList arrayList = new ArrayList();
                File file = new File(GallerySelectedVideo_A.this.path);
                MediaMetadataRetriever mediaMetadataRetriever2 = null;
                try {
                    mediaMetadataRetriever = new MediaMetadataRetriever();
                    try {
                        fileInputStream = new FileInputStream(file.getAbsolutePath());
                        try {
                            mediaMetadataRetriever.setDataSource(fileInputStream.getFD());
                            if ("yes".equals(mediaMetadataRetriever.extractMetadata(17)) && file.length() > 3000) {
                                arrayList.add(GallerySelectedVideo_A.this.path);
                            }
                            mediaMetadataRetriever.release();
                        } catch (IOException e) {
                            e = e;
                            mediaMetadataRetriever2 = mediaMetadataRetriever;
                            try {
                                Log.e("GallerySelectVideo", e.getMessage(), e);
                                if (mediaMetadataRetriever2 != null) {
                                }
                            } catch (Throwable th) {
                                th = th;
                                mediaMetadataRetriever = mediaMetadataRetriever2;
                                if (mediaMetadataRetriever != null) {
                                    mediaMetadataRetriever.release();
                                }
                                if (fileInputStream != null) {
                                    try {
                                        fileInputStream.close();
                                    } catch (IOException unused) {
                                    }
                                }
                                throw th;
                            }
                        } catch (Throwable th2) {
                            if (mediaMetadataRetriever != null) {
                            }
                            if (fileInputStream != null) {
                            }
                        }
                    } catch (IOException e2) {
                        fileInputStream = null;
                        mediaMetadataRetriever2 = mediaMetadataRetriever;
//                        Log.e("GallerySelectVideo", e.getMessage(), e);
                        if (mediaMetadataRetriever2 != null) {
                        }
                    } catch (Throwable th3) {
                        fileInputStream = null;
                        if (mediaMetadataRetriever != null) {
                        }
                        if (fileInputStream != null) {
                        }
                    }
                } catch (Throwable th4) {
                    mediaMetadataRetriever = null;
                    fileInputStream = null;
                    if (mediaMetadataRetriever != null) {
                    }
                    if (fileInputStream != null) {
                    }
                }
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    Movie[] movieArr = new Movie[arrayList.size()];
                    for (int i = 0; i < arrayList.size(); i++) {
                        movieArr[i] = MovieCreator.build((String) arrayList.get(i));
                    }
                    LinkedList linkedList = new LinkedList();
                    LinkedList linkedList2 = new LinkedList();
                    for (Movie tracks : movieArr) {
                        for (Track track : tracks.getTracks()) {
                            if (track.getHandler().equals("soun")) {
                                linkedList2.add(track);
                            }
                            if (track.getHandler().equals("vide")) {
                                linkedList.add(track);
                            }
                        }
                    }
                    Movie movie = new Movie();
                    if (linkedList2.size() > 0) {
                        movie.addTrack(new AppendTrack((Track[]) linkedList2.toArray(new Track[linkedList2.size()])));
                    }
                    if (linkedList.size() > 0) {
                        movie.addTrack(new AppendTrack((Track[]) linkedList.toArray(new Track[linkedList.size()])));
                    }
                    Container build = new DefaultMp4Builder().build(movie);
                    if (GallerySelectedVideo_A.this.audio == null) {
                        Log.e("asa", str);
                        str = Glob.outputfile;
                    } else {
                        str = Glob.outputfile2;
                    }
                    FileOutputStream fileOutputStream = new FileOutputStream(new File(str));
                    build.writeContainer(fileOutputStream.getChannel());
                    fileOutputStream.close();
                    GallerySelectedVideo_A.this.runOnUiThread(new Runnable() {
                        public void run() {
                            Common.dismissDialog();
                            if (GallerySelectedVideo_A.this.audio != null) {
                                GallerySelectedVideo_A.this.Merge_withAudio();
                            } else {
                                GallerySelectedVideo_A.this.Go_To_preview_Activity(str);
                            }
                        }
                    });
                } catch (Exception unused2) {
                }
            }
        }).start();
        return true;
    }

    public void Merge_withAudio() {
        String file = Environment.getExternalStorageDirectory().toString();
        StringBuilder sb = new StringBuilder();
        sb.append(file);
        sb.append("/");
        sb.append(Glob.SelectedAudio);
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(file);
        sb3.append("/output.mp4");
        String sb4 = sb3.toString();
        StringBuilder sb5 = new StringBuilder();
        sb5.append(file);
        sb5.append("/output2.mp4");
        String sb6 = sb5.toString();
        Log.e("sb2", sb2);
        Log.e("sb4", sb4);
        Log.e("sb6", sb6);
        new Merge_Video_Audio(this).doInBackground(sb2, sb4, sb6);
    }

    public void Go_To_preview_Activity(String str) {
        Log.e("NEXT", str);
        Intent intent = new Intent(getApplicationContext(), Preview_Video_A.class);
        intent.putExtra("path", str);
        startActivity(intent);
//        startActivity(new Intent(this, Preview_Video_A.class));
//        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
        finish();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.video_player != null) {
            this.video_player.setPlayWhenReady(true);
        }
    }

    public void onStop() {
        super.onStop();
        try {
            if (this.video_player != null) {
                this.video_player.setPlayWhenReady(false);
            }
            if (this.audio != null) {
                this.audio.pause();
            }
        } catch (Exception unused) {
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.video_player != null) {
            this.video_player.release();
        }
        if (this.audio != null) {
            this.audio.pause();
            this.audio.release();
        }
    }

    public void onPlayerStateChanged(boolean z, int i) {
        if (i == 4) {
//            this.video_player.seekTo(0);
            this.video_player.setPlayWhenReady(true);
            if (this.audio != null) {
                this.audio.seekTo(0);
                this.audio.start();
            }
        }
    }

    @SuppressLint("WrongConstant")
    public void onRepeatModeChanged(int i) {
        Toast.makeText(this, "Repeat mode change", 0).show();
    }

    public void onSeekProcessed() {
        Log.d("resp", "smmdsmd");
    }

    public void Hide_navigation() {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(5894);
            final View decorView = getWindow().getDecorView();
            decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                public void onSystemUiVisibilityChange(int i) {
                    if ((i & 4) == 0) {
                        decorView.setSystemUiVisibility(5894);
                    }
                }
            });
        }
    }

    @SuppressLint({"NewApi"})
    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (Build.VERSION.SDK_INT >= 19 && z) {
            getWindow().getDecorView().setSystemUiVisibility(5894);
        }
    }
}
