package com.india.flipclip.Home.Botttom.Home.Profile.WatchUserVideo;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daasuu.gpuv.composer.GPUMp4Composer;
import com.daasuu.gpuv.egl.filter.GlWatermarkFilter;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.india.flipclip.DataBase.DatabaseHandler;
import com.india.flipclip.DataBase.VideoLikeModel;
import com.india.flipclip.DemoActivity;
import com.india.flipclip.Home.Botttom.Home.Comment.Comment_F;
import com.india.flipclip.Glob;
import com.india.flipclip.Home.Botttom.Home.Profile.All_userProfileActivity;
//import com.india.flipclip.Home.DemoActivity;
import com.india.flipclip.Model.Customer;
import com.india.flipclip.Model.Example;
import com.india.flipclip.R;
import com.india.flipclip.Rest.API;
import com.india.flipclip.Util.Common;
import com.india.flipclip.Util.PrefManager;
import com.volokh.danylo.hashtaghelper.HashTagHelper;

import java.io.File;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

public class UserVideoWatchActivity extends AppCompatActivity implements Player.EventListener {

    ProgressBar p_bar;
    boolean is_visible_to_user = true;
    SwipeRefreshLayout swiperefresh;
    PrefManager prefManager;
    String username;
    WatchVideoAdapter adapter;
    ArrayList<Customer> data_list;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    SimpleExoPlayer privious_player;
    int currentPage = -1;
    TextView tv_nodata;
    private Uri uri;
    private boolean isFavourite;
    private Intent intent;
    private int position;
    private String video_id;
    private ArrayList arrayList;
    private TextView like_txt;
    String unamepassget;
    private String userpic;

    private DatabaseHandler databaseHandler;
    private SQLiteDatabase sqLiteDatabase;
    FrameLayout frame_user_video;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_video_watch);


        prefManager = new PrefManager(this);
        username = prefManager.getUserName();

        tv_nodata = (TextView) findViewById(R.id.tv_nodata);
        recyclerView = (RecyclerView) findViewById(R.id.recylerview);
        frame_user_video = findViewById(R.id.frame_user_video);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(false);
        new PagerSnapHelper().attachToRecyclerView(recyclerView);
        p_bar = (ProgressBar) findViewById(R.id.p_bar);

        data_list = getIntent().getParcelableArrayListExtra("arraylist");
//        data_list.add((Customer) getIntent().getSerializableExtra("arraylist"));
        position = getIntent().getIntExtra("position", 0);
        userpic = getIntent().getStringExtra("userpic");
        unamepassget = getIntent().getStringExtra("username");
        Set_Adapter();


        swiperefresh = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        swiperefresh.setProgressViewOffset(false, 0, 200);
        swiperefresh.setColorSchemeResources(R.color.black);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            public void onRefresh() {
                currentPage = -1;
            }
        });


    }


    public void Set_Adapter() {
        this.adapter = new WatchVideoAdapter(unamepassget, userpic, this, this.data_list, new WatchVideoAdapter.OnItemClickListener() {
            public void onItemClick(int i, final Customer home_Get_Set, View view) {
                switch (view.getId()) {
                    case R.id.comment_layout:
                        onPause();
                        if (username.isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Please Login First", Toast.LENGTH_SHORT).show();
                        } else {
                            OpenComment(home_Get_Set);
                        }
                        return;
                    case R.id.like_layout:
//                        onPause();
                        if (username.isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Please Login First", Toast.LENGTH_SHORT).show();
                        } else {

                            final ImageView like_image = view.findViewById(R.id.like_image);

                            Log.e("VIdeo", home_Get_Set.getVideoid());
                            String tag = like_image.getTag().toString();
                            if (tag.equalsIgnoreCase("grey")) {
                                Log.e("if1", "if1");
                                prefManager.SetUserFav(home_Get_Set.getVideoid());
//                                Toast.makeText(getContext(), getActivity().getResources().getString(R.string.add_favr), Toast.LENGTH_SHORT).show();
                                user_likevideo(prefManager.getRegId(), home_Get_Set.getVideoid());
                                like_image.setTag("red");
                                like_image.setBackground(getResources().getDrawable(R.drawable.ic_likefill));
                            } else {
                                user_dislikevideo(prefManager.getRegId(), home_Get_Set.getVideoid());
                                Log.e("else1", "else1");
                                prefManager.ClearUserFav();
                                like_image.setTag("grey");
                                like_image.setBackground(getResources().getDrawable(R.drawable.ic_like));
//                                Toast.makeText(getContext(), getActivity().getResources().getString(R.string.remove_favr), Toast.LENGTH_SHORT).show();
                            }

                        }
                        return;
                    case R.id.shared_layout /*2131296707*/:
                        onPause();
                        if (username.isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Please login", Toast.LENGTH_SHORT).show();
                        } else {
//                            deltefolderwithimages(DemoActivity.folder);
                            Save_Video(home_Get_Set);
                        }
                        return;
                    case R.id.user_pic /*2131296816*/:
                        onPause();
                        if (username.isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Please Login First", Toast.LENGTH_SHORT).show();
                        } else {
                            finish();
                        }
                        return;
                    default:
                        return;
                }
            }
        });
        this.adapter.setHasStableIds(true);
        this.recyclerView.setAdapter(this.adapter);
        this.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                super.onScrollStateChanged(recyclerView, i);
            }

            public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                super.onScrolled(recyclerView, i, i2);
                int computeVerticalScrollOffset = recyclerView.computeVerticalScrollOffset() / recyclerView.getHeight();
                if (computeVerticalScrollOffset != currentPage) {
                    currentPage = computeVerticalScrollOffset;
                    Release_Privious_Player();
                    Set_Player(currentPage);
                }
            }
        });
        this.recyclerView.scrollToPosition(this.position);
    }

    private void user_dislikevideo(String userRegistationId, String videoid) {
        API.user().userdislike_video(userRegistationId, videoid).enqueue(new retrofit2.Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                Example videoResponse = response.body();
                if (videoResponse.getSuccess() == 1) {
                    Common.dismissDialog();
//                    Toast.makeText(context, "if", Toast.LENGTH_SHORT).show();
                } else {
                    if (videoResponse.getSuccess() == 0) {
                        Common.dismissDialog();
//                        Toast.makeText(context, "else", Toast.LENGTH_SHORT).show();
                    }
                }
//                Common.dismissDialog();
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
//                Common.showToast(UserVideoWatchActivity.this, "Password does not match");
                Common.dismissDialog();
            }
        });

    }

    private void user_likevideo(String userRegistationId, String videoid) {
        API.user().userlike_video(userRegistationId, videoid).enqueue(new retrofit2.Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                Example videoResponse = response.body();
                if (videoResponse.getSuccess() == 1) {
                    Common.dismissDialog();
//                    Toast.makeText(context, "like_if", Toast.LENGTH_SHORT).show();
                } else {
                    if (videoResponse.getSuccess() == 0) {
                        Common.dismissDialog();
//                        Toast.makeText(context, "like_else", Toast.LENGTH_SHORT).show();
                    }
                }
//                Common.dismissDialog();
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
//                Common.showToast(UserVideoWatchActivity.this, "Password does not match");
//                Common.dismissDialog();
            }
        });

    }

    private void saveState(boolean isFavourite) {
        SharedPreferences aSharedPreferences = UserVideoWatchActivity.this.getSharedPreferences(
                "Favourite", Context.MODE_PRIVATE);
        SharedPreferences.Editor aSharedPreferencesEdit = aSharedPreferences
                .edit();
        aSharedPreferencesEdit.putBoolean("State", isFavourite);
        aSharedPreferencesEdit.commit();
    }

    private boolean readState() {
        SharedPreferences aSharedPreferences = UserVideoWatchActivity.this.getSharedPreferences(
                "Favourite", Context.MODE_PRIVATE);
        return aSharedPreferences.getBoolean("State", true);
    }


    private void OpenComment(Customer home_Get_Set) {
        Log.e("AAAAAAA", home_Get_Set.getVideoid());
        Comment_F comment_F = new Comment_F();
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString("video_id", home_Get_Set.getVideoid());
        bundle.putString("comment_total", home_Get_Set.getComment());
        comment_F.setArguments(bundle);
        beginTransaction.addToBackStack(null);
        beginTransaction.replace(R.id.frame_user_video, comment_F).commit();
    }

    public void Set_Player(int i) {
        Log.e("@@@", String.valueOf(data_list.get(i)));
        if (i >= 0 && this.data_list.size() > 0 && i < this.data_list.size() - 1) {
            Customer home_Get_Set = (Customer) this.data_list.get(i);
            final SimpleExoPlayer newSimpleInstance = ExoPlayerFactory.newSimpleInstance(UserVideoWatchActivity.this, new DefaultTrackSelector());
            DefaultDataSourceFactory defaultDataSourceFactory = new DefaultDataSourceFactory(UserVideoWatchActivity.this, Util.getUserAgent(UserVideoWatchActivity.this, "TikTok"));
            Log.e("dddddd", String.valueOf(data_list.get(i).getComment()));

            newSimpleInstance.prepare(new ExtractorMediaSource.Factory(defaultDataSourceFactory).setExtractorsFactory(new DefaultExtractorsFactory().setMp4ExtractorFlags(1)).createMediaSource(Uri.parse(home_Get_Set.getVideolink())));
            newSimpleInstance.setRepeatMode(2);
            newSimpleInstance.addListener(this);
            View findViewByPosition = this.layoutManager.findViewByPosition(i);


            PlayerView playerView = (PlayerView) findViewByPosition.findViewById(R.id.playerview);
            playerView.setPlayer(newSimpleInstance);
            newSimpleInstance.setPlayWhenReady(this.is_visible_to_user);
            this.privious_player = newSimpleInstance;
            final RelativeLayout relativeLayout = (RelativeLayout) findViewByPosition.findViewById(R.id.mainlayout);
            final Customer home_Get_Set2 = home_Get_Set;
            final int i2 = i;
            View.OnTouchListener r2 = new View.OnTouchListener() {
                private GestureDetector gestureDetector = new GestureDetector(UserVideoWatchActivity.this, new GestureDetector.SimpleOnGestureListener() {
                    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
                        super.onFling(motionEvent, motionEvent2, f, f2);
                        float x = motionEvent.getX() - motionEvent2.getX();
                        float abs = Math.abs(x);
                        if (abs > 100.0f && abs < 1000.0f && x > 0.0f) {
//                            Home_F.this.OpenProfile(home_Get_Set2, true);
                        }
                        return true;
                    }

                    public boolean onSingleTapUp(MotionEvent motionEvent) {
                        super.onSingleTapUp(motionEvent);
                        if (!newSimpleInstance.getPlayWhenReady()) {
                            privious_player.setPlayWhenReady(true);
                        } else {
                            privious_player.setPlayWhenReady(false);
                        }
                        return true;
                    }

                    public void onLongPress(MotionEvent motionEvent) {
                        super.onLongPress(motionEvent);
//                        Show_video_option(home_Get_Set2);
                    }

                    public boolean onDoubleTap(MotionEvent motionEvent) {
                        if (!newSimpleInstance.getPlayWhenReady()) {
                            privious_player.setPlayWhenReady(true);
                        }
//                        if (Variables.sharedPreferences.getBoolean(Variables.islogin, false)) {
//                            Home_F.this.Show_heart_on_DoubleTap(home_Get_Set2, relativeLayout, motionEvent);
//                            Home_F.this.Like_Video(i2, home_Get_Set2);
//                        } else {
//                            Toast.makeText(Home_F.this.context, "Please Login into app", 0).show();
//                        }
                        return super.onDoubleTap(motionEvent);
                    }
                });

                public boolean onTouch(View view, MotionEvent motionEvent) {
                    this.gestureDetector.onTouchEvent(motionEvent);
                    return true;
                }
            };
            playerView.setOnTouchListener(r2);
            HashTagHelper.Creator.create(getResources().getColor(R.color.colorAccent), new HashTagHelper.OnHashTagClickListener() {
                public void onHashTagClicked(String str) {
                    onPause();
//                   OpenHashtag(str);
                }
            }).handle((TextView) findViewByPosition.findViewById(R.id.desc_txt));
            ((LinearLayout) findViewByPosition.findViewById(R.id.sound_image_layout)).startAnimation(AnimationUtils.loadAnimation(UserVideoWatchActivity.this, R.anim.d_clockwise_rotation));
//            if (Variables.sharedPreferences.getBoolean(Variables.islogin, false)) {
//                Functions.Call_Api_For_update_view(getActivity(), home_Get_Set.video_id);
//            }
        }
    }


    public void onResume() {
        super.onResume();
        isFavourite = readState();
        if (this.privious_player != null && this.is_visible_to_user) {
            this.privious_player.setPlayWhenReady(true);
        }
    }


    public void onPause() {
        super.onPause();
        if (this.privious_player != null) {
            this.privious_player.setPlayWhenReady(false);
        }
    }

    public void onStop() {
        super.onStop();
        if (this.privious_player != null) {
            this.privious_player.setPlayWhenReady(false);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.privious_player != null) {
            this.privious_player.release();
        }
    }

//    public void setUserVisibleHint(boolean z) {
//        super.setUserVisibleHint(z);
//        this.is_visible_to_user = z;
//        if (this.privious_player != null && z) {
//            this.privious_player.setPlayWhenReady(true);
//        } else if (this.privious_player != null && !z) {
//            this.privious_player.setPlayWhenReady(false);
//        }
//    }

    public void Release_Privious_Player() {
        isFavourite = readState();
        if (this.privious_player != null) {
            this.privious_player.removeListener(this);
            this.privious_player.release();
        }
    }

    @SuppressLint("WrongConstant")
    public void onPlayerStateChanged(boolean z, int i) {
        isFavourite = readState();
        if (i == 2) {
            this.p_bar.setVisibility(0);
        } else if (i == 3) {
            this.p_bar.setVisibility(8);
        }
    }

    public void Save_Video(final Customer home_Get_Set) {
        Glob.Show_determinent_loader(UserVideoWatchActivity.this, false, false);
        PRDownloader.initialize(UserVideoWatchActivity.this);
        String str = home_Get_Set.getVideolink();
        StringBuilder sb = new StringBuilder();
        sb.append(DemoActivity.folder);
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(home_Get_Set.getVideoid());
        sb3.append("no_watermark.mp4");
        Log.e("D1", sb2.toString());
        PRDownloader.download(str, sb2, sb3.toString()).build().setOnStartOrResumeListener(new OnStartOrResumeListener() {
            public void onStartOrResume() {
            }
        }).setOnPauseListener(new OnPauseListener() {
            public void onPause() {
            }
        }).setOnCancelListener(new OnCancelListener() {
            public void onCancel() {
            }
        }).setOnProgressListener(new OnProgressListener() {
            public void onProgress(Progress progress) {
                Glob.Show_loading_progress(((int) ((progress.currentBytes * 100) / progress.totalBytes)) / 2);
            }
        }).start(new OnDownloadListener() {
            public void onDownloadComplete() {
                Applywatermark(home_Get_Set);
            }

            @SuppressLint("WrongConstant")
            public void onError(Error error) {
                Delete_file_no_watermark(home_Get_Set);
                Toast.makeText(UserVideoWatchActivity.this, "Error", 0).show();
                Glob.cancel_determinent_loader();
            }
        });
    }

    public void Delete_file_no_watermark(Customer home_Get_Set) {
        StringBuilder sb = new StringBuilder();
        sb.append(DemoActivity.folder);
        sb.append(home_Get_Set.getVideoid());
        sb.append("no_watermark.mp4");
        File file = new File(sb.toString());
        if (file.exists()) {
            file.delete();
        }
    }

    public void Delete_file_share(String home_Get_Set) {
        File file = new File(home_Get_Set);
        if (file.exists()) {
            file.delete();
        }
    }


    public void Applywatermark(final Customer home_Get_Set) {
        GlWatermarkFilter glWatermarkFilter = new GlWatermarkFilter(Bitmap.createScaledBitmap(((BitmapDrawable) getResources().getDrawable(R.drawable.logo)).getBitmap(), 50, 50, false), GlWatermarkFilter.Position.LEFT_TOP);
        StringBuilder sb = new StringBuilder();
        sb.append(DemoActivity.folder);
        sb.append("/");
        sb.append(home_Get_Set.getVideoid());
        sb.append("no_watermark.mp4");
        final String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(DemoActivity.folder);
        sb3.append("/");
        sb3.append(home_Get_Set.getVideoid());
        sb3.append(".mp4");
        Log.e("ARPITA", sb2.toString());
        Log.e("AAKBRI", sb3.toString());
        new GPUMp4Composer(sb2, sb3.toString()).filter(glWatermarkFilter).listener(new GPUMp4Composer.Listener() {
            public void onProgress(double d) {
                StringBuilder sb = new StringBuilder();
                sb.append("");
                double d2 = d * 100.0d;
                sb.append((int) d2);
                Log.e("resp", sb.toString());
                Glob.Show_loading_progress(((int) (d2 / 2.0d)) + 50);
            }

            public void onCompleted() {
                UserVideoWatchActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Glob.cancel_determinent_loader();
                        Delete_file_no_watermark(home_Get_Set);
                        Scan_file(sb2.toString());
                    }
                });
            }

            public void onCanceled() {
                Log.e("resp", "onCanceled");
            }

            public void onFailed(Exception exc) {
                Log.e("resp", exc.toString());
                UserVideoWatchActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            Delete_file_no_watermark(home_Get_Set);
                            Glob.cancel_determinent_loader();
                            Toast.makeText(UserVideoWatchActivity.this, "Try Again", 0).show();
                        } catch (Exception unused) {
                        }
                    }
                });
            }
        }).start();
    }

    public void Scan_file(String home_Get_Set) {
//        Context activity = getContext();
//        StringBuilder sb = new StringBuilder();
//        sb.append(DemoActivity.folder);
//        sb.append(home_Get_Set.getVideoid());
//        sb.append(".mp4");
        Log.e("SA", home_Get_Set);

        Uri U = FileProvider.getUriForFile(UserVideoWatchActivity.this, "com.india.flipclip.fileprovider" +
                "" +
                "" +
                "", new File(home_Get_Set));
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("video/*");
        i.putExtra(Intent.EXTRA_STREAM, U);
        startActivityForResult(Intent.createChooser(i, "Share video Using"), 1);

//        File file = new File(home_Get_Set);
//        if (file.exists()) {
//            file.delete();
//        } else {
//            Log.e("Wrong", "w");
//        }

//        this.shareIntent = new Intent("android.intent.action.SEND");
//        this.shareIntent.setType("video/*");
//        shareIntent.putExtra("android.intent.extra.TEXT", getResources().getString(R.string.app_name) + " Create By : https://play.google.com/store/apps/details?id=" + getContext().getPackageName());
//        if (Build.VERSION.SDK_INT >= 23) {
//            uri = FileProvider.getUriForFile(getActivity(), "com.india.flipclip.fileprovider", new File(home_Get_Set));
//        } else {
//            uri = Uri.fromFile(new File(home_Get_Set));
//        }
//        this.shareIntent.putExtra("android.intent.extra.STREAM", uri);

//        MediaScannerConnection.scanFile(activity, new String[]{sb.toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
//            public void onScanCompleted(String str, Uri uri) {
//                Log.e("DAA",str);
//                StringBuilder sb = new StringBuilder();
//                sb.append("Scanned ");
//                sb.append(str);
//                sb.append(":");
//                Log.e("ExternalStorage", sb.toString());
//                StringBuilder sb2 = new StringBuilder();
//                sb2.append("-> uri=");
//                sb2.append(uri);
//                Log.e("ExternalStorage", sb2.toString());
//            }
//        });
    }

    public /* synthetic */ void onIsPlayingChanged(boolean z) {
//        CC.$default$onIsPlayingChanged(this, z);
    }

    public void onLoadingChanged(boolean z) {
    }

    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
    }

    public /* synthetic */ void onPlaybackSuppressionReasonChanged(int i) {
//        CC.$default$onPlaybackSuppressionReasonChanged(this, i);
    }

    public void onPlayerError(ExoPlaybackException exoPlaybackException) {
    }

    public void onPositionDiscontinuity(int i) {
    }

    public void onRepeatModeChanged(int i) {
    }

    public void onSeekProcessed() {
    }

    public void onShuffleModeEnabledChanged(boolean z) {
    }

    public /* synthetic */ void onTimelineChanged(Timeline timeline, int i) {
//        CC.$default$onTimelineChanged(this, timeline, i);
    }

    public void onTimelineChanged(Timeline timeline, @Nullable Object obj, int i) {
    }

    public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
    }


}