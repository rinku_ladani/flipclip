package com.india.flipclip.Home.Botttom.Home;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.daasuu.gpuv.composer.GPUMp4Composer;
import com.daasuu.gpuv.egl.filter.GlWatermarkFilter;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.india.flipclip.DemoActivity;
import com.india.flipclip.Home.Botttom.Home.Profile.All_userProfileActivity;
import com.india.flipclip.Home.Botttom.Home.Comment.Comment_F;
import com.india.flipclip.Home.Botttom.Login.ProfileFragment;
//import com.india.flipclip.Home.DemoActivity;
import com.india.flipclip.Home.Botttom.Home.Comment.Comments_Adapter;
import com.india.flipclip.DataBase.DatabaseHandler;
import com.india.flipclip.DataBase.VideoLikeModel;
import com.india.flipclip.Glob;
import com.india.flipclip.Model.Example;
import com.india.flipclip.R;
import com.india.flipclip.Model.Customer;
import com.india.flipclip.Model.VideoResponse;
import com.india.flipclip.Rest.API;
import com.india.flipclip.Splash.SplashActivity;
import com.india.flipclip.Util.AppPrefs;
import com.india.flipclip.Util.Common;
import com.india.flipclip.Util.PrefManager;
import com.volokh.danylo.hashtaghelper.HashTagHelper;

import java.io.File;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements Player.EventListener {

    AppPrefs appPrefs;
    ImageView img_videoup;
    ProgressBar p_bar;
    boolean is_visible_to_user = true;
    SwipeRefreshLayout swiperefresh;
    PrefManager prefManager;
    String username;
    Home_Adapter adapter;
    ArrayList<Customer> data_list;
    ArrayList<Customer> data_list1;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    SimpleExoPlayer privious_player;
    int currentPage = -1;
    TextView tv_nodata;
    Context context;

    public HomeFragment() {

    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View root_view = layoutInflater.inflate(R.layout.home_fragment, viewGroup, false);

        appPrefs = new AppPrefs(getContext());
        context = getActivity();
        prefManager = new PrefManager(getActivity());
        username = prefManager.getUserName();

        img_videoup = root_view.findViewById(R.id.img_videoup);
        tv_nodata = (TextView) root_view.findViewById(R.id.tv_nodata);
        recyclerView = (RecyclerView) root_view.findViewById(R.id.recylerview);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(false);
        new PagerSnapHelper().attachToRecyclerView(recyclerView);
        p_bar = (ProgressBar) root_view.findViewById(R.id.p_bar);
        swiperefresh = (SwipeRefreshLayout) root_view.findViewById(R.id.swiperefresh);
        swiperefresh.setProgressViewOffset(false, 0, 200);
        swiperefresh.setColorSchemeResources(R.color.black);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            public void onRefresh() {
                currentPage = -1;
                getAllCustomer();
            }
        });


//        img_videoup.setVisibility(View.VISIBLE);
        Animation aniSlide = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        img_videoup.startAnimation(aniSlide);
        img_videoup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appPrefs.setisFirstTime("TRUE");
            }
        });

        if (appPrefs.getisFirstTime().equalsIgnoreCase("TRUE")) {
            img_videoup.setVisibility(View.GONE);
        }


        getAllCustomer();
        this.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                super.onScrollStateChanged(recyclerView, i);
            }

            public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                super.onScrolled(recyclerView, i, i2);
                int computeVerticalScrollOffset = recyclerView.computeVerticalScrollOffset() / recyclerView.getHeight();
                if (computeVerticalScrollOffset != currentPage) {
                    currentPage = computeVerticalScrollOffset;
                    Release_Privious_Player();
                    Set_Player(currentPage);
                }
            }
        });

        return root_view;
    }

    private void getAllCustomer() {
        Common.showProgressDialog(getContext());
        API.user().getAllVideo().enqueue(new retrofit2.Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                swiperefresh.setRefreshing(false);
                Example videoResponse = response.body();
                if (videoResponse.getSuccess() == 1) {
                    data_list = videoResponse.getCustomer();
                    Log.e("SIZE", String.valueOf(data_list.size()));
                    Set_Adapter();
                    Common.dismissDialog();
                } else {
                    if (videoResponse.getSuccess() == 0) {
                        Common.dismissDialog();
                    }
                }
//                Common.dismissDialog();
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
//                Common.showToast(getContext(), "Password does not match");
                Common.dismissDialog();
            }
        });
    }


    public void Set_Adapter() {
        this.adapter = new Home_Adapter(getContext(), this.data_list, new Home_Adapter.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            public void onItemClick(int i, final Customer home_Get_Set, View view) {
                switch (view.getId()) {
                    case R.id.comment_layout /*2131296406*/:
                        onPause();
                        if (username.isEmpty()) {
                            Toast.makeText(getContext(), "Please Login First", Toast.LENGTH_SHORT).show();
                        } else {
                            OpenComment(home_Get_Set);
                        }
                        return;
                    case R.id.like_layout /*2131296548*/:
//                        onPause();
                        if (username.isEmpty()) {
                            Toast.makeText(getContext(), "Please Login First", Toast.LENGTH_SHORT).show();
                        } else {

                            final ImageView like_image = view.findViewById(R.id.like_image);
                            Log.e("VideoFra==", home_Get_Set.getVideoid());
                            String tag = like_image.getTag().toString();
                            if (tag.equalsIgnoreCase("grey")) {
                                Log.e("if1", "if1");
                                prefManager.SetUserFav(home_Get_Set.getVideoid());
                                user_likevideo(prefManager.getRegId(), home_Get_Set.getVideoid());
                                like_image.setTag("red");
                                like_image.setBackground(getActivity().getResources().getDrawable(R.drawable.ic_likefill));
                            } else {
                                user_dislikevideo(prefManager.getRegId(), home_Get_Set.getVideoid());
                                Log.e("else1", "else1");
                                prefManager.ClearUserFav();
                                like_image.setTag("grey");
                                like_image.setBackground(getActivity().getResources().getDrawable(R.drawable.ic_like));
                            }
                        }
                        return;
                    case R.id.shared_layout /*2131296707*/:
                        onPause();
                        if (username.isEmpty()) {
                            Toast.makeText(getContext(), "Please login", Toast.LENGTH_SHORT).show();
                        } else {
//                            deltefolderwithimages(DemoActivity.folder);
                            Save_Video(home_Get_Set);
                        }
//                        Home_F.this.is_add_show = false;
//                        new VideoAction_F(home_Get_Set.video_id, new Fragment_Callback() {
//                            public void Responce(Bundle bundle) {
//                                if (bundle.getString(NativeProtocol.WEB_DIALOG_ACTION).equals("save")) {
//                                    Home_F.this.Save_Video(home_Get_Set);
//                                }
//                            }
//                        }).show(Home_F.this.getChildFragmentManager(), "");
                        return;
                    case R.id.user_pic /*2131296816*/:
                        onPause();
                        if (username.isEmpty()) {
                            Toast.makeText(getContext(), "Please Login First", Toast.LENGTH_SHORT).show();
                        } else {
                            if (prefManager.getUserName().equalsIgnoreCase(home_Get_Set.getUname())) {
                                FragmentTransaction beginTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                                beginTransaction.replace(R.id.framelayout, new ProfileFragment());
                                beginTransaction.commit();
                            } else {
                                Intent intent = new Intent(getContext(), All_userProfileActivity.class);
                                intent.putExtra("userid", home_Get_Set.getUserRegistationId());
                                intent.putExtra("username", home_Get_Set.getUname());
                                intent.putExtra("userpic", home_Get_Set.getProfilePic());
                                startActivity(intent);
                            }
                        }
                        return;
                    default:
                        return;
                }
            }
        });
        this.adapter.setHasStableIds(true);
        this.recyclerView.setAdapter(this.adapter);
    }

    private void user_dislikevideo(String userRegistationId, String videoid) {
        API.user().userdislike_video(userRegistationId, videoid).enqueue(new retrofit2.Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                Example videoResponse = response.body();
                if (videoResponse.getSuccess() == 1) {
                    Common.dismissDialog();
                    Log.e("dislikec", String.valueOf(videoResponse.getLikeCount()));
//                    Toast.makeText(context, "if", Toast.LENGTH_SHORT).show();
                } else {
                    if (videoResponse.getSuccess() == 0) {
//                        Toast.makeText(context, "else", Toast.LENGTH_SHORT).show();
                        Common.dismissDialog();
                    }
                }
//                Common.dismissDialog();
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
//                Common.showToast(getActivity(), "Password does not match");
                Common.dismissDialog();
            }
        });

    }

    private void user_likevideo(String userRegistationId, String videoid) {
        API.user().userlike_video(userRegistationId, videoid).enqueue(new retrofit2.Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                Example videoResponse = response.body();
                if (videoResponse.getSuccess() == 1) {
                    Common.dismissDialog();
                    Log.e("likec", String.valueOf(videoResponse.getLikeCount()));
//                    Toast.makeText(context, "like_if", Toast.LENGTH_SHORT).show();
                } else {
                    if (videoResponse.getSuccess() == 0) {
                        Common.dismissDialog();
//                        Toast.makeText(context, "like_else", Toast.LENGTH_SHORT).show();
                    }
                }
//                Common.dismissDialog();
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
//                Common.showToast(getActivity(), "Password does not match");
                Common.dismissDialog();
            }
        });

    }

    private void saveState(boolean isFavourite) {
        SharedPreferences aSharedPreferences = getContext().getSharedPreferences(
                "Favourite", Context.MODE_PRIVATE);
        SharedPreferences.Editor aSharedPreferencesEdit = aSharedPreferences
                .edit();
        aSharedPreferencesEdit.putBoolean("State", isFavourite);
        aSharedPreferencesEdit.commit();
    }

    private boolean readState() {
        SharedPreferences aSharedPreferences = getContext().getSharedPreferences(
                "Favourite", Context.MODE_PRIVATE);
        return aSharedPreferences.getBoolean("State", true);
    }


    public void Set_Player(int i) {
        if (i >= 0 && this.data_list.size() > 0 && i < this.data_list.size() - 1) {
            Customer home_Get_Set = (Customer) this.data_list.get(i);
            final SimpleExoPlayer newSimpleInstance = ExoPlayerFactory.newSimpleInstance(getContext(), new DefaultTrackSelector());
            DefaultDataSourceFactory defaultDataSourceFactory = new DefaultDataSourceFactory(getContext(), Util.getUserAgent(getContext(), "TikTok"));
            Log.d("resp", home_Get_Set.getVideolink());
            newSimpleInstance.prepare(new ExtractorMediaSource.Factory(defaultDataSourceFactory).setExtractorsFactory(new DefaultExtractorsFactory().setMp4ExtractorFlags(1)).createMediaSource(Uri.parse(home_Get_Set.getVideolink())));
            newSimpleInstance.setRepeatMode(2);
            newSimpleInstance.addListener(this);
            View findViewByPosition = this.layoutManager.findViewByPosition(i);
            PlayerView playerView = (PlayerView) findViewByPosition.findViewById(R.id.playerview);
            playerView.setPlayer(newSimpleInstance);
            newSimpleInstance.setPlayWhenReady(this.is_visible_to_user);
            this.privious_player = newSimpleInstance;
            final RelativeLayout relativeLayout = (RelativeLayout) findViewByPosition.findViewById(R.id.mainlayout);
            final Customer home_Get_Set2 = home_Get_Set;
            final int i2 = i;
            View.OnTouchListener r2 = new View.OnTouchListener() {
                private GestureDetector gestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {
                    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
                        super.onFling(motionEvent, motionEvent2, f, f2);
                        float x = motionEvent.getX() - motionEvent2.getX();
                        float abs = Math.abs(x);
                        if (abs > 100.0f && abs < 1000.0f && x > 0.0f) {
//                            Home_F.this.OpenProfile(home_Get_Set2, true);
                        }
                        return true;
                    }

                    public boolean onSingleTapUp(MotionEvent motionEvent) {
                        super.onSingleTapUp(motionEvent);
                        if (!newSimpleInstance.getPlayWhenReady()) {
                            privious_player.setPlayWhenReady(true);
                        } else {
                            privious_player.setPlayWhenReady(false);
                        }
                        return true;
                    }

                    public void onLongPress(MotionEvent motionEvent) {
                        super.onLongPress(motionEvent);
//                        Show_video_option(home_Get_Set2);
                    }

                    public boolean onDoubleTap(MotionEvent motionEvent) {
                        if (!newSimpleInstance.getPlayWhenReady()) {
                            privious_player.setPlayWhenReady(true);
                        }
//                        if (Variables.sharedPreferences.getBoolean(Variables.islogin, false)) {
//                            Home_F.this.Show_heart_on_DoubleTap(home_Get_Set2, relativeLayout, motionEvent);
//                            Home_F.this.Like_Video(i2, home_Get_Set2);
//                        } else {
//                            Toast.makeText(Home_F.this.context, "Please Login into app", 0).show();
//                        }
                        return super.onDoubleTap(motionEvent);
                    }
                });

                public boolean onTouch(View view, MotionEvent motionEvent) {
                    this.gestureDetector.onTouchEvent(motionEvent);
                    return true;
                }
            };
            playerView.setOnTouchListener(r2);
            HashTagHelper.Creator.create(getResources().getColor(R.color.colorAccent), new HashTagHelper.OnHashTagClickListener() {
                public void onHashTagClicked(String str) {
                    onPause();
//                   OpenHashtag(str);
                }
            }).handle((TextView) findViewByPosition.findViewById(R.id.desc_txt));
            ((LinearLayout) findViewByPosition.findViewById(R.id.sound_image_layout)).startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.d_clockwise_rotation));
//            if (Variables.sharedPreferences.getBoolean(Variables.islogin, false)) {
//                Functions.Call_Api_For_update_view(getActivity(), home_Get_Set.video_id);
//            }
        }
    }


    public void onResume() {
        super.onResume();
        if (this.privious_player != null && this.is_visible_to_user) {
            this.privious_player.setPlayWhenReady(true);
        }
    }


    public void onPause() {
        super.onPause();
        if (this.privious_player != null) {
            this.privious_player.setPlayWhenReady(false);
        }
    }

    public void onStop() {
        super.onStop();
        if (this.privious_player != null) {
            this.privious_player.setPlayWhenReady(false);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.privious_player != null) {
            this.privious_player.release();
        }
    }

//    public void setUserVisibleHint(boolean z) {
//        super.setUserVisibleHint(z);
//        this.is_visible_to_user = z;
//        if (this.privious_player != null && z) {
//            this.privious_player.setPlayWhenReady(true);
//        } else if (this.privious_player != null && !z) {
//            this.privious_player.setPlayWhenReady(false);
//        }
//    }

    public void Release_Privious_Player() {
        if (this.privious_player != null) {
            this.privious_player.removeListener(this);
            this.privious_player.release();
        }
    }

    @SuppressLint("WrongConstant")
    public void onPlayerStateChanged(boolean z, int i) {
        if (i == 2) {
            this.p_bar.setVisibility(0);
        } else if (i == 3) {
            this.p_bar.setVisibility(8);
        }
    }

    public void Save_Video(final Customer home_Get_Set) {
        Glob.Show_determinent_loader(getContext(), false, false);
        PRDownloader.initialize(getActivity());
        String str = home_Get_Set.getVideolink();
        StringBuilder sb = new StringBuilder();
        sb.append(DemoActivity.folder);
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(home_Get_Set.getVideoid());
        sb3.append("no_watermark.mp4");
        Log.e("D1", sb2.toString());
        PRDownloader.download(str, sb2, sb3.toString()).build().setOnStartOrResumeListener(new OnStartOrResumeListener() {
            public void onStartOrResume() {
            }
        }).setOnPauseListener(new OnPauseListener() {
            public void onPause() {
            }
        }).setOnCancelListener(new OnCancelListener() {
            public void onCancel() {
            }
        }).setOnProgressListener(new OnProgressListener() {
            public void onProgress(Progress progress) {
                Glob.Show_loading_progress(((int) ((progress.currentBytes * 100) / progress.totalBytes)) / 2);
            }
        }).start(new OnDownloadListener() {
            public void onDownloadComplete() {
                Applywatermark(home_Get_Set);
            }

            @SuppressLint("WrongConstant")
            public void onError(Error error) {
                Delete_file_no_watermark(home_Get_Set);
                Toast.makeText(getContext(), "Error", 0).show();
                Glob.cancel_determinent_loader();
            }
        });
    }

    public void Delete_file_no_watermark(Customer home_Get_Set) {
        StringBuilder sb = new StringBuilder();
        sb.append(DemoActivity.folder);
        sb.append(home_Get_Set.getVideoid());
        sb.append("no_watermark.mp4");
        File file = new File(sb.toString());
        if (file.exists()) {
            file.delete();
        }
    }

    public void Delete_file_share(String home_Get_Set) {
        File file = new File(home_Get_Set);
        if (file.exists()) {
            file.delete();
        }
    }


    public void Applywatermark(final Customer home_Get_Set) {
        GlWatermarkFilter glWatermarkFilter = new GlWatermarkFilter(Bitmap.createScaledBitmap(((BitmapDrawable) getResources().getDrawable(R.drawable.logo)).getBitmap(), 50, 50, false), GlWatermarkFilter.Position.LEFT_TOP);
        StringBuilder sb = new StringBuilder();
        sb.append(DemoActivity.folder);
        sb.append("/");
        sb.append(home_Get_Set.getVideoid());
        sb.append("no_watermark.mp4");
        final String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(DemoActivity.folder);
        sb3.append("/");
        sb3.append(home_Get_Set.getVideoid());
        sb3.append(".mp4");
        Log.e("ARPITA", sb2.toString());
        Log.e("AAKBRI", sb3.toString());
        new GPUMp4Composer(sb2, sb3.toString()).filter(glWatermarkFilter).listener(new GPUMp4Composer.Listener() {
            public void onProgress(double d) {
                StringBuilder sb = new StringBuilder();
                sb.append("");
                double d2 = d * 100.0d;
                sb.append((int) d2);
                Log.e("resp", sb.toString());
                Glob.Show_loading_progress(((int) (d2 / 2.0d)) + 50);
            }

            public void onCompleted() {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        Glob.cancel_determinent_loader();
                        Delete_file_no_watermark(home_Get_Set);
                        Scan_file(sb2.toString());
                    }
                });
            }

            public void onCanceled() {
                Log.e("resp", "onCanceled");
            }

            public void onFailed(Exception exc) {
                Log.e("resp", exc.toString());
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            Delete_file_no_watermark(home_Get_Set);
                            Glob.cancel_determinent_loader();
                            Toast.makeText(getContext(), "Try Again", 0).show();
                        } catch (Exception unused) {
                        }
                    }
                });
            }
        }).start();
    }

    public void Scan_file(String home_Get_Set) {
//        Context activity = getContext();
//        StringBuilder sb = new StringBuilder();
//        sb.append(DemoActivity.folder);
//        sb.append(home_Get_Set.getVideoid());
//        sb.append(".mp4");
        Log.e("SA", home_Get_Set);

        Uri U = FileProvider.getUriForFile(getActivity(), "com.india.flipclip.fileprovider" +
                "" +
                "" +
                "", new File(home_Get_Set));
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("video/*");
        i.putExtra(Intent.EXTRA_STREAM, U);
        startActivityForResult(Intent.createChooser(i, "Share video Using"), 1);
    }

    private void OpenComment(Customer home_Get_Set) {
        Comment_F comment_F = new Comment_F();
        FragmentTransaction beginTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString("video_id", home_Get_Set.getVideoid());
        bundle.putString("comment_total", home_Get_Set.getComment());
        comment_F.setArguments(bundle);
        beginTransaction.addToBackStack(null);
        beginTransaction.replace(R.id.MainMenuFragment, comment_F).commit();
    }

}