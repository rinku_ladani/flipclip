package com.india.flipclip.Home.Botttom.VideoUpload;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.india.flipclip.R;
import com.india.flipclip.Util.Custom_ViewPager;

public class SoundList_Main_A extends AppCompatActivity implements View.OnClickListener {
    private ViewPagerAdapter adapter;
    protected Custom_ViewPager pager;
    protected TabLayout tablayout;

    class ViewPagerAdapter extends FragmentPagerAdapter {
        SparseArray<Fragment> registeredFragments = new SparseArray<>();

        public int getCount() {
            return 2;
        }

        public CharSequence getPageTitle(int i) {
            switch (i) {
                case 0:
                    return "Discover";
                case 1:
                    return "My Favorites";
                default:
                    return null;
            }
        }

        public ViewPagerAdapter(Resources resources, FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public Fragment getItem(int i) {
            switch (i) {
                case 0:
//                    return new Discover_SoundList_F();
                case 1:
//                    return new Favourite_Sound_F();
                default:
                    return null;
            }
        }

        public Object instantiateItem(ViewGroup viewGroup, int i) {
            Fragment fragment = (Fragment) super.instantiateItem(viewGroup, i);
            this.registeredFragments.put(i, fragment);
            return fragment;
        }

        public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
            this.registeredFragments.remove(i);
            super.destroyItem(viewGroup, i, obj);
        }

        public Fragment getRegisteredFragment(int i) {
            return (Fragment) this.registeredFragments.get(i);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_sound_list__main_);
        this.tablayout = (TabLayout) findViewById(R.id.groups_tab);
        this.pager = (Custom_ViewPager) findViewById(R.id.viewpager);
        this.pager.setOffscreenPageLimit(2);
        this.pager.setPagingEnabled(false);
        this.adapter = new ViewPagerAdapter(getResources(), getSupportFragmentManager());
        this.pager.setAdapter(this.adapter);
        this.tablayout.setupWithViewPager(this.pager);
        findViewById(R.id.Goback).setOnClickListener(this);
    }

    public void onClick(View view) {
        if (view.getId() == R.id.Goback) {
            onBackPressed();
        }
    }
}

