package com.india.flipclip.Home.Botttom.VideoUpload;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView.Adapter;
import androidx.recyclerview.widget.RecyclerView.LayoutParams;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.bumptech.glide.Glide;
import com.india.flipclip.Model.GalleryVideoModel;
import com.india.flipclip.R;

import java.util.ArrayList;

public class GalleryVideos_Adapter extends Adapter<GalleryVideos_Adapter.CustomViewHolder> {
    public Context context;
    private ArrayList<GalleryVideoModel> dataList;
    private OnItemClickListener listener;

    class CustomViewHolder extends ViewHolder {
        ImageView thumb_image;
        TextView view_txt;

        public CustomViewHolder(View view) {
            super(view);
            this.thumb_image = (ImageView) view.findViewById(R.id.thumb_image);
            this.view_txt = (TextView) view.findViewById(R.id.view_txt);
        }

        public void bind(final int i, final GalleryVideoModel galleryVideo_Model, final OnItemClickListener onItemClickListener) {
            this.itemView.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    onItemClickListener.onItemClick(i, galleryVideo_Model, view);
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int i, GalleryVideoModel galleryVideo_Model, View view);
    }

    public GalleryVideos_Adapter(Context context2, ArrayList<GalleryVideoModel> arrayList, OnItemClickListener onItemClickListener) {
        this.context = context2;
        this.dataList = arrayList;
        this.listener = onItemClickListener;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_galleryvideo_layout, null);
        inflate.setLayoutParams(new LayoutParams(-1, -2));
        return new CustomViewHolder(inflate);
    }

    public int getItemCount() {
        return this.dataList.size();
    }

    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        GalleryVideoModel galleryVideo_Model = (GalleryVideoModel) this.dataList.get(i);
        customViewHolder.view_txt.setText(galleryVideo_Model.getVideo_time());
        Glide.with(this.context).load(galleryVideo_Model.getThumb_path()).into(customViewHolder.thumb_image);
        customViewHolder.bind(i, galleryVideo_Model, this.listener);
    }
}
