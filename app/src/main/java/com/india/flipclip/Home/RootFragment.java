package com.india.flipclip.Home;

import androidx.fragment.app.Fragment;

import com.india.flipclip.BackPress.BackPressImplimentation;
import com.india.flipclip.BackPress.OnBackPressListener;

/**
 * ARPITA AKBARI
 **/

public class RootFragment extends Fragment implements OnBackPressListener {
    public boolean onBackPressed() {
        return new BackPressImplimentation(this).onBackPressed();
    }
}

