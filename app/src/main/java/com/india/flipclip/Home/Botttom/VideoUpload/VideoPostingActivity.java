package com.india.flipclip.Home.Botttom.VideoUpload;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;

import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.HttpClient;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.india.flipclip.Glob;
import com.india.flipclip.R;
import com.india.flipclip.Util.AndroidMultiPartEntity;
import com.india.flipclip.Util.Common;
import com.india.flipclip.Util.PrefManager;
import com.india.flipclip.Util.VolleyMultipartRequest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

//import static com.india.flipclip.Home.HomeActivity.folder;

public class VideoPostingActivity extends AppCompatActivity {
    private static final String TAG = "VideoUPLOAD";
    EditText description_edit;
    ProgressDialog progressDialog;
    String video_path;
    ImageView video_thumbnail;
    String login_userid;
    ProgressBar progres_bar;

    PrefManager prefManager;
    private String attachedFile;
    private long totalSize = 0;
    private ProgressBar progress_bar1;
    TextView tv_num;
//    private UploadFileToServer uploadFileToServer;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_post__video_);

        prefManager = new PrefManager(this);
        video_path = getIntent().getStringExtra("path1");

        login_userid = prefManager.getRegId();

//        this.video_path = Glob.output_filter_file;
        Log.e("video_path", login_userid);
        progres_bar = findViewById(R.id.progres_bar);
        this.video_thumbnail = (ImageView) findViewById(R.id.video_thumbnail);
        this.description_edit = (EditText) findViewById(R.id.description_edit);
        Bitmap createVideoThumbnail = ThumbnailUtils.createVideoThumbnail(this.video_path, 2);
        if (createVideoThumbnail != null) {
            this.video_thumbnail.setImageBitmap(createVideoThumbnail);
        }
        this.progressDialog = new ProgressDialog(this);
        this.progressDialog.setMessage("Please wait");
        this.progressDialog.setCancelable(false);
        findViewById(R.id.Goback).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                VideoPostingActivity.this.onBackPressed();
            }
        });
        findViewById(R.id.post_btn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                if (description_edit.getText().toString().isEmpty()) {
                    Toast.makeText(VideoPostingActivity.this, "Please Description Add", Toast.LENGTH_SHORT).show();
                } else {

                    Uri uri = Uri.parse(video_path);
                    try {
                        uploadvideo(video_path);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
//                    dialog.show();
//                    uploadFileToServer = new UploadFileToServer();
//                    uploadFileToServer.execute();
                }
            }
        });
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    private void uploadvideo(String video_path) throws IOException {
        Common.showProgressDialog(this);
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, Glob.FILE_UPLOAD_URL,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        Common.dismissDialog();
                        try {
                            JSONObject obj = new JSONObject(new String(response.data));
                            Log.e("DDDDD", obj.toString());
//                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Common.dismissDialog();
//                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("id", login_userid);
                params.put("description", description_edit.getText().toString());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() throws IOException {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("uploaded_file", new DataPart(imagename + ".mp4", getFileDataFromDrawable(video_path)));
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);

        requestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {

                Log.e("WQQQ", request.toString());
                Common.dismissDialog();
                finish();


            }
        });
    }

    public byte[] getFileDataFromDrawable(String video_path) throws IOException {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(video_path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] b = new byte[1024];

        for (int readNum; (readNum = fis.read(b)) != -1; ) {
            bos.write(b, 0, readNum);
        }

        byte[] bytes = bos.toByteArray();

        return bytes;
    }


    private String videoToBase64(File file) throws IOException {

        InputStream inputStream = null;
        String encodedFile = "", lastVal;
        try {
            inputStream = new FileInputStream(file);

            byte[] buffer = new byte[1024];//specify the size to allow
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output64.write(buffer, 0, bytesRead);
            }


            output64.close();


            encodedFile = output.toString();

        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        lastVal = encodedFile;
        return lastVal;


//        String encodedString = null;
//
//        InputStream inputStream = null;
//        try {
//            inputStream = new FileInputStream(file);
//        } catch (Exception e) {
//            // TODO: handle exception
//        }
//        byte[] bytes;
//        byte[] buffer = new byte[8192];
//        int bytesRead;
//        ByteArrayOutputStream output = new ByteArrayOutputStream();
//        try {
//            while ((bytesRead = inputStream.read(buffer)) != -1) {
//                output.write(buffer, 0, bytesRead);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        bytes = output.toByteArray();
//        encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
//        Log.e("Strng", encodedString);
//
//        return encodedString;
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e("DDD1", "1");
//            progres_bar.setVisibility(View.VISIBLE);
            progress_bar1.setProgress(0);
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            progress_bar1.setProgress(progress[0]);
            Log.e("DDD1", String.valueOf(progress[0]));
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        private String uploadFile() {
            Log.e("DDD1", "3");
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Glob.FILE_UPLOAD_URL);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
//                                Common.showProgressDialog(Post_Video_A.this);
                                progress_bar1.setProgress((int) ((num / (float) totalSize) * 100));
//                                tv_num.setText(String.valueOf((int) ((num / (float) totalSize) * 100)));
                                Log.e("FILE", String.valueOf((int) ((num / (float) totalSize) * 100)));
                            }
                        });

                File sourceFile = new File(video_path);

                // Adding file data to http body
                entity.addPart("id", new StringBody(login_userid));
                entity.addPart("description", new StringBody(description_edit.getText().toString()));
                entity.addPart("uploaded_file", new FileBody(sourceFile));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("FINAL", "Response from server: " + result);
            finish();
//            dialog.dismiss();
            super.onPostExecute(result);
        }

    }


    public void onStop() {
        super.onStop();
    }

    public void onBackPressed() {
        super.onBackPressed();
        finish();
//        if (uploadFileToServer != null) {
//            Toast.makeText(this, "Wait Sometime Video Upload", Toast.LENGTH_SHORT).show();
//        } else {
//            finish();
//        }
    }


    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

}
