package com.india.flipclip.Home.Botttom.Login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import com.india.flipclip.Model.Example;
import com.india.flipclip.R;
import com.india.flipclip.Model.VideoResponse;
import com.india.flipclip.Rest.API;
import com.india.flipclip.Util.Common;
import com.india.flipclip.Util.PrefManager;

import retrofit2.Call;
import retrofit2.Response;

public class LogInActivity extends AppCompatActivity {
    EditText etUserName, etPassword;
    private ImageView btnLogin;
    String emailInput;
    private Context context;
    PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        context = this;
        prefManager = new PrefManager(LogInActivity.this);

        etPassword = findViewById(R.id.etPassword);
        etUserName = findViewById(R.id.etUserName);
        btnLogin = findViewById(R.id.btnLogin);


        if (prefManager.getRegId().isEmpty()) {
//            Toast.makeText(context, "login first", Toast.LENGTH_SHORT).show();
        } else {
            onBackPressed();
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailInput = etUserName.getText().toString().trim();
                if (etPassword.getText().toString().trim().length() != 0 && etUserName.getText().toString().trim().length() != 0) {
                    if (checkValidations()) {
                        if (Common.isConnectingToInternet(context)) {
                            checkLogin();
                        } else {
                            Common.showToast(context, context.getResources().getString(R.string.no_internet_available));
                        }
                    }
                }
            }
        });

    }

    private void checkLogin() {
        Common.showProgressDialog(context);
        API.user().getLoginResponse(etUserName.getText().toString().trim(),
                etPassword.getText().toString().trim()).enqueue(new retrofit2.Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                Example loginResponse = response.body();
                if (loginResponse.getSuccess() == 1) {

                    String email = loginResponse.getCustomer().get(0).getEmailid();
                    String dob = loginResponse.getCustomer().get(0).getDob();
                    String profile = loginResponse.getCustomer().get(0).getProfile();
                    String gender = loginResponse.getCustomer().get(0).getGender();

                    Log.e("DDD", gender);
                    prefManager.setUserData(etUserName.getText().toString().trim(), gender,
                            email, loginResponse.getCustomer().get(0).getId(), dob, etPassword.getText().toString().trim(), profile);

                    Common.errorLog("user", loginResponse.getCustomer().get(0).getResponse() +
                            etUserName.getText().toString().trim() +
                            etPassword.getText().toString().trim());
                    Common.showToast(context, loginResponse.getCustomer().get(0).getResponse() + "");
                    Common.dismissDialog();
                    finish();

                } else {
//                    Toast.makeText(context, "else", Toast.LENGTH_SHORT).show();
                    if (loginResponse.getSuccess() == 0) {
                        Common.showToast(context, loginResponse.getCustomer().get(0).getResponse() + "");
                        Common.dismissDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
//                Common.showToast (context, "Password does not match");
                Common.dismissDialog();
            }
        });

    }


    private boolean checkValidations() {
//        if (!Common.isNotNullEditTextBox (etUserName)) {
//            etUserName.requestFocus ();
//            Common.showToast (context, "Please Enter Users Name");
//            return false;
//        } else if (!Common.isValidName (emailInput)) {
//            Common.showToast (context, "Please Insert Valid UserName");
//            return false;
//        }

//        if (!Common.isNotNullEditTextBox (etPassword)) {
//            etPassword.requestFocus ();
//            Common.showToast (context, "Please Enter Password");
//            return false;
//        } else {
//            if (etPassword.getText ().toString ().trim ().length () != 8 && etPassword.getText ().toString ().trim ().length () < 8) {
//                etPassword.requestFocus ();
//                Common.showToast (context, "You must have 8 characters in your password");
//                return false;
//            }
//        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
