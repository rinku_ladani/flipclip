package com.india.flipclip.Home.Botttom.Login;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.Wave;
import com.india.flipclip.Glob;
import com.india.flipclip.R;
import com.india.flipclip.Util.AndroidMultiPartEntity;
import com.india.flipclip.Util.Common;
import com.india.flipclip.Util.PrefManager;
import com.india.flipclip.Util.VolleyMultipartRequest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class SignInActivity extends AppCompatActivity {
    private static final int RESULT_LOAD_IMAGE = 01;
    private static final String TAG = "ARPITA";
    private LoginButton facebook_login;
    private CallbackManager callbackManager;
    private ImageView btnSubmit;
    private EditText etUserName, etDob, etEmail, etPassword, etConfPassword;
    private String emailInput, firstNameInput, dobInput, mobileInput, passwordInput;
    private PrefManager prefManager;
    private Context context;
    private int mYear, mMonth, mDay;
    private DatePickerDialog datePickerDialog;
    private String currentDate = "", gender = "Male";
    private RadioGroup sg_gender;
    CircleImageView img_profile;
    private String img = "";
    private Dialog dialog;
    private ProgressBar progress_bar1;
    private long totalSize = 0;
    //    private UploadFileToServer uploadFileToServer;
    private String currentDatepass;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        context = this;
        btnSubmit = findViewById(R.id.btnSubmit);
        etUserName = findViewById(R.id.etFirstName);
        etDob = findViewById(R.id.etDob);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etConfPassword = findViewById(R.id.etConfPassword);
        sg_gender = (RadioGroup) findViewById(R.id.sg_gender);
        img_profile = findViewById(R.id.img_profile);

        prefManager = new PrefManager(SignInActivity.this);

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.video_upload_progress);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawableResource(17170445);

        progress_bar1 = dialog.findViewById(R.id.progress_bar1);
        Sprite doubleBounce = new Wave();
        progress_bar1.setIndeterminateDrawable(doubleBounce);


        etDob.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                etDob.setRawInputType(InputType.TYPE_NULL);
                etDob.setFocusable(true);
                if (b)
                    selDate();
            }
        });

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });


        etDob.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
            @Override
            public void onClick(View view) {
                etDob.setRawInputType(InputType.TYPE_NULL);
                etDob.setFocusable(true);
                selDate();
            }
        });
        sg_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId) {
                    case R.id.btn_male:
                        gender = "Male";
//                        Common.showToast (context, "Male");
                        break;
                    case R.id.btn_female:
                        gender = "Female";
//                        Common.showToast (context, "Female");
                        break;
                    default:
                        // Nothing to do
                }
            }
        });


//        facebook_login = findViewById (R.id.facebook_login);

//        loginWithFb ();
       /* lnl_facebook_login.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View view) {
                facebook_login.callOnClick ();
            }
        });*/


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstNameInput = etUserName.getText().toString().trim();
                dobInput = currentDatepass;
                emailInput = etEmail.getText().toString().trim();
                passwordInput = etPassword.getText().toString().trim();

                if (checkValidations()) {

                    if (Common.isConnectingToInternet(context)) {
//                        if (img == null && img.isEmpty()) {
//                            Toast.makeText(context, "Please Profile Upload", Toast.LENGTH_SHORT).show();
//                        } else {
                        addRegistration();
//                        }
                    } else {
                        Common.showToast(context, context.getResources().getString(R.string.no_internet_available));
                    }
                }
            }
        });
    }


    private void selDate() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String month = null, date;
                        if (monthOfYear < 10) {
                            month = "0" + (monthOfYear + 1);
                        } else {
                            month = "" + (monthOfYear + 1);
                        }
                        if (dayOfMonth < 10) {
                            date = "0" + dayOfMonth;
                        } else {
                            date = "" + dayOfMonth;
                        }
                        currentDatepass = year + "-" + month + "-" + date;
                        currentDate = date + "-" + month + "-" + year;
                        etDob.setText(currentDate);
                        Log.e("DATE", "onDateSet: " + etDob.getText().toString().trim());
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void addRegistration() {
//        dialog.show();

        uploadimageprofile(bitmap, etUserName.getText().toString(), currentDatepass, etEmail.getText().toString(), etPassword.getText().toString(), gender);

//        uploadFileToServer = new UploadFileToServer();
//        uploadFileToServer.execute();
//        Common.showProgressDialog(context);
//        API.user().getRegistrationResponse(etUserName.getText().toString().trim(), etEmail.getText().toString().trim(),
//                etPassword.getText().toString().trim(), gender, etDob.getText().toString().trim()
//        ).enqueue(new retrofit2.Callback<Example>() {
//            @Override
//            public void onResponse(Call<Example> call, Response<Example> response) {
//                Example example = response.body();
//                if (example.getSuccess() == 1) {
//                    prefManager.setUserData(etUserName.getText().toString().trim(), gender,
//                            etEmail.getText().toString().trim(), "", etDob.getText().toString().trim(), etPassword.getText().toString().trim());
//
//                    Common.errorLog("DATA", "" + etUserName.getText().toString().trim() + etEmail.getText().toString().trim() +
//                            etPassword.getText().toString().trim() + gender + etDob.getText().toString().trim());
//                    Common.showToast(context, example.getCustomer().get(0).getResponse());
//                    finish();
//                } else if (example.getSuccess() == 0) {
//                    Common.showToast(context, "Users Registration successfully");
//                }
//                Common.dismissDialog();
//            }
//
//            @Override
//            public void onFailure(Call<Example> call, Throwable t) {
//                Common.dismissDialog();
//                Common.showToast(context, "Please try again later!!");
//            }
//        });
    }


//    private void uploadimageprofile(Bitmap bitmap, String username, String currentDatepass, String email, String password, String gender) {
//        Common.showProgressDialog(this);
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//        byte[] imageBytes = baos.toByteArray();
//        final String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
//
//        StringRequest request = new StringRequest(Request.Method.POST, Glob.IMAGE_UPLOAD_URL, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String s) {
//                Common.dismissDialog();
//                Log.e("WWWWWW", s.toString());
////                if (s.equals("true")) {
////                    Toast.makeText(SignInActivity.this, "Uploaded Successful", Toast.LENGTH_LONG).show();
////                } else {
////                    Toast.makeText(SignInActivity.this, "Some error occurred!", Toast.LENGTH_LONG).show();
////                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                Common.dismissDialog();
//                Toast.makeText(SignInActivity.this, "Some error occurred -> " + volleyError, Toast.LENGTH_LONG).show();
//            }
//        }) {
//            //adding parameters to send
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> parameters = new HashMap<String, String>();
//                parameters.put("username", username);
//                parameters.put("emailid", email);
//                parameters.put("password", password);
//                parameters.put("gender", gender);
//                parameters.put("dob", currentDatepass);
//                parameters.put("uploaded_file", imageString);
//                return parameters;
//            }
//        };
//
//        RequestQueue rQueue = Volley.newRequestQueue(SignInActivity.this);
//        rQueue.add(request);
//    }


    private void uploadimageprofile(Bitmap bitmap, String username, String currentDatepass, String email, String password, String gender) {
        Common.showProgressDialog(this);
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, Glob.IMAGE_UPLOAD_URL,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        Common.dismissDialog();
                        try {
                            JSONObject obj = new JSONObject(new String(response.data));
                            Log.e("DDDDD", obj.toString());
                            prefManager.setUserData(etUserName.getText().toString().trim(), gender,
                                    etEmail.getText().toString().trim(), "", currentDatepass, etPassword.getText().toString().trim(), img);

                            Intent intent = new Intent(SignInActivity.this, LogInActivity.class);
                            startActivity(intent);
//                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Common.dismissDialog();
//                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                entity.addPart("username", new StringBody(etUserName.getText().toString().trim()));
//                entity.addPart("emailid", new StringBody(etEmail.getText().toString().trim()));
//                entity.addPart("password", new StringBody(etPassword.getText().toString().trim()));
//                entity.addPart("gender", new StringBody(gender));
//                entity.addPart("dob", new StringBody(currentDatepass));
//                entity.addPart("uploaded_file", new FileBody(sourceFile));
                params.put("username", username);
                params.put("emailid", email);
                params.put("password", password);
                params.put("gender", gender);
                params.put("dob", currentDatepass);
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("uploaded_file", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(volleyMultipartRequest);

        requestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {

                Log.e("WQQQ", request.toString());
                Common.dismissDialog();




            }
        });
        //adding the request to volley
//        Volley.newRequestQueue(this).add(volleyMultipartRequest);

    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e("DDD1", "1");
//            progres_bar.setVisibility(View.VISIBLE);
            progress_bar1.setProgress(0);
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            progress_bar1.setProgress(progress[0]);
            Log.e("DDD1", String.valueOf(progress[0]));
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        private String uploadFile() {
            Log.e("DDD1", "3");
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Glob.IMAGE_UPLOAD_URL);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
//                                Common.showProgressDialog(Post_Video_A.this);
                                progress_bar1.setProgress((int) ((num / (float) totalSize) * 100));
//                                tv_num.setText(String.valueOf((int) ((num / (float) totalSize) * 100)));
                                Log.e("FILE", String.valueOf((int) ((num / (float) totalSize) * 100)));
                            }
                        });

                File sourceFile = new File(img);
//                prefManager.setUserData(etUserName.getText().toString().trim(),
//                        gender,
//                        etEmail.getText().toString().trim(),
//                        "", etDob.getText().toString().trim(),
//                        etPassword.getText().toString().trim());

                // Adding file data to http body
                entity.addPart("username", new StringBody(etUserName.getText().toString().trim()));
                entity.addPart("emailid", new StringBody(etEmail.getText().toString().trim()));
                entity.addPart("password", new StringBody(etPassword.getText().toString().trim()));
                entity.addPart("gender", new StringBody(gender));
                entity.addPart("dob", new StringBody(currentDatepass));
                entity.addPart("uploaded_file", new FileBody(sourceFile));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e(TAG, "Response from server: " + result);
            String imageget = "";
            prefManager.setUserData(etUserName.getText().toString().trim(), gender,
                    etEmail.getText().toString().trim(), "", currentDatepass, etPassword.getText().toString().trim(), img);
//            if (folder.isDirectory()) {
//                String[] children = folder.list();
//                for (int i = 0; i < children.length; i++) {
//                    new File(folder, children[i]).delete();
//                }
//            }
            finish();
            // showing the server response in an alert dialog
//            showAlert(result);
            dialog.dismiss();
            super.onPostExecute(result);
//            Common.dismissDialog();
        }

    }

    private boolean checkValidations() {

        if (bitmap == null) {
            etUserName.requestFocus();
            Common.showToast(context, "Please Profile Upload");
            return false;
        } else if (!Common.isValidName(firstNameInput)) {
            Common.showToast(context, "Something Wrong");
            return false;
        }

        if (!Common.isNotNullEditTextBox(etUserName)) {
            etUserName.requestFocus();
            Common.showToast(context, "Please Enter Users Name");
            return false;
        } else if (!Common.isValidName(firstNameInput)) {
            Common.showToast(context, "First Name not allow space");
            return false;
        }

       /* if (!Common.isNotNullEditTextBox (etDob)) {
            etDob.requestFocus ();
            Common.showToast (context, "Please Select DOB");
            return false;
        } else if (!Common.isValidName (dobInput)) {
            Common.showToast (context, "Please Date of Birth");
            return false;
        }*/

        if (!Common.isNotNullEditTextBox(etEmail)) {
            etEmail.requestFocus();
            Common.showToast(context, "Please Enter EmailId");
            return false;
        } else if (!Common.isValidateEmail(emailInput)) {
            Common.showToast(context, "Please Insert Valid EmailId");
            return false;
        }

        if (!Common.isNotNullEditTextBox(etPassword)) {
            etPassword.requestFocus();
            Common.showToast(context, "Please Enter Password");
            return false;
        } else {
            if (etPassword.getText().toString().trim().length() != 8 && etPassword.getText().toString().trim().length() < 8) {
                etPassword.requestFocus();
                Common.showToast(context, "You must have 8 characters in your password");
                return false;
            }
        }

        if (!Common.isNotNullEditTextBox(etConfPassword)) {
            etConfPassword.requestFocus();
            Common.showToast(context, "Please Enter Confirm Password");
            return false;
        } else {
            if (!etPassword.getText().toString().trim().equals(etConfPassword.getText().toString().trim())) {
                etConfPassword.requestFocus();
                Common.showToast(context, "password and confirm password must be same!!!");
                return false;
            }
        }

//        if (img == null) {
//            Toast.makeText(context, "Please Profile Pic Upload", Toast.LENGTH_SHORT).show();
//            return false;
//        }

        return true;
    }

    private void loginWithFb() {
        callbackManager = CallbackManager.Factory.create();
        facebook_login.setReadPermissions(Arrays.asList("email", "public_profile"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.e("succ", "onSuccess: " + loginResult.getAccessToken().getToken());
                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.e("resp", "onCompleted: " + object.toString());
                        String email = "";
                        try {
                            if (object.has("email")) {
                                email = object.getString("email");
                                Log.e("email", "onCompleted: " + email);
                            }
//                            fbLoginAction();
                            LoginManager.getInstance().logOut();
                        } catch (Exception e) {
                            Log.e("ex", "onCompleted: " + e.getMessage());
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri imageUri = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                img_profile.setImageBitmap(bitmap);

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(imageUri, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                img = cursor.getString(columnIndex);
                cursor.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        if (uploadFileToServer != null) {
//            Toast.makeText(context, "Wait SomeTime", Toast.LENGTH_SHORT).show();
//        } else {
//            finish();
//        }
    }
}
