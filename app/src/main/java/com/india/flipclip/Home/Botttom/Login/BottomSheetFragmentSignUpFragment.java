package com.india.flipclip.Home.Botttom.Login;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.india.flipclip.R;

public class BottomSheetFragmentSignUpFragment extends Fragment {

    ImageView img_usernamelogin;
    ImageView Goback;
    RelativeLayout relative_sign;
    TextView tv_login;
    RelativeLayout relative_login;
    ImageView img_usernamelogin_1;
    TextView tv_login_1;
    Fragment context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);

        context = this;
        img_usernamelogin = view.findViewById(R.id.img_usernamelogin);
        Goback = view.findViewById(R.id.Goback);
        relative_sign = view.findViewById(R.id.relative_sign);
        tv_login = view.findViewById(R.id.tv_login);
        relative_login = view.findViewById(R.id.relative_login);
        img_usernamelogin_1 = view.findViewById(R.id.img_usernamelogin_1);
        tv_login_1 = view.findViewById(R.id.tv_login_1);

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                relative_sign.setVisibility(View.GONE);
                relative_login.setVisibility(View.VISIBLE);
            }
        });

        tv_login_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                relative_sign.setVisibility(View.VISIBLE);
                relative_login.setVisibility(View.GONE);
            }
        });
        img_usernamelogin_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), LogInActivity.class);
                startActivity(intent);
            }
        });

        Goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                getActivity().getFragmentManager().popBackStackImmediate();
//                getActivity().getFragmentManager().beginTransaction().remove(me).commit();
                getActivity().onBackPressed();
            }
        });
        img_usernamelogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SignInActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }
}
