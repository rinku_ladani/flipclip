package com.india.flipclip.Home.Botttom.VideoUpload;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.coremedia.iso.boxes.Container;
import com.coremedia.iso.boxes.MovieHeaderBox;
import com.daasuu.gpuv.composer.GPUMp4Composer;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.upstream.DefaultLoadErrorHandlingPolicy;
import com.googlecode.mp4parser.DataSource;
import com.googlecode.mp4parser.FileDataSourceImpl;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.CroppedTrack;
import com.googlecode.mp4parser.util.Matrix;
import com.googlecode.mp4parser.util.Path;
import com.india.flipclip.Glob;
import com.india.flipclip.Model.GalleryVideoModel;
import com.india.flipclip.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

//import static com.india.flipclip.Home.HomeActivity.folder;

public class GalleryVideoActivity extends AppCompatActivity {

    GalleryVideos_Adapter adapter;
    ArrayList<GalleryVideoModel> data_list;
    public RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_video);

//        if (folder.isDirectory()) {
//            String[] children = folder.list();
//            for (int i = 0; i < children.length; i++) {
//                new File(folder, children[i]).delete();
//            }
//        }


        this.data_list = new ArrayList<>();
        this.recyclerView = (RecyclerView) findViewById(R.id.recylerview);
        this.recyclerView.setHasFixedSize(true);
        this.recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        this.data_list = new ArrayList<>();
        this.adapter = new GalleryVideos_Adapter(this, this.data_list, new GalleryVideos_Adapter.OnItemClickListener() {
            public void onItemClick(int i, GalleryVideoModel galleryVideo_Model, View view) {
                if (galleryVideo_Model.getVideo_duration_ms() < 19500) {
                    GalleryVideoActivity.this.Chnage_Video_size(galleryVideo_Model.getVideo_path(), Glob.gallery_resize_video);
                    return;
                }
                try {
                    GalleryVideoActivity.this.startTrim(new File(galleryVideo_Model.getVideo_path()), new File(Glob.gallery_trimed_video), 1000, 18000);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        this.recyclerView.setAdapter(this.adapter);
        getAllVideoPath(this);
        findViewById(R.id.Goback).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                GalleryVideoActivity.this.finish();
//                GalleryVideoActivity.this.overridePendingTransition(R.anim.in_from_top, R.anim.out_from_bottom);
            }
        });
    }

    public void getAllVideoPath(Context context) {
        Cursor query = context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, new String[]{"_data", "duration", "_data"}, null, null, "date_modified desc");
        if (query != null) {
            while (query.moveToNext()) {
                GalleryVideoModel galleryVideo_Model = new GalleryVideoModel();
                galleryVideo_Model.setVideo_path(query.getString(0));
                galleryVideo_Model.setVideo_duration_ms(query.getLong(1));
                galleryVideo_Model.setThumb_path(query.getString(2));
                StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(galleryVideo_Model.getVideo_duration_ms());
                Log.d("resp", sb.toString());
                if (galleryVideo_Model.getVideo_duration_ms() > DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS) {
                    galleryVideo_Model.setVideo_time(change_sec_to_time(galleryVideo_Model.getVideo_duration_ms()));
                    this.data_list.add(galleryVideo_Model);
                }
            }
            this.adapter.notifyDataSetChanged();
            query.close();
        }
    }

    public long getfileduration(Uri uri) {
        try {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(this, uri);
            return (long) Integer.parseInt(mediaMetadataRetriever.extractMetadata(9));
        } catch (Exception unused) {
            return 0;
        }
    }

    public String change_sec_to_time(long j) {
        return String.format("%02d:%02d", new Object[]{Long.valueOf((j / DefaultLoadErrorHandlingPolicy.DEFAULT_TRACK_BLACKLIST_MS) % 60), Long.valueOf((j / 1000) % 60)});
    }

    public void Chnage_Video_size(String str, String str2) {
        Log.e("aa", str);
        Log.e("aa1", str2);
        Glob.Show_determinent_loader(this, false, false);
        new GPUMp4Composer(str, str2).size(540, 960).videoBitrate(2073600).listener(new GPUMp4Composer.Listener() {
            public void onProgress(double d) {
                StringBuilder sb = new StringBuilder();
                sb.append("");
                int i = (int) (d * 100.0d);
                sb.append(i);
                Log.d("resp", sb.toString());
                Glob.Show_loading_progress(i);
            }

            public void onCompleted() {
                GalleryVideoActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Glob.cancel_determinent_loader();
                        Intent intent = new Intent(GalleryVideoActivity.this, GallerySelectedVideo_A.class);
                        intent.putExtra("video_path", Glob.gallery_resize_video);
                        GalleryVideoActivity.this.startActivity(intent);
                    }
                });
            }

            public void onCanceled() {
                Log.d("resp", "onCanceled");
            }

            public void onFailed(Exception exc) {
                Log.d("resp", exc.toString());
                GalleryVideoActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            Glob.cancel_determinent_loader();
                            Toast.makeText(GalleryVideoActivity.this, "Try Again", 0).show();
                        } catch (Exception unused) {
                        }
                    }
                });
            }
        }).start();
    }


    @Override
    protected void onResume() {
//        if (folder.isDirectory()) {
//            String[] children = folder.list();
//            for (int i = 0; i < children.length; i++) {
//                new File(folder, children[i]).delete();
//            }
//        }
        super.onResume();
    }

    public void startTrim(File file, File file2, int i, int i2) throws IOException {
        final File file3 = file;
        final int i3 = i;
        final int i4 = i2;
        final File file4 = file2;
        AsyncTask r0 = new AsyncTask<String, Void, String>() {
            /* access modifiers changed from: protected */
            public String doInBackground(String... strArr) {
                FileDataSourceImpl fileDataSourceImpl = null;
                FileOutputStream fileOutputStream = null;
                FileChannel channel = null;
                try {
                    fileDataSourceImpl = new FileDataSourceImpl(file3);
                    Movie build = MovieCreator.build((DataSource) fileDataSourceImpl);
                    List<Track> tracks = build.getTracks();
                    build.setTracks(new LinkedList());
                    double d = (double) (i3 / 1000);
                    double d2 = (double) (i4 / 1000);
                    double d3 = d2;
                    double d4 = d;
                    boolean z = false;
                    for (Track track : tracks) {
                        if (track.getSyncSamples() != null && track.getSyncSamples().length > 0) {
                            if (!z) {
                                double correctTimeToSyncSample = Glob.correctTimeToSyncSample(track, d4, false);
                                d3 = Glob.correctTimeToSyncSample(track, d3, true);
                                d4 = correctTimeToSyncSample;
                                z = true;
                            } else {
                                throw new RuntimeException("The startTime has already been corrected by another track with SyncSample. Not Supported.");
                            }
                        }
                    }
                    for (Track track2 : tracks) {
                        long j = 0;
                        double d5 = 0.0d;
                        long j2 = -1;
                        long j3 = -1;
                        int i = 0;
                        while (i < track2.getSampleDurations().length) {
                            if (d5 <= d4) {
                                j2 = j;
                            }
                            if (d5 > d3) {
                                break;
                            }
                            d5 += ((double) track2.getSampleDurations()[i]) / ((double) track2.getTrackMetaData().getTimescale());
                            i++;
                            j3 = j;
                            j = 1 + j;
                            d3 = d3;
                        }
                        double d6 = d3;
                        CroppedTrack croppedTrack = new CroppedTrack(track2, j2, j3);
                        build.addTrack(croppedTrack);
                        d3 = d6;
                    }
                    Container build2 = new DefaultMp4Builder().build(build);
                    ((MovieHeaderBox) Path.getPath(build2, "moov/mvhd")).setMatrix(Matrix.ROTATE_180);
                    if (!file4.exists()) {
                        file4.createNewFile();
                    }
                    fileOutputStream = new FileOutputStream(file4);
                    channel = fileOutputStream.getChannel();
                    build2.writeContainer(channel);
                    channel.close();
                    fileOutputStream.close();
                    fileDataSourceImpl.close();
                    fileDataSourceImpl.close();
                    return "Ok";
                } catch (Exception unused) {
                    return "error";
                } catch (Throwable th) {
                    Throwable th2 = th;
                    try {
                        channel.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        fileDataSourceImpl.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return "";
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
                Glob.Show_indeterminent_loader(GalleryVideoActivity.this, true, true);
            }

            /* access modifiers changed from: protected */
            @SuppressLint("StaticFieldLeak")
            public void onPostExecute(String str) {
                if (str.equals("error")) {
                    Toast.makeText(GalleryVideoActivity.this, "Try Again", 0).show();
                    return;
                }
                Glob.cancel_indeterminent_loader();
                GalleryVideoActivity.this.Chnage_Video_size(Glob.gallery_trimed_video, Glob.gallery_resize_video);
            }
        };
        r0.execute(new String[0]);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
//        DeleteFile();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        DeleteFile();
    }

    public void DeleteFile() {
        File file = new File(Glob.outputfile);
        File file2 = new File(Glob.outputfile2);
        File file3 = new File(Glob.output_filter_file);
        File file4 = new File(Glob.gallery_trimed_video);
        File file5 = new File(Glob.gallery_resize_video);
        if (file.exists()) {
            file.delete();
        }
        if (file2.exists()) {
            file2.delete();
        }
        if (file3.exists()) {
            file3.delete();
        }
        if (file4.exists()) {
            file4.delete();
        }
        if (file5.exists()) {
            file5.delete();
        }
    }
}

