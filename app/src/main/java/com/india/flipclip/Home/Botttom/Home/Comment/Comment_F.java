package com.india.flipclip.Home.Botttom.Home.Comment;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.india.flipclip.Model.Customer;
import com.india.flipclip.Model.Example;
import com.india.flipclip.R;
import com.india.flipclip.Rest.API;
import com.india.flipclip.Util.Common;
import com.india.flipclip.Util.PrefManager;


import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * ARPITA AKBARI
 **/

public class Comment_F extends Fragment {

    Comments_Adapter adapter;
    TextView comment_count_txt;
    Context context;
    ArrayList<Customer> data_list;
    EditText message_edit;
    RecyclerView recyclerView;
    ImageButton send_btn;
    TextView no_comment;
    ProgressBar send_progress;
    String user_id;
    String video_id;
    View view;
    private PrefManager prefManager;
    private String comment_total;
    private String comment;

    public Comment_F() {
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.view = layoutInflater.inflate(R.layout.fragment_comment, viewGroup, false);
        this.context = getContext();
//        this.comment_screen = (FrameLayout) this.view.findViewById(R.id.comment_screen);
//        this.comment_screen.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                Comment_F.this.getActivity().onBackPressed();
//            }
//        });

        Bundle arguments = getArguments();
        if (arguments != null) {
            video_id = arguments.getString("video_id");
            comment_total = arguments.getString("comment_total");
            prefManager = new PrefManager(getActivity());
            user_id = prefManager.getRegId();
        }

        no_comment = view.findViewById(R.id.no_comment);
        comment_count_txt = (TextView) view.findViewById(R.id.comment_count);
        recyclerView = (RecyclerView) view.findViewById(R.id.recylerview);
        recyclerView.setHasFixedSize(true);

        view.findViewById(R.id.Goback).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Comment_F.this.getActivity().onBackPressed();
            }
        });

        message_edit = (EditText) view.findViewById(R.id.message_edit);
        send_progress = (ProgressBar) view.findViewById(R.id.send_progress);
        send_btn = (ImageButton) view.findViewById(R.id.send_btn);
        this.send_btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String comment = Comment_F.this.message_edit.getText().toString();
                if (TextUtils.isEmpty(comment)) {
                    return;
                }
//                if (Variables.sharedPreferences.getBoolean(Variables.islogin, false)) {
                Comment_F.this.Send_Comments(user_id, Comment_F.this.video_id, comment);
                Comment_F.this.message_edit.setText(null);
                Comment_F.this.send_progress.setVisibility(0);
//                Comment_F.this.send_btn.setVisibility(8);
//                    return;
//                }
//                Toast.makeText(Comment_F.this.context, "Please Login into the app", 0).show();
            }
        });
        Get_All_Comments(video_id);
        return view;
    }

    public void Get_All_Comments(String video_id) {
        Common.showProgressDialog(getActivity());
        API.user().getUserComment(video_id).enqueue(new retrofit2.Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                Example videoResponse = response.body();
                if (videoResponse.getSuccess() == 1) {
                    data_list = videoResponse.getCustomer();

                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(data_list.size());
                    stringBuilder.append(" ");
                    stringBuilder.append("Comments");
                    comment_count_txt.setText(stringBuilder.toString());

                    no_comment.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    setAdapter();
                    Common.dismissDialog();
                } else {
                    no_comment.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    Common.dismissDialog();
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
//                Common.showToast(getContext(), "Password does not match");
                Common.dismissDialog();
            }
        });
    }


    private void setAdapter() {
        adapter = new Comments_Adapter(context, data_list, new Comments_Adapter.OnItemClickListener() {
            public void onItemClick(int i, Customer comment_Get_Set, View view) {
//                Toast.makeText(context, "hiii", Toast.LENGTH_SHORT).show();
            }
        });
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
    }

    public void Send_Comments(String user_id, String video_id, final String comment) {
        Common.showProgressDialog(getActivity());
        API.user().Usercommentset(user_id, video_id, comment).enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                Example videoResponse = response.body();
                if (videoResponse.getSuccess() == 1) {
                    Common.dismissDialog();
                    Comment_F.this.getActivity().onBackPressed();
//                    Common.dismissDialog();
                    Log.e("w", "w");
                } else {
                    if (videoResponse.getSuccess() == 0) {
                        Log.e("f", "f");
                        Comment_F.this.getActivity().onBackPressed();
                        Common.dismissDialog();
                    }
                }
//                Common.dismissDialog();
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                Log.e("wr", "wr");
                Comment_F.this.getActivity().onBackPressed();
//                Common.showToast(getContext(), "Something Worng");
                Common.dismissDialog();
            }

        });
        Comment_F.this.send_progress.setVisibility(8);
        Comment_F.this.send_btn.setVisibility(0);
//        Comment_F.this.send_progress.setVisibility(8);
//        Comment_F.this.send_btn.setVisibility(0);
//        Functions.Call_Api_For_Send_Comment(getActivity(), str, str2, new API_CallBack() {
//            public void OnFail(String str) {
//            }
//
//            public void OnSuccess(String str) {
//            }
//
//            public void ArrayData(ArrayList arrayList) {
//                Iterator it = arrayList.iterator();
//                while (it.hasNext()) {
//                    Comment_F.this.data_list.add(0, (Customer) it.next());
//                    Comment_F.comment_count++;
//                    Comment_F.this.SendPushNotification(Comment_F.this.getActivity(), Comment_F.this.user_id, str2);
//                    TextView textView = Comment_F.this.comment_count_txt;
//                    StringBuilder sb = new StringBuilder();
//                    sb.append(Comment_F.comment_count);
//                    sb.append(" comments");
//                    textView.setText(sb.toString());
////                    if (Comment_F.this.fragment_data_send != null) {
////                        Fragment_Data_Send fragment_Data_Send = Comment_F.this.fragment_data_send;
////                        StringBuilder sb2 = new StringBuilder();
////                        sb2.append("");
////                        sb2.append(Comment_F.comment_count);
////                        fragment_Data_Send.onDataSent(sb2.toString());
////                    }
//                }
//                Comment_F.this.adapter.notifyDataSetChanged();
//                Comment_F.this.send_progress.setVisibility(8);
//                Comment_F.this.send_btn.setVisibility(0);
//            }
//        });
    }

//    public void SendPushNotification(Activity activity, String str, String str2) {
//        JSONObject jSONObject = new JSONObject();
//        String str3 = "title";
//        try {
//            StringBuilder sb = new StringBuilder();
//            sb.append(Variables.sharedPreferences.getString(Variables.u_name, ""));
//            sb.append(" Comment on your video");
//            jSONObject.put(str3, sb.toString());
//            jSONObject.put("message", str2);
//            jSONObject.put(SettingsJsonConstants.APP_ICON_KEY, Variables.sharedPreferences.getString(Variables.u_pic, ""));
//            jSONObject.put("senderid", Variables.sharedPreferences.getString(Variables.u_id, ""));
//            jSONObject.put("receiverid", str);
//            jSONObject.put(ShareConstants.WEB_DIALOG_PARAM_ACTION_TYPE, "comment");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        ApiRequest.Call_Api(this.context, Variables.sendPushNotification, jSONObject, null);
//    }
//}

}
