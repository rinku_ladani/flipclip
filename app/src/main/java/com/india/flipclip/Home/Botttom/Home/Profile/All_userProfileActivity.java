package com.india.flipclip.Home.Botttom.Home.Profile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.india.flipclip.Home.Botttom.Login.LogInActivity;
import com.india.flipclip.Home.Botttom.Login.SignInActivity;
import com.india.flipclip.R;
import com.india.flipclip.Model.Customer;
import com.india.flipclip.Model.Example;
import com.india.flipclip.Rest.API;
import com.india.flipclip.Util.Common;
import com.india.flipclip.Util.PrefManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

public class All_userProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView btnLogin;
    private TextView tv_sign_up;
    private RelativeLayout lnl_login;
    private PrefManager prefManager;
    private RelativeLayout rnl_profile;

    ImageView back_btn;
    Context context;
    public TextView fans_count_txt;
    public TextView follow_count_txt;
    public TextView follow_unfollow_btn;
    public TextView heart_count_txt;
    public ImageView imageView;
    public boolean isdataload = false;
    ImageView setting_btn;
    RelativeLayout tabs_main_layout;
    LinearLayout top_layout;
    String user_id;
    String uname;
    public TextView username;
    public TextView video_count_txt;
    LinearLayout following_layout;
    LinearLayout fans_layout;
    RecyclerView recyclerView;
    private ArrayList<Customer> mediaObjectList;
    private RelativeLayout no_data_layout;
    MyVideos_Adapter adapter;
    String videocount = "";
    String userpic = "";
    CardView btn_following;
    TextView tv_following;

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_btn /*2131296318*/:
                onBackPressed();
                return;
            case R.id.fans_layout /*2131296429*/:
                getFollowers();

                return;
            case R.id.follow_unfollow_btn /*2131296445*/:
               /* if (Variables.sharedPreferences.getBoolean(Variables.islogin, false)) {
                    Follow_unFollow_User();
                    return;
                } else {
                    Toast.makeText(this.context, "Please login in to app", 0).show();
                    return;
                }*/
            case R.id.following_layout /*2131296446*/:
/*
                Open_Following();
*/
                return;
            case R.id.setting_btn /*2131296623*/:
/*
                Open_Setting();
*/
                return;
            case R.id.user_image /*2131296718*/:
/*
                OpenfullsizeImage(pic_url);
*/
                return;
            default:
                return;
        }


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_user_profile);

//        init();

        prefManager = new PrefManager(this);

        btnLogin = findViewById(R.id.btnLogin);
        tv_sign_up = findViewById(R.id.tv_sign_up);
        lnl_login = findViewById(R.id.lnl_login);
        rnl_profile = findViewById(R.id.rnl_profile);
        this.no_data_layout = (RelativeLayout) findViewById(R.id.no_data_layout);
        btn_following = findViewById(R.id.btn_following);
        tv_following = findViewById(R.id.tv_following);

        recyclerView = findViewById(R.id.rcy_uerall);
        this.recyclerView.setLayoutManager(new GridLayoutManager(this.context, 3));
        this.recyclerView.setHasFixedSize(true);

        this.username = (TextView) findViewById(R.id.username);
        this.imageView = (ImageView) findViewById(R.id.user_image);
        this.imageView.setOnClickListener(this);
        this.video_count_txt = (TextView) findViewById(R.id.video_count_txt);
        this.follow_count_txt = (TextView) findViewById(R.id.follow_count_txt);
        this.fans_count_txt = (TextView) findViewById(R.id.fan_count_txt);
        this.heart_count_txt = (TextView) findViewById(R.id.heart_count_txt);
        this.setting_btn = (ImageView) findViewById(R.id.setting_btn);
        this.setting_btn.setOnClickListener(this);
        this.back_btn = (ImageView) findViewById(R.id.back_btn);
        this.back_btn.setOnClickListener(this);
        this.follow_unfollow_btn = (TextView) findViewById(R.id.follow_unfollow_btn);
        this.follow_unfollow_btn.setOnClickListener(this);
        //        this.tabLayout = (TabLayout) findViewById(R.id.tabs);
//        this.pager = (ViewPager) findViewById(R.id.pager);
//        this.pager.setOffscreenPageLimit(2);
//        this.adapter = new ViewPagerAdapter(getResources(), getSupportFragmentManager());
//        this.pager.setAdapter(this.adapter);
//        this.tabLayout.setupWithViewPager(this.pager);
//        setupTabIcons();
        this.tabs_main_layout = (RelativeLayout) findViewById(R.id.tabs_main_layout);
        this.top_layout = (LinearLayout) findViewById(R.id.top_layout);
        this.top_layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                final int measuredHeight = top_layout.getMeasuredHeight();
                top_layout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                tabs_main_layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        ViewGroup.LayoutParams layoutParams = tabs_main_layout.getLayoutParams();
                        layoutParams.height = tabs_main_layout.getMeasuredHeight() + measuredHeight;
                        tabs_main_layout.setLayoutParams(layoutParams);
                        tabs_main_layout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                });
            }
        });
        following_layout = findViewById(R.id.following_layout);
        following_layout.setOnClickListener(this);
        fans_layout = findViewById(R.id.fans_layout);
        fans_layout.setOnClickListener(this);
        this.isdataload = true;

        user_id = getIntent().getStringExtra("userid");
        uname = getIntent().getStringExtra("username");
        userpic = getIntent().getStringExtra("userpic");

        Glide.with(this).load(userpic).placeholder(getDrawable(R.drawable.profile_image_placeholder)).into(imageView);
        Call_Api_For_get_Allvideos(user_id);

        if (prefManager.getUserName() != null && !prefManager.getUserName().equals("")) {
            rnl_profile.setVisibility(View.VISIBLE);
            lnl_login.setVisibility(View.GONE);
            username.setText(uname);
        } else {
            lnl_login.setVisibility(View.VISIBLE);
            rnl_profile.setVisibility(View.GONE);
        }


        if (prefManager.getFollwingUser().equalsIgnoreCase(user_id)) {
            btn_following.setCardBackgroundColor(getResources().getColor(R.color.white));
            tv_following.setText("Following");
            tv_following.setTextColor(getResources().getColor(R.color.black));
            btn_following.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    follwing_api_delete(user_id, prefManager.getRegId());
                }
            });
        } else {
            btn_following.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    follwing_apicall(user_id, prefManager.getRegId());
                }
            });
        }


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(All_userProfileActivity.this, LogInActivity.class));
            }
        });

        tv_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(All_userProfileActivity.this, SignInActivity.class));
            }
        });

        getProfile_data(user_id);
    }

    private void getProfile_data(String user_id) {
        Common.showProgressDialog(All_userProfileActivity.this);
        API.user().getUserProfile_data(user_id).enqueue(new retrofit2.Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                Example videoResponse = response.body();
                if (videoResponse.getSuccess() == 1) {
                    String followers_count = String.valueOf(videoResponse.getFollowersCount());
                    fans_count_txt.setText(followers_count);
                    Common.dismissDialog();
                } else {
                    if (videoResponse.getSuccess() == 0) {
                        Common.dismissDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                Common.dismissDialog();
            }
        });
    }

    private void follwing_api_delete(String followeuser_id, String loginuserid) {
        Common.showProgressDialog(All_userProfileActivity.this);
        API.user().UserFollwingDelete(loginuserid, followeuser_id).enqueue(new retrofit2.Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                Example videoResponse = response.body();
                if (videoResponse.getSuccess() == 1) {
                    btn_following.setCardBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    tv_following.setText("Following");
                    tv_following.setTextColor(getResources().getColor(R.color.white));
                    prefManager.ClearUserFollwing();
                    Common.dismissDialog();
                } else {
                    if (videoResponse.getSuccess() == 0) {
                        Common.dismissDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                Common.dismissDialog();
            }
        });
    }

    private void follwing_apicall(String followeuser_id, String loginuserid) {
        Common.showProgressDialog(All_userProfileActivity.this);
        API.user().UserFollwing(loginuserid, followeuser_id).enqueue(new retrofit2.Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                Example videoResponse = response.body();
                if (videoResponse.getSuccess() == 1) {
                    btn_following.setCardBackgroundColor(getResources().getColor(R.color.white));
                    tv_following.setText("Follower");
                    tv_following.setTextColor(getResources().getColor(R.color.black));
                    prefManager.SetFollwingUser(followeuser_id);
                    Common.dismissDialog();
                } else {
                    if (videoResponse.getSuccess() == 0) {
                        Common.dismissDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                Common.dismissDialog();
            }
        });
    }

    private void Call_Api_For_get_Allvideos(String userid) {
        Common.showProgressDialog(All_userProfileActivity.this);
        API.user().getUserWisevideoget(userid).enqueue(new retrofit2.Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                Example videoResponse = response.body();
                if (videoResponse.getSuccess() == 1) {
                    recyclerView.setVisibility(View.VISIBLE);
                    no_data_layout.setVisibility(View.GONE);
                    mediaObjectList = videoResponse.getCustomer();
                    adapter = new MyVideos_Adapter(All_userProfileActivity.this, mediaObjectList, userpic, uname);
                    recyclerView.setAdapter(adapter);
                    videocount = String.valueOf(mediaObjectList.size());
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(videocount);
                    stringBuilder.append(" ");
                    stringBuilder.append("Video");
                    video_count_txt.setText(stringBuilder.toString());
                    Common.dismissDialog();
                } else {
                    if (videoResponse.getSuccess() == 0) {
                        recyclerView.setVisibility(View.GONE);
                        no_data_layout.setVisibility(View.VISIBLE);
                        Common.dismissDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                Common.dismissDialog();
//                Common.showToast(getApplicationContext(), "Password does not match");
            }
        });

    }


    private void getFollowers() {
        Intent intent = new Intent(All_userProfileActivity.this, Followers_ListActivity.class);
        intent.putExtra("username", uname);
        intent.putExtra("userid", user_id);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
