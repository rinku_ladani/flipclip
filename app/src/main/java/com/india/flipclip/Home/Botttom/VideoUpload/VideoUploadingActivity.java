package com.india.flipclip.Home.Botttom.VideoUpload;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.coremedia.iso.boxes.Container;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;
import com.india.flipclip.Glob;
import com.india.flipclip.R;
import com.india.flipclip.Util.Common;
import com.india.flipclip.Util.Merge_Video_Audio;
import com.india.flipclip.Util.ProgressBarListener;
import com.india.flipclip.Util.SegmentedProgressBar;
import com.wonderkiln.camerakit.CameraKitError;
import com.wonderkiln.camerakit.CameraKitEvent;
import com.wonderkiln.camerakit.CameraKitEventListener;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraKitVideo;
import com.wonderkiln.camerakit.CameraView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import static com.india.flipclip.DemoActivity.folder;

public class VideoUploadingActivity extends AppCompatActivity implements View.OnClickListener {

    public static int Sounds_list_Request_code = 1;
    TextView add_sound_txt;
    MediaPlayer audio;
    CameraView cameraView;
    LinearLayout camera_options;
    int delete_count = 0;
    ImageButton done_btn;
    ImageButton flash_btn;
    boolean is_flash_on = false;
    boolean is_recording = false;
    int number = 0;
    ImageView record_image;
    ImageButton rotate_camera;
    int sec_passed = 0;
    SegmentedProgressBar video_progress;
    ArrayList<String> videopaths = new ArrayList<>();
    private String str;
    private LinearLayout upload_layout;
    private ImageButton Goback;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_uploading);


        this.sec_passed = 0;
        Glob.Selected_sound_id = "null";
        this.cameraView = (CameraView) findViewById(R.id.camera);
        this.camera_options = (LinearLayout) findViewById(R.id.camera_options);
        this.cameraView.addCameraKitListener(new CameraKitEventListener() {
            public void onError(CameraKitError cameraKitError) {
            }

            public void onEvent(CameraKitEvent cameraKitEvent) {
            }

            public void onImage(CameraKitImage cameraKitImage) {
            }

            public void onVideo(CameraKitVideo cameraKitVideo) {
            }
        });
        this.record_image = (ImageView) findViewById(R.id.record_image);
        upload_layout = findViewById(R.id.upload_layout);
        upload_layout.setOnClickListener(this);
        this.done_btn = (ImageButton) findViewById(R.id.done);
        this.done_btn.setEnabled(false);
        this.done_btn.setOnClickListener(this);
        this.rotate_camera = (ImageButton) findViewById(R.id.rotate_camera);
        this.rotate_camera.setOnClickListener(this);
        this.flash_btn = (ImageButton) findViewById(R.id.flash_camera);
        this.flash_btn.setOnClickListener(this);
        Goback = findViewById(R.id.Goback);
        Goback.setOnClickListener(this);
        this.add_sound_txt = (TextView) findViewById(R.id.add_sound_txt);
        this.add_sound_txt.setOnClickListener(this);
        final Timer[] timerArr = {new Timer()};
        this.record_image.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    timerArr[0] = new Timer();
                    timerArr[0].schedule(new TimerTask() {
                        public void run() {
                            VideoUploadingActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    if (!VideoUploadingActivity.this.is_recording) {
                                        VideoUploadingActivity.this.Start_or_Stop_Recording();
                                    }
                                }
                            });
                        }
                    }, 200);
                } else if (motionEvent.getAction() == 1) {
                    timerArr[0].cancel();
                    if (VideoUploadingActivity.this.is_recording) {
                        VideoUploadingActivity.this.Start_or_Stop_Recording();
                    }
                }
                return false;
            }
        });
        this.video_progress = (SegmentedProgressBar) findViewById(R.id.video_progress);
        this.video_progress.enableAutoProgressView(18000);
        this.video_progress.setDividerColor(-1);
        this.video_progress.setDividerEnabled(true);
        this.video_progress.setDividerWidth(4.0f);
        this.video_progress.setShader(new int[]{-16711681, -16711681, -16711681});
        this.video_progress.SetListener(new ProgressBarListener() {
            public void TimeinMill(long j) {
                VideoUploadingActivity.this.sec_passed = (int) (j / 1000);
                if (VideoUploadingActivity.this.sec_passed > 17) {
                    VideoUploadingActivity.this.Start_or_Stop_Recording();
                }
            }
        });

    }

    public void Start_or_Stop_Recording() {
        if (!this.is_recording && this.sec_passed < 18) {
            this.number++;
            this.is_recording = true;
            StringBuilder sb = new StringBuilder();
            sb.append(folder);
            sb.append("/myvideo");
            sb.append(this.number);
            sb.append(".mp4");
            File file = new File(sb.toString());
            ArrayList<String> arrayList = this.videopaths;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(folder);
            sb2.append("/myvideo");
            sb2.append(this.number);
            sb2.append(".mp4");
            arrayList.add(sb2.toString());
            Log.e("qqqqq", String.valueOf(file));
            this.cameraView.captureVideo(file);
            if (this.audio != null) {
                this.audio.start();
            }
            this.video_progress.resume();
            this.done_btn.setBackgroundResource(R.drawable.ic_not_done);
            this.done_btn.setEnabled(false);
            this.record_image.setImageDrawable(getResources().getDrawable(R.drawable.ic_recoding_yes));
            this.camera_options.setVisibility(8);
            this.add_sound_txt.setClickable(false);
            this.rotate_camera.setVisibility(8);
        } else if (this.is_recording) {
            this.is_recording = false;
            this.video_progress.pause();
            this.video_progress.addDivider();
            if (this.audio != null) {
                this.audio.pause();
            }
            this.cameraView.stopVideo();
            if (this.sec_passed > 5) {
                this.done_btn.setBackgroundResource(R.drawable.ic_done);
                this.done_btn.setEnabled(true);
            }
            this.record_image.setImageDrawable(getResources().getDrawable(R.drawable.ic_recoding_no));
            this.camera_options.setVisibility(0);
        } else if (this.sec_passed > 17) {
            Common.Show_Alert(VideoUploadingActivity.this, "Alert", "Video only can be a 18s.");
        }
    }

    private boolean append() {
        final ProgressDialog progressDialog = new ProgressDialog(VideoUploadingActivity.this);
        new Thread(new Runnable() {
            public void run() {
                FileInputStream fileInputStream;
                MediaMetadataRetriever mediaMetadataRetriever;
                VideoUploadingActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        progressDialog.setMessage("Please wait..");
                        progressDialog.show();
                    }
                });
                ArrayList arrayList = new ArrayList();
                for (int i = 0; i < VideoUploadingActivity.this.videopaths.size(); i++) {
                    File file = new File((String) VideoUploadingActivity.this.videopaths.get(i));
                    if (file.exists()) {
                        MediaMetadataRetriever mediaMetadataRetriever2 = null;
                        try {
                            mediaMetadataRetriever = new MediaMetadataRetriever();
                            try {
                                fileInputStream = new FileInputStream(file.getAbsolutePath());
                                try {
                                    mediaMetadataRetriever.setDataSource(fileInputStream.getFD());
                                    if ("yes".equals(mediaMetadataRetriever.extractMetadata(17)) && file.length() > 3000) {
                                        Log.d("resp", (String) VideoUploadingActivity.this.videopaths.get(i));
                                        arrayList.add(VideoUploadingActivity.this.videopaths.get(i));
                                    }
                                    mediaMetadataRetriever.release();
                                } catch (IOException e) {
                                    e = e;
                                    mediaMetadataRetriever2 = mediaMetadataRetriever;
                                    try {
                                        Log.e("Video_Recorder", e.getMessage(), e);
                                        if (mediaMetadataRetriever2 != null) {
                                            mediaMetadataRetriever2.release();
                                        }
                                        if (fileInputStream == null) {
                                        }
                                        fileInputStream.close();
                                    } catch (Throwable th) {
                                        th = th;
                                        mediaMetadataRetriever = mediaMetadataRetriever2;
                                        if (mediaMetadataRetriever != null) {
                                            mediaMetadataRetriever.release();
                                        }
                                        if (fileInputStream != null) {
                                            try {
                                                fileInputStream.close();
                                            } catch (IOException unused) {
                                            }
                                        }
                                        throw th;
                                    }
                                } catch (Throwable th2) {
                                    if (mediaMetadataRetriever != null) {
                                    }
                                    if (fileInputStream != null) {
                                    }
                                }
                            } catch (IOException e2) {
                                fileInputStream = null;
                                mediaMetadataRetriever2 = mediaMetadataRetriever;
                                Log.e("Video_Recorder", e2.getMessage(), e2);
                                if (mediaMetadataRetriever2 != null) {
                                }
                                if (fileInputStream == null) {
                                }
                                fileInputStream.close();
                            } catch (Throwable th3) {
                                fileInputStream = null;
                                if (mediaMetadataRetriever != null) {
                                }
                                if (fileInputStream != null) {
                                }
                            }
                        } catch (IOException e3) {
                            fileInputStream = null;
                            Log.e("Video_Recorder", e3.getMessage(), e3);
                            if (mediaMetadataRetriever2 != null) {
                            }
                            if (fileInputStream == null) {
                            }
                            try {
                                fileInputStream.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (Throwable th4) {
                            mediaMetadataRetriever = null;
                            fileInputStream = null;
                            if (mediaMetadataRetriever != null) {
                            }
                            if (fileInputStream != null) {
                            }
                        }
                        try {
                            fileInputStream.close();
                        } catch (IOException unused2) {
                        }
                    }
                }
                try {
                    Movie[] movieArr = new Movie[arrayList.size()];
                    for (int i2 = 0; i2 < arrayList.size(); i2++) {
                        movieArr[i2] = MovieCreator.build((String) arrayList.get(i2));
                    }
                    LinkedList linkedList = new LinkedList();
                    LinkedList linkedList2 = new LinkedList();
                    for (Movie tracks : movieArr) {
                        for (Track track : tracks.getTracks()) {
                            if (track.getHandler().equals("soun")) {
                                linkedList2.add(track);
                            }
                            if (track.getHandler().equals("vide")) {
                                linkedList.add(track);
                            }
                        }
                    }
                    Movie movie = new Movie();
                    if (linkedList2.size() > 0) {
                        movie.addTrack(new AppendTrack((Track[]) linkedList2.toArray(new Track[linkedList2.size()])));
                    }
                    if (linkedList.size() > 0) {
                        movie.addTrack(new AppendTrack((Track[]) linkedList.toArray(new Track[linkedList.size()])));
                    }
                    Container build = new DefaultMp4Builder().build(movie);
                    if (VideoUploadingActivity.this.audio != null) {
                        str = Glob.outputfile;
                    } else {
                        str = Glob.outputfile2;
                    }
                    FileOutputStream fileOutputStream = new FileOutputStream(new File(str));
                    build.writeContainer(fileOutputStream.getChannel());
                    fileOutputStream.close();
                    VideoUploadingActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            progressDialog.dismiss();
                            if (VideoUploadingActivity.this.audio != null) {
                                VideoUploadingActivity.this.Merge_withAudio();
                            } else {
                                VideoUploadingActivity.this.Go_To_preview_Activity(str);
                            }
                        }
                    });
                } catch (Exception unused3) {
                }
            }
        }).start();
        return true;
    }

    public void Merge_withAudio() {
        String file = Environment.getExternalStorageDirectory().toString();
        StringBuilder sb = new StringBuilder();
        sb.append(file);
        sb.append("/");
        sb.append(Glob.SelectedAudio);
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(file);
        sb3.append("/output.mp4");
        String sb4 = sb3.toString();
        StringBuilder sb5 = new StringBuilder();
        sb5.append(file);
        sb5.append("/output2.mp4");
        String sb6 = sb5.toString();
        new Merge_Video_Audio(VideoUploadingActivity.this).doInBackground(sb2, sb4, sb6);
    }

    public void RotateCamera() {
        this.cameraView.toggleFacing();
    }

    @SuppressLint({"WrongConstant"})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.Goback:
                onBackPressed();
                return;
            case R.id.add_sound_txt /*2131296343*/:
                startActivityForResult(new Intent(VideoUploadingActivity.this, AudioActivity.class), Sounds_list_Request_code);
//                overridePendingTransition(R.anim.in_from_bottom, R.anim.out_to_top);
                return;
            case R.id.done /*2131296439*/:
                append();
                return;
            case R.id.flash_camera /*2131296494*/:
                if (this.is_flash_on) {
                    this.is_flash_on = false;
                    this.cameraView.setFlash(0);
                    this.flash_btn.setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_on));
                    return;
                }
                this.is_flash_on = true;
                this.cameraView.setFlash(3);
                this.flash_btn.setImageDrawable(getResources().getDrawable(R.drawable.ic_flash_off));
                return;
            case R.id.record_image /*2131296662*/:
                Start_or_Stop_Recording();
                return;
            case R.id.rotate_camera /*2131296672*/:
                RotateCamera();
                return;
            case R.id.upload_layout /*2131296807*/:
                startActivity(new Intent(VideoUploadingActivity.this, GalleryVideoActivity.class));
//                overridePendingTransition(R.anim.in_from_bottom, R.anim.out_to_top);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == Sounds_list_Request_code && intent != null && intent.getStringExtra("isSelected").equals("yes")) {
            this.add_sound_txt.setText(intent.getStringExtra("sound_name"));
            Glob.Selected_sound_id = intent.getStringExtra("sound_id");
            PreparedAudio();
        }
    }

    public void PreparedAudio() {
        StringBuilder sb = new StringBuilder();
        sb.append(folder);
        sb.append("/");
        sb.append(Glob.SelectedAudio);
        if (new File(sb.toString()).exists()) {
            this.audio = new MediaPlayer();
            try {
                MediaPlayer mediaPlayer = this.audio;
                StringBuilder sb2 = new StringBuilder();
                sb2.append(folder);
                sb2.append("/");
                sb2.append(Glob.SelectedAudio);
                mediaPlayer.setDataSource(sb2.toString());
                this.audio.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.cameraView.start();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        try {
            if (this.audio != null) {
                this.audio.stop();
                this.audio.reset();
                this.audio.release();
            }
            this.cameraView.stop();
        } catch (Exception unused) {
        }
    }

    public void onBackPressed() {
        Dialog indeterminant_dialog = new Dialog(this);
        indeterminant_dialog.setContentView(R.layout.video_creation_exit);
        indeterminant_dialog.setCancelable(false);
        indeterminant_dialog.show();

        ImageView img_no = indeterminant_dialog.findViewById(R.id.img_no);
        ImageView img_yes = indeterminant_dialog.findViewById(R.id.img_yes);

        img_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indeterminant_dialog.dismiss();
                VideoUploadingActivity.this.DeleteFile();
                VideoUploadingActivity.this.finish();
            }
        });

        img_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                indeterminant_dialog.dismiss();
            }
        });

//        new AlertDialog.Builder(VideoUploadingActivity.this).setTitle("Alert").setMessage("Are you Sure? if you Go back you can't undo this action").setNegativeButton("No", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//            }
//        }).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//                VideoUploadingActivity.this.DeleteFile();
//                VideoUploadingActivity.this.finish();
//                VideoUploadFragment.this.overridePendingTransition(R.anim.in_from_top, R.anim.out_from_bottom);
//            }
//        }).show();
    }

    public void Go_To_preview_Activity(String str) {
        Intent intent = new Intent(VideoUploadingActivity.this, Preview_Video_A.class);
        intent.putExtra("path", str);
        startActivity(intent);
//        startActivity(new Intent(getContext(), Preview_Video_A.class));
//        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }

    public void DeleteFile() {
        this.delete_count++;
        File file = new File(Glob.outputfile);
        File file2 = new File(Glob.outputfile2);
        File file3 = new File(Glob.output_filter_file);
        if (file.exists()) {
            file.delete();
        }
        if (file2.exists()) {
            file2.delete();
        }
        if (file3.exists()) {
            file3.delete();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(folder);
        sb.append("/myvideo");
        sb.append(this.delete_count);
        sb.append(".mp4");
        File file4 = new File(sb.toString());
        if (file4.exists()) {
            file4.delete();
            DeleteFile();
        }
    }
}

