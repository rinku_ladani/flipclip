package com.india.flipclip.Home.Botttom.Login;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.india.flipclip.Home.Botttom.Home.Profile.MyVideos_Adapter;
import com.india.flipclip.Model.Customer;
import com.india.flipclip.Model.Example;
import com.india.flipclip.R;
import com.india.flipclip.Rest.API;
import com.india.flipclip.Util.Common;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

public class Profile_Liked_Video extends Fragment {
    public static int myvideo_count;
    UserLikeVideo_Adapter adapter;
    Context context;
    Boolean isVisibleToUser = Boolean.valueOf(false);
    Boolean is_api_run = Boolean.valueOf(false);
    RelativeLayout no_data_layout;
    public RecyclerView recyclerView;
    String user_id;
    View view;
    private ArrayList<Customer> mediaObjectList;


    public Profile_Liked_Video() {
    }

    @SuppressLint({"ValidFragment"})
    public Profile_Liked_Video(String str) {
        this.user_id = str;
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.view = layoutInflater.inflate(R.layout.fragment_user_likedvideo, viewGroup, false);
        this.context = getContext();
        this.recyclerView = (RecyclerView) this.view.findViewById(R.id.recylerview);
        this.recyclerView.setLayoutManager(new GridLayoutManager(this.context, 3));
        this.recyclerView.setHasFixedSize(true);
        this.no_data_layout = (RelativeLayout) this.view.findViewById(R.id.no_data_layout);
        Call_Api_For_get_Allvideos(user_id);
        return this.view;
    }

    public void setUserVisibleHint(boolean z) {
        super.setUserVisibleHint(z);
        this.isVisibleToUser = Boolean.valueOf(z);
        if (this.view != null && z) {
            Call_Api_For_get_Allvideos(user_id);
        }
    }

    public void onResume() {
        super.onResume();
        if (this.view != null && this.isVisibleToUser.booleanValue() && !this.is_api_run.booleanValue()) {
            Call_Api_For_get_Allvideos(user_id);
        }
    }

    private void Call_Api_For_get_Allvideos(String userid) {
        Common.showProgressDialog(getActivity());
        API.user().getUserLikeVideo(userid).enqueue(new retrofit2.Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                Example videoResponse = response.body();
                if (videoResponse.getSuccess() == 1) {
                    recyclerView.setVisibility(View.VISIBLE);
                    no_data_layout.setVisibility(View.GONE);
                    mediaObjectList = videoResponse.getCustomer();
                    adapter = new UserLikeVideo_Adapter(getActivity(), mediaObjectList);
                    recyclerView.setAdapter(adapter);
                    Common.dismissDialog();
                    Log.e("QQQQQQQ", "onResponse: " + mediaObjectList.size());
                } else {
                    if (videoResponse.getSuccess() == 0) {
                        recyclerView.setVisibility(View.GONE);
                        no_data_layout.setVisibility(View.VISIBLE);
//                        Common.showToast(getActivity(), videoResponse.getCustomer().get(0).getResponse() + "");
                        Common.dismissDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
//                Common.showToast(getActivity(), "Password does not match");
                Common.dismissDialog();
            }
        });

    }

    /* access modifiers changed from: private */
    public void OpenWatchVideo(int i) {
       /* Intent intent = new Intent(getActivity(), WatchVideos_F.class);
        intent.putExtra("arraylist", this.data_list);
        intent.putExtra("position", i);
        startActivity(intent);*/
    }
}
