package com.india.flipclip.Home.Botttom.Login;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.tabs.TabLayout;
import com.india.flipclip.Model.Example;
import com.india.flipclip.R;
import com.india.flipclip.Rest.API;
import com.india.flipclip.Util.Common;
import com.india.flipclip.Util.PrefManager;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Response;

public class ProfileFragment extends Fragment implements View.OnClickListener, PopupMenu.OnMenuItemClickListener {


    ImageView btnLogin;
    TextView tv_sign_up;
    RelativeLayout lnl_login;
    PrefManager prefManager;
    RelativeLayout rnl_profile;
    ViewPagerAdapter adapter;
    ImageView back_btn;
    Context context;
    TextView fans_count_txt;
    TextView follow_count_txt;
    TextView follow_unfollow_btn;
    TextView heart_count_txt;
    CircleImageView imageView;
    boolean isdataload = false;
    ViewPager pager;
    ImageView setting_btn;
    TabLayout tabLayout;
    RelativeLayout tabs_main_layout;
    LinearLayout top_layout;
    String user_id;
    public TextView username;
    public TextView video_count_txt;
    LinearLayout following_layout;
    LinearLayout fans_layout;


    ImageView img_usernamelogin;
    ImageView Goback;
    RelativeLayout relative_sign;
    TextView tv_login;
    RelativeLayout relative_login;
    ImageView img_usernamelogin_1;
    TextView tv_login_1;
    private View dialogView;
    private BottomSheetDialog dialog;


    public ProfileFragment() {

    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View view = layoutInflater.inflate(R.layout.profile_fragment, viewGroup, false);
        this.context = getContext();

        prefManager = new PrefManager(getContext());
        dialogView = getLayoutInflater().inflate(R.layout.bottomsheet_login_layout, null);
        dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(dialogView);

        user_id = prefManager.getRegId();
        init(view);

        if (prefManager.getUserName() != null && !prefManager.getUserName().equals("")) {
            rnl_profile.setVisibility(View.VISIBLE);
            lnl_login.setVisibility(View.GONE);
            username.setText(prefManager.getUserName());
            getVideoProfileCount(user_id);
        } else {
            lnl_login.setVisibility(View.VISIBLE);
            rnl_profile.setVisibility(View.GONE);
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.show();

                FrameLayout bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);

                BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
                bottomSheetBehavior.setPeekHeight(Resources.getSystem().getDisplayMetrics().heightPixels);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                img_usernamelogin = dialog.findViewById(R.id.img_usernamelogin);
                relative_sign = dialog.findViewById(R.id.relative_sign);
                tv_login = dialog.findViewById(R.id.tv_login);
                relative_login = dialog.findViewById(R.id.relative_login);
                img_usernamelogin_1 = dialog.findViewById(R.id.img_usernamelogin_1);
                tv_login_1 = dialog.findViewById(R.id.tv_login_1);
                Goback = dialog.findViewById(R.id.Goback);

                Goback.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                tv_login.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        relative_sign.setVisibility(View.GONE);
                        relative_login.setVisibility(View.VISIBLE);
                    }
                });

                tv_login_1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        relative_sign.setVisibility(View.VISIBLE);
                        relative_login.setVisibility(View.GONE);
                    }
                });
                img_usernamelogin_1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), LogInActivity.class);
                        startActivity(intent);
                    }
                });
                img_usernamelogin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), SignInActivity.class);
                        startActivity(intent);
                    }
                });
            }
        });

        tv_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), SignInActivity.class));
            }
        });

        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        if (dialog.isShowing()) {
            dialog.dismiss();
        } else {

        }
    }


    public View init(View view) {
        btnLogin = view.findViewById(R.id.btnLogin);
        tv_sign_up = view.findViewById(R.id.tv_sign_up);
        lnl_login = view.findViewById(R.id.lnl_login);
        rnl_profile = view.findViewById(R.id.rnl_profile);

        username = (TextView) view.findViewById(R.id.username);
        imageView = (CircleImageView) view.findViewById(R.id.user_image);
        imageView.setOnClickListener(this);


//        String userpicuri = "http://paperpoint.co.in/Tiktok/API/user_profile/";
//        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append(userpicuri);
//        stringBuilder.append(userprofile_name);
//
//        Log.e("SSSSS", stringBuilder.toString());
//
//        finauserpic = stringBuilder.toString();

        Log.e("QSQSQS", prefManager.getuserpic());
        Glide.with(context).load(prefManager.getuserpic()).placeholder(context.getDrawable(R.drawable.profile_image_placeholder)).into(imageView);

        this.video_count_txt = (TextView) view.findViewById(R.id.video_count_txt);
        this.follow_count_txt = (TextView) view.findViewById(R.id.follow_count_txt);
        this.fans_count_txt = (TextView) view.findViewById(R.id.fan_count_txt);
        this.heart_count_txt = (TextView) view.findViewById(R.id.heart_count_txt);
        this.setting_btn = (ImageView) view.findViewById(R.id.setting_btn);
        this.setting_btn.setOnClickListener(this);
        this.back_btn = (ImageView) view.findViewById(R.id.back_btn);
        this.back_btn.setOnClickListener(this);
        this.follow_unfollow_btn = (TextView) view.findViewById(R.id.follow_unfollow_btn);
        this.follow_unfollow_btn.setOnClickListener(this);
        this.tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        this.pager = (ViewPager) view.findViewById(R.id.pager);
        this.pager.setOffscreenPageLimit(2);
        this.adapter = new ViewPagerAdapter(getResources(), getChildFragmentManager());
        this.pager.setAdapter(this.adapter);
        this.tabLayout.setupWithViewPager(this.pager);
        setupTabIcons();
        this.tabs_main_layout = (RelativeLayout) view.findViewById(R.id.tabs_main_layout);
        this.top_layout = (LinearLayout) view.findViewById(R.id.top_layout);
        this.top_layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                final int measuredHeight = top_layout.getMeasuredHeight();
                top_layout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                tabs_main_layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        ViewGroup.LayoutParams layoutParams = tabs_main_layout.getLayoutParams();
                        layoutParams.height = tabs_main_layout.getMeasuredHeight() + measuredHeight;
                        tabs_main_layout.setLayoutParams(layoutParams);
                        tabs_main_layout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                });
            }
        });
        following_layout = view.findViewById(R.id.following_layout);
        following_layout.setOnClickListener(this);
        fans_layout = view.findViewById(R.id.fans_layout);
        fans_layout.setOnClickListener(this);
        this.isdataload = true;
        return view;
    }

    private void getVideoProfileCount(String user_id) {
        Common.showProgressDialog(getActivity());
        API.user().getUserProfileVideocount(user_id).enqueue(new retrofit2.Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                Example videoResponse = response.body();
                if (videoResponse.getSuccess() == 1) {
                    String videoAllcount = String.valueOf(videoResponse.getVideocount());
                    String videolikeCount = String.valueOf(videoResponse.getVideocountlike());
                    String following_count = String.valueOf(videoResponse.getFollowingCount());
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(videoAllcount);
                    stringBuilder.append(" ");
                    stringBuilder.append("Video");
                    video_count_txt.setText(stringBuilder.toString());
                    heart_count_txt.setText(videolikeCount);
                    follow_count_txt.setText(following_count);

                    Common.dismissDialog();
                } else {
                    if (videoResponse.getSuccess() == 0) {
                        Common.dismissDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
//                Common.showToast(getActivity(), "Password does not match");
                Common.dismissDialog();
            }
        });

    }

    private void setupTabIcons() {
        View inflate = LayoutInflater.from(this.context).inflate(R.layout.item_tabs_profile_menu, null);
        ((ImageView) inflate.findViewById(R.id.image)).setImageDrawable(getResources().getDrawable(R.drawable.ic_my_video_color));
        this.tabLayout.getTabAt(0).setCustomView(inflate);
        View inflate2 = LayoutInflater.from(this.context).inflate(R.layout.item_tabs_profile_menu, null);
        ((ImageView) inflate2.findViewById(R.id.image)).setImageDrawable(getResources().getDrawable(R.drawable.ic_liked_video_gray));
        this.tabLayout.getTabAt(1).setCustomView(inflate2);
        this.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            public void onTabReselected(TabLayout.Tab tab) {
            }

            public void onTabSelected(TabLayout.Tab tab) {
                View customView = tab.getCustomView();
                ImageView imageView = (ImageView) customView.findViewById(R.id.image);
                switch (tab.getPosition()) {
                    case 0:
                        imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_video_color));
                        break;
                    case 1:
                        imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_liked_video_color));
                        break;
                }
                tab.setCustomView(customView);
            }

            public void onTabUnselected(TabLayout.Tab tab) {
                View customView = tab.getCustomView();
                ImageView imageView = (ImageView) customView.findViewById(R.id.image);
                switch (tab.getPosition()) {
                    case 0:
                        imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_video_gray));
                        break;
                    case 1:
                        imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_liked_video_gray));
                        break;
                }
                tab.setCustomView(customView);
            }
        });

       /* public void Open_Setting() {
            Open_Chat_F();
        }

        public void Follow_unFollow_User() {

        }

        public void OpenfullsizeImage(String str) {
            See_Full_Image_F see_Full_Image_F = new See_Full_Image_F();
            FragmentTransaction beginTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            beginTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            Bundle bundle2 = new Bundle();
            bundle2.putSerializable("image_url", str);
            see_Full_Image_F.setArguments(bundle2);
            beginTransaction.addToBackStack(null);
            if (getActivity().findViewById(R.id.MainMenuFragment) != null) {
                beginTransaction.replace(R.id.MainMenuFragment, see_Full_Image_F).commit();
            } else {
                beginTransaction.replace(R.id.Profile_F, see_Full_Image_F).commit();
            }
        }

        public void Open_Chat_F() {
            Chat_Activity chat_Activity = new Chat_Activity();
            FragmentTransaction beginTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            beginTransaction.setCustomAnimations(R.anim.in_from_bottom, R.anim.out_to_top, R.anim.in_from_top, R.anim.out_from_bottom);
            Bundle bundle2 = new Bundle();
            bundle2.putString(AccessToken.USER_ID_KEY, this.user_id);
            bundle2.putString("user_name", this.user_name);
            bundle2.putString("user_pic", this.user_pic);
            chat_Activity.setArguments(bundle2);
            beginTransaction.addToBackStack(null);
            if (getActivity().findViewById(R.id.MainMenuFragment) != null) {
                beginTransaction.replace(R.id.MainMenuFragment, chat_Activity).commit();
            } else {
                beginTransaction.replace(R.id.Profile_F, chat_Activity).commit();
            }
        }

        public void Open_Following() {
            Following_F following_F = new Following_F();
            FragmentTransaction beginTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            beginTransaction.setCustomAnimations(R.anim.in_from_bottom, R.anim.out_to_top, R.anim.in_from_top, R.anim.out_from_bottom);
            Bundle bundle2 = new Bundle();
            bundle2.putString("id", this.user_id);
            bundle2.putString("from_where", "following");
            following_F.setArguments(bundle2);
            beginTransaction.addToBackStack(null);
            if (getActivity().findViewById(R.id.MainMenuFragment) != null) {
                beginTransaction.replace(R.id.MainMenuFragment, following_F).commit();
            } else {
                beginTransaction.replace(R.id.Profile_F, following_F).commit();
            }
        }

        public void Open_Followers() {
            Following_F following_F = new Following_F();
            FragmentTransaction beginTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            beginTransaction.setCustomAnimations(R.anim.in_from_bottom, R.anim.out_to_top, R.anim.in_from_top, R.anim.out_from_bottom);
            Bundle bundle2 = new Bundle();
            bundle2.putString("id", this.user_id);
            bundle2.putString("from_where", "fan");
            following_F.setArguments(bundle2);
            beginTransaction.addToBackStack(null);
            if (getActivity().findViewById(R.id.MainMenuFragment) != null) {
                beginTransaction.replace(R.id.MainMenuFragment, following_F).commit();
            } else {
                beginTransaction.replace(R.id.Profile_F, following_F).commit();
            }
        }

        public void onDetach() {
            super.onDetach();
            if (this.fragment_callback != null) {
                this.fragment_callback.Responce(new Bundle());
            }
            Functions.deleteCache(this.context);
        }
*/
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_btn /*2131296318*/:
                getActivity().onBackPressed();
                return;
            case R.id.fans_layout /*2131296429*/:
/*
                Open_Followers();
*/
                return;
            case R.id.follow_unfollow_btn /*2131296445*/:
               /* if (Variables.sharedPreferences.getBoolean(Variables.islogin, false)) {
                    Follow_unFollow_User();
                    return;
                } else {
                    Toast.makeText(this.context, "Please login in to app", 0).show();
                    return;
                }*/
            case R.id.following_layout /*2131296446*/:

                Intent intent = new Intent(getContext(), FollowingListActivity.class);
                startActivity(intent);

                return;
            case R.id.setting_btn /*2131296623*/:
                Open_Setting(view);
                return;
            case R.id.user_image /*2131296718*/:
/*
                OpenfullsizeImage(pic_url);
*/
                return;
            default:
                return;
        }
    }

    private void Open_Setting(View view) {
        PopupMenu popup = new PopupMenu(getContext(), view);
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.profile_menu);
        popup.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (prefManager.getUserName() != null && !prefManager.getUserName().equals("")) {
            rnl_profile.setVisibility(View.VISIBLE);
            lnl_login.setVisibility(View.GONE);
            username.setText(prefManager.getUserName());
        } else {
            lnl_login.setVisibility(View.VISIBLE);
            rnl_profile.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit_profile:
                if (user_id.isEmpty()) {
                    Toast.makeText(context, "fisrt login", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(getContext(), ProfileEditActivity.class);
                    intent.putExtra("user_id", user_id);
                    intent.putExtra("username", prefManager.getUserName());
                    intent.putExtra("emailid", prefManager.getUserEmail());
                    intent.putExtra("password", prefManager.getpassword());
                    intent.putExtra("gender", prefManager.getGender());
                    intent.putExtra("birthdate", prefManager.getdob());
                    intent.putExtra("finaluserpic", prefManager.getuserpic());
                    startActivity(intent);
                }
                return true;
            case R.id.profile_logout:
                valuedelete();
                return true;
            default:
                return false;
        }
    }


    private void valuedelete() {
        prefManager.userLogout();
        lnl_login.setVisibility(View.VISIBLE);
        rnl_profile.setVisibility(View.GONE);
//        getActivity().finish();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        SparseArray<Fragment> registeredFragments = new SparseArray<>();
        private final Resources resources;

        public int getCount() {
            return 2;
        }

        public CharSequence getPageTitle(int i) {
            return null;
        }

        public ViewPagerAdapter(Resources resources2, FragmentManager fragmentManager) {
            super(fragmentManager);
            this.resources = resources2;
        }

        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    return new User_Profile_Video(user_id);
                case 1:
                    return new Profile_Liked_Video(user_id);
                default:
                    return null;
            }
        }

        public Object instantiateItem(ViewGroup viewGroup, int i) {
            Fragment fragment = (Fragment) super.instantiateItem(viewGroup, i);
            this.registeredFragments.put(i, fragment);
            return fragment;
        }

        public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
            this.registeredFragments.remove(i);
            super.destroyItem(viewGroup, i, obj);
        }

        public Fragment getRegisteredFragment(int i) {
            return (Fragment) this.registeredFragments.get(i);
        }
    }

}
