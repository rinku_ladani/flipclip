package com.india.flipclip.Home.Botttom.Home.Profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.SignInButton;
import com.india.flipclip.Home.Botttom.Login.LogInActivity;
import com.india.flipclip.Home.Botttom.Login.SignInActivity;
import com.india.flipclip.R;

public class ProfileActivity extends AppCompatActivity {
    private Context context;
    private TextView tv_sign_up;
    private SignInButton btnGoogle;
    private LoginButton btnFacebook;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        FacebookSdk.sdkInitialize(getApplicationContext());


        context = this;
        tv_sign_up = findViewById(R.id.tv_sign_up);
        btnGoogle = findViewById(R.id.btnGoogle);
        btnFacebook = findViewById(R.id.btnFacebook);
        btnLogin = findViewById(R.id.btnLogin);


        tv_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, SignInActivity.class));
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, LogInActivity.class));

            }
        });
    }
}
