package com.india.flipclip.Home.Botttom.Home.Comment;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.india.flipclip.Model.Customer;
import com.india.flipclip.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * ARPITA AKBARI
 **/

public class Comments_Adapter extends RecyclerView.Adapter<Comments_Adapter.CustomViewHolder> {
    public Context context;
    private ArrayList<Customer> dataList;
    private OnItemClickListener listener;

    class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView message;
        ImageView user_pic;
        TextView username;

        public CustomViewHolder(View view) {
            super(view);
            this.username = (TextView) view.findViewById(R.id.username);
            this.user_pic = (ImageView) view.findViewById(R.id.user_pic);
            this.message = (TextView) view.findViewById(R.id.message);
        }

        public void bind(final int i, final Customer comment_Get_Set, final OnItemClickListener onItemClickListener) {
            this.itemView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    onItemClickListener.onItemClick(i, comment_Get_Set, view);
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int i, Customer comment_Get_Set, View view);
    }

    public Comments_Adapter(Context context2, ArrayList<Customer> arrayList, OnItemClickListener onItemClickListener) {
        this.context = context2;
        this.dataList = arrayList;
        this.listener = onItemClickListener;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_comment_layout, null);
        inflate.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
        return new CustomViewHolder(inflate);
    }

    public int getItemCount() {
        return this.dataList.size();
    }

    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        Customer comment_Get_Set = (Customer) this.dataList.get(i);
        TextView textView = customViewHolder.username;
        StringBuilder sb = new StringBuilder();
        sb.append(comment_Get_Set.getUname());
        textView.setText(sb.toString());
        Picasso.get().load(comment_Get_Set.getProfilePic()).into(customViewHolder.user_pic);
        customViewHolder.message.setText(Html.fromHtml(comment_Get_Set.getComment()));
        customViewHolder.bind(i, comment_Get_Set, this.listener);
    }

}
