package com.india.flipclip.Home.Botttom.Chat;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.india.flipclip.Home.Botttom.Login.SignInActivity;
import com.india.flipclip.Home.Botttom.Login.LogInActivity;
import com.india.flipclip.R;
import com.india.flipclip.Util.PrefManager;


public class InboxFragment extends Fragment {

    ImageView btn_sign_up;
    FrameLayout frame_inbox;
    PrefManager prefManager;
    ImageView img_usernamelogin;
    ImageView Goback;
    RelativeLayout relative_sign;
    TextView tv_login;
    RelativeLayout relative_login;
    ImageView img_usernamelogin_1;
    TextView tv_login_1;
    private View dialogView;
    private BottomSheetDialog dialog;

    public  InboxFragment(){

    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View root_view = layoutInflater.inflate(R.layout.inbox_fragment, viewGroup, false);

        prefManager = new PrefManager(getActivity());

        dialogView = getLayoutInflater().inflate(R.layout.bottomsheet_login_layout, null);
        dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(dialogView);

        btn_sign_up = root_view.findViewById(R.id.btn_sign_up);
        frame_inbox = root_view.findViewById(R.id.frame_inbox);

        if (prefManager.getUserName().isEmpty()) {
            btn_sign_up.setVisibility(View.VISIBLE);
        } else {
            btn_sign_up.setVisibility(View.GONE);
        }

        btn_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();

                FrameLayout bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);

                BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
                bottomSheetBehavior.setPeekHeight(Resources.getSystem().getDisplayMetrics().heightPixels);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                img_usernamelogin = dialog.findViewById(R.id.img_usernamelogin);
                relative_sign = dialog.findViewById(R.id.relative_sign);
                tv_login = dialog.findViewById(R.id.tv_login);
                relative_login = dialog.findViewById(R.id.relative_login);
                img_usernamelogin_1 = dialog.findViewById(R.id.img_usernamelogin_1);
                tv_login_1 = dialog.findViewById(R.id.tv_login_1);
                Goback = dialog.findViewById(R.id.Goback);

                Goback.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                tv_login.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        relative_sign.setVisibility(View.GONE);
                        relative_login.setVisibility(View.VISIBLE);
                    }
                });

                tv_login_1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        relative_sign.setVisibility(View.VISIBLE);
                        relative_login.setVisibility(View.GONE);
                    }
                });
                img_usernamelogin_1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), LogInActivity.class);
                        startActivity(intent);
                    }
                });
                img_usernamelogin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), SignInActivity.class);
                        startActivity(intent);
                    }
                });

//                FragmentTransaction beginTransaction3 = getActivity().getSupportFragmentManager().beginTransaction();
//                beginTransaction3.replace(R.id.MainMenuFragment, new SignUpFragment());
//                beginTransaction3.commit();
            }
        });

        return root_view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (dialog.isShowing()){
            dialog.dismiss();
        }else {

        }
    }

}
