package com.india.flipclip.Home.Botttom.Login;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.india.flipclip.Home.Botttom.Home.Profile.MyVideos_Adapter;
import com.india.flipclip.Home.Botttom.Home.Profile.WatchUserVideo.UserVideoWatchActivity;
import com.india.flipclip.Model.Customer;
import com.india.flipclip.R;

import java.util.ArrayList;

/**
 * ARPITA AKBARI
 **/

public class UserVideo_Adapter extends RecyclerView.Adapter<UserVideo_Adapter.CustomViewHolder> {
    public Context context;
    private ArrayList<Customer> dataList;
    String unamepass;
    String userpicpass;

    class CustomViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rl_main;
        ImageView thumb_image;
        TextView view_txt;

        public CustomViewHolder(View view) {
            super(view);
            this.thumb_image = (ImageView) view.findViewById(R.id.thumb_image);
            this.view_txt = (TextView) view.findViewById(R.id.view_txt);
            rl_main = (RelativeLayout) view.findViewById(R.id.rl_main);
        }
    }

    public UserVideo_Adapter(Context context2, ArrayList<Customer> arrayList, String userpic, String uname) {
        this.context = context2;
        this.dataList = arrayList;
        unamepass = uname;
        userpicpass = userpic;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_myvideo_layout, null);
        inflate.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
        return new CustomViewHolder(inflate);
    }

    public int getItemCount() {
        return this.dataList.size();
    }

    public void onBindViewHolder(CustomViewHolder customViewHolder, final int i) {
        Log.e("ASASASA", String.valueOf(dataList.size()));
        Glide.with(context).load(dataList.get(i).getVideolink()).placeholder(context.getDrawable(R.drawable.logo)).into(customViewHolder.thumb_image);


        customViewHolder.rl_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("DAAAAA", String.valueOf(dataList));
                Intent intent = new Intent(context, LoginUserVideoWatchActivity.class);
                intent.putExtra("arraylist", dataList);
                intent.putExtra("position", i);
                intent.putExtra("userpic", userpicpass);
                intent.putExtra("username", unamepass);
                context.startActivity(intent);
            }
        });

    }
}
