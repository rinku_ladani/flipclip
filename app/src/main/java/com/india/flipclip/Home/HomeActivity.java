//package com.india.flipclip.Home;
//
//import androidx.annotation.RequiresApi;
//import androidx.appcompat.app.AlertDialog;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.fragment.app.FragmentTransaction;
//
//import android.Manifest;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.pm.PackageManager;
//import android.net.Uri;
//import android.os.Build;
//import android.os.Bundle;
//import android.os.Environment;
//import android.util.Log;
//import android.widget.FrameLayout;
//import android.widget.Toast;
//
//import com.etebarian.meowbottomnavigation.MeowBottomNavigation;
//import com.india.flipclip.R;
//import com.india.flipclip.Update.GooglePlayStoreAppVersionNameLoader;
//import com.india.flipclip.Update.WSCallerVersionListener;
//import com.india.flipclip.Home.Botttom.VideoUpload.VideoUploadFragment;
//import com.india.flipclip.Home.Botttom.Home.HomeFragment;
//import com.india.flipclip.Home.Botttom.Chat.InboxFragment;
//import com.india.flipclip.Home.Botttom.Login.ProfileFragment;
//import com.india.flipclip.Home.Botttom.Search.SearchFragment;
//import com.india.flipclip.Util.PrefManager;
//
//import java.io.File;
//
//public class HomeActivity extends AppCompatActivity implements WSCallerVersionListener {
//    FrameLayout framelayout;
//    MeowBottomNavigation bottomNavigation;
//    private final static int ID_HOME = 1;
//    private final static int ID_SEARCH = 2;
//    private final static int ID_VIDEOEDIT = 3;
//    private final static int ID_PROFILE = 4;
//    private final static int ID_INBOX = 5;
//    private boolean isForceUpdate = true;
//    private boolean isPermitted;
//    public static File folder;
//
//    PrefManager prefManager;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_home);
//
//        new GooglePlayStoreAppVersionNameLoader(getApplicationContext(), this).execute();
//        checkRunTimePermission();
//
//        prefManager = new PrefManager(this);
//        folder = new File(Environment.getExternalStorageDirectory() + "/" + getResources().getString(R.string.app_name));
//        boolean success = true;
//        if (!folder.exists()) {
//            //Toast.makeText(MainActivity.this, "Directory Does Not Exist, Create It", Toast.LENGTH_SHORT).show();
//            success = folder.mkdir();
//        }
//        if (success) {
//            //Toast.makeText(MainActivity.this, "Directory Created", Toast.LENGTH_SHORT).show();
//        } else {
//            //Toast.makeText(MainActivity.this, "Failed - Error", Toast.LENGTH_SHORT).show();
//        }
//
//
//        if (folder.isDirectory()) {
//            String[] children = folder.list();
//            for (int i = 0; i < children.length; i++) {
//                new File(folder, children[i]).delete();
//            }
//        }
//
//        bottomNavigation = findViewById(R.id.bottomNavigation);
//        framelayout = findViewById(R.id.framelayout);
//        bottomNavigation.add(new MeowBottomNavigation.Model(ID_HOME, R.drawable.home));
//        bottomNavigation.add(new MeowBottomNavigation.Model(ID_SEARCH, R.drawable.search));
//        bottomNavigation.add(new MeowBottomNavigation.Model(ID_VIDEOEDIT, R.drawable.btn_add_main));
//        bottomNavigation.add(new MeowBottomNavigation.Model(ID_PROFILE, R.drawable.profile));
//        bottomNavigation.add(new MeowBottomNavigation.Model(ID_INBOX, R.drawable.inbox));
//
//
//        bottomNavigation.setOnClickMenuListener(new MeowBottomNavigation.ClickListener() {
//            @Override
//            public void onClickItem(MeowBottomNavigation.Model item) {
////                Toast.makeText (HomeActivity.this, "clicked item : " + item.getId (), Toast.LENGTH_SHORT).show ();
//            }
//        });
//
//
//        bottomNavigation.setOnShowListener(new MeowBottomNavigation.ShowListener() {
//            @Override
//            public void onShowItem(MeowBottomNavigation.Model item) {
////                Toast.makeText (HomeActivity.this, "showing item : " + item.getId (), Toast.LENGTH_SHORT).show ();
//                String name;
//                switch (item.getId()) {
//                    case ID_HOME:
//                        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
//                        beginTransaction.replace(R.id.framelayout, new HomeFragment());
//                        beginTransaction.commit();
//                        break;
//                    case ID_SEARCH:
//                        if (prefManager.getUserName().isEmpty()) {
//                            FragmentTransaction beginTransaction3 = getSupportFragmentManager().beginTransaction();
//                            beginTransaction3.replace(R.id.framelayout, new ProfileFragment());
//                            beginTransaction3.commit();
////                            Toast.makeText(HomeActivity.this, "Please Login First", Toast.LENGTH_SHORT).show();
//                        } else {
////                        Toast.makeText(HomeActivity.this, "Comming Soon", Toast.LENGTH_SHORT).show();
//                            FragmentTransaction beginTransaction1 = getSupportFragmentManager().beginTransaction();
//                            beginTransaction1.replace(R.id.framelayout, new SearchFragment());
//                            beginTransaction1.commit();
//                        }
//                        break;
//                    case ID_VIDEOEDIT:
//                        if (prefManager.getUserName().isEmpty()) {
//                            FragmentTransaction beginTransaction3 = getSupportFragmentManager().beginTransaction();
//                            beginTransaction3.replace(R.id.framelayout, new ProfileFragment());
//                            beginTransaction3.commit();
////                            Toast.makeText(HomeActivity.this, "Please Login First", Toast.LENGTH_SHORT).show();
//                        } else {
////                        Toast.makeText(HomeActivity.this, "Comming Soon", Toast.LENGTH_SHORT).show();
//                            FragmentTransaction beginTransaction2 = getSupportFragmentManager().beginTransaction();
//                            beginTransaction2.replace(R.id.framelayout, new VideoUploadFragment());
//                            beginTransaction2.commit();
//                        }
//                        break;
//                    case ID_PROFILE:
//                        FragmentTransaction beginTransaction3 = getSupportFragmentManager().beginTransaction();
//                        beginTransaction3.replace(R.id.framelayout, new ProfileFragment());
//                        beginTransaction3.commit();
//                        break;
//                    case ID_INBOX:
//                        if (prefManager.getUserName().isEmpty()) {
//                            FragmentTransaction beginTransaction5 = getSupportFragmentManager().beginTransaction();
//                            beginTransaction5.replace(R.id.framelayout, new ProfileFragment());
//                            beginTransaction5.commit();
////                            Toast.makeText(HomeActivity.this, "Please Login First", Toast.LENGTH_SHORT).show();
//                        } else {
////                        Toast.makeText(HomeActivity.this, "Comming Soon", Toast.LENGTH_SHORT).show();
//                            FragmentTransaction beginTransaction4 = getSupportFragmentManager().beginTransaction();
//                            beginTransaction4.replace(R.id.framelayout, new InboxFragment());
//                            beginTransaction4.commit();
//                        }
//                        break;
//                    default:
//                        name = "";
//                }
//            }
//        });
//
//        bottomNavigation.setOnReselectListener(new MeowBottomNavigation.ReselectListener() {
//            @Override
//            public void onReselectItem(MeowBottomNavigation.Model item) {
////                Toast.makeText (HomeActivity.this, "reselected item : " + item.getId (), Toast.LENGTH_SHORT).show ();
//            }
//        });
//
//
//        bottomNavigation.show(ID_HOME, true);
//
//    }
//
//    @Override
//    public void onGetResponse(boolean isUpdateAvailable) {
//        Log.e("ResultAPPMAIN", String.valueOf(isUpdateAvailable));
//        if (isUpdateAvailable) {
//            showUpdateDialog();
//        }
//    }
//
//    public void showUpdateDialog() {
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
//
//        alertDialogBuilder.setTitle(getString(R.string.app_name));
//        alertDialogBuilder.setMessage(getString(R.string.update_message));
//        alertDialogBuilder.setCancelable(false);
//        alertDialogBuilder.setPositiveButton(R.string.update_now, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
//                dialog.cancel();
//            }
//        });
//        alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                if (isForceUpdate) {
//                    finish();
//                }
//                dialog.dismiss();
//            }
//        });
//        alertDialogBuilder.show();
//    }
//
//    public void checkRunTimePermission() {
//        String[] permissionArrays = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_PHONE_STATE, Manifest.permission.MODIFY_AUDIO_SETTINGS, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO};
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            requestPermissions(permissionArrays, 11111);
//        } else {
//            // if already permition granted
//            // PUT YOUR ACTION (Like Open cemara etc..)
//        }
//    }
//
//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        boolean openActivityOnce = true;
//        boolean openDialogOnce = true;
//        if (requestCode == 11111) {
//            for (int i = 0; i < grantResults.length; i++) {
//                String permission = permissions[i];
//
//                isPermitted = grantResults[i] == PackageManager.PERMISSION_GRANTED;
//
//                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
//                    // user rejected the permission
//                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
//                    if (!showRationale) {
//                        //execute when 'never Ask Again' tick and permission dialog not show
//                    } else {
//                        if (openDialogOnce) {
//                            alertView();
//                        }
//                    }
//                }
//            }
//
//            if (isPermitted) {
//
//            }
////                if (isPermissionFromGallery)
////                    openGalleryFragment();
//        }
//    }
//
//    private void alertView() {
//        AlertDialog.Builder dialog = new AlertDialog.Builder(getApplicationContext(), R.style.MyAlertDialogStyle);
//
//        dialog.setTitle("Permission Denied")
//                .setInverseBackgroundForced(true)
//                //.setIcon(R.drawable.ic_info_black_24dp)
//                .setMessage("Without those permission the app is unable to save your profile. App needs to save profile image in your external storage and also need to get profile image from camera or external storage.Are you sure you want to deny this permission?")
//
//                .setNegativeButton("I'M SURE", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialoginterface, int i) {
//                        dialoginterface.dismiss();
//                    }
//                })
//                .setPositiveButton("RE-TRY", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialoginterface, int i) {
//                        dialoginterface.dismiss();
//                        checkRunTimePermission();
//
//                    }
//                }).show();
//    }
//}
