package com.india.flipclip.Home.Botttom.VideoUpload;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView.Adapter;
import androidx.recyclerview.widget.RecyclerView.LayoutParams;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;


import com.india.flipclip.R;

import java.util.List;

import jp.co.cyberagent.android.gpuimage.GPUImageView;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageBilateralBlurFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageBoxBlurFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageBrightnessFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageCGAColorspaceFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageColorInvertFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageContrastFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageCrosshatchFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageExposureFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageGammaFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageGaussianBlurFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageGrayscaleFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageHalftoneFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageHazeFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageHighlightShadowFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageHueFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageLuminanceFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageLuminanceThresholdFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageMonochromeFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageOpacityFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImagePixelationFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImagePosterizeFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageRGBFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageSaturationFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageSepiaToneFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageSharpenFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageSolarizeFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageSphereRefractionFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageSwirlFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageToneCurveFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageVignetteFilter;
import jp.co.cyberagent.android.gpuimage.filter.GPUImageWeakPixelInclusionFilter;

//import jp.co.cyberagent.android.gpuimage.filter.GPUImageVibranceFilter;

public class Filter_Adapter extends Adapter<Filter_Adapter.CustomViewHolder> {
    public Context context;
    List<FilterType> datalist;
    Bitmap image;
    public OnItemClickListener listener;

    class CustomViewHolder extends ViewHolder {
        TextView fiter_txt;
        /* access modifiers changed from: private */
        public GPUImageView ivPhoto;

        public CustomViewHolder(View view) {
            super(view);
            this.fiter_txt = (TextView) view.findViewById(R.id.filter_txt);
            this.ivPhoto = (GPUImageView) view.findViewById(R.id.iv_photo);
        }

        public void bind(final int i, final FilterType filterType, final OnItemClickListener onItemClickListener) {
            this.itemView.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    onItemClickListener.onItemClick(view, i, filterType);
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int i, FilterType filterType);
    }

    public Filter_Adapter(Context context2, List<FilterType> list, OnItemClickListener onItemClickListener) {
        this.context = context2;
        this.datalist = list;
        this.listener = onItemClickListener;
        this.image = BitmapFactory.decodeResource(context2.getResources(), R.drawable.ic_bg_filter);
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_filter_layout, viewGroup, false);
        inflate.setLayoutParams(new LayoutParams(-2, -2));
        return new CustomViewHolder(inflate);
    }

    public int getItemCount() {
        return this.datalist.size();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0098, code lost:
        if (r1.equals("BRIGHTNESS") != false) goto L_0x01d3;
     */
    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        char c = 0;
        customViewHolder.setIsRecyclable(false);
        String name = ((FilterType) this.datalist.get(i)).name();
        customViewHolder.fiter_txt.setText(name);
        if (Preview_Video_A.select_postion == i) {
            customViewHolder.ivPhoto.setBackgroundColor(this.context.getResources().getColor(R.color.redcolor));
        }
        customViewHolder.ivPhoto.setImage(this.image);
        switch (name.hashCode()) {
            case -2130373674:
                if (name.equals("INVERT")) {
                    c = 8;
                    break;
                }
            case -2119345441:
                if (name.equals("WEAK_PIXEL")) {
                    c = '!';
                    break;
                }
            case -2038923277:
                if (name.equals("SOLARIZE")) {
                    c = 27;
                    break;
                }
            case -2031525657:
                if (name.equals("SPHERE_REFRACTION")) {
                    c = 28;
                    break;
                }
            case -1454481330:
                if (name.equals("GRAY_SCALE")) {
                    c = 4;
                    break;
                }
            case -1420885219:
                if (name.equals("MONOCHROME")) {
                    c = 10;
                    break;
                }
            case -1350306346:
                if (name.equals("BILATERAL_BLUR")) {
                    c = 18;
                    break;
                }
            case -1231146373:
                if (name.equals("BOX_BLUR")) {
                    c = 19;
                    break;
                }
            case -1143378681:
                if (name.equals("EXPOSURE")) {
                    c = 1;
                    break;
                }
            case -549223445:
                if (name.equals("OPACITY")) {
                    c = 11;
                    break;
                }
            case -482559835:
                if (name.equals("CGA_COLORSPACE")) {
                    c = 21;
                    break;
                }
            case -330279720:
                if (name.equals("LUMINANCE")) {
                    c = 9;
                    break;
                }
            case 71896:
                if (name.equals("HUE")) {
                    c = 7;
                    break;
                }
            case 81069:
                if (name.equals("RGB")) {
                    c = 14;
                    break;
                }
            case 2210276:
                if (name.equals("HAZE")) {
                    c = 5;
                    break;
                }
            case 2580850:
                if (name.equals("TONE")) {
                    c = 30;
                    break;
                }
            case 67582855:
                if (name.equals("GAMMA")) {
                    c = 3;
                    break;
                }
            case 78787030:
                if (name.equals("SEPIA")) {
                    c = 16;
                    break;
                }
            case 78862282:
                if (name.equals("SHARP")) {
                    c = 17;
                    break;
                }
            case 79316831:
                if (name.equals("SWIRL")) {
                    c = 29;
                    break;
                }
            case 173486419:
                if (name.equals("ZOOM_BLUR")) {
                    c = '\"';
                    break;
                }
            case 215679746:
                if (name.equals("CONTRAST")) {
                    c = 22;
                    break;
                }
            case 254601170:
                if (name.equals("SATURATION")) {
                    c = 15;
                    break;
                }
            case 458749999:
                if (name.equals("PIXELATION")) {
                    c = 12;
                    break;
                }
            case 500075719:
                if (name.equals("POSTERIZE")) {
                    c = 13;
                    break;
                }
            case 729532453:
                if (name.equals("HALFTONE")) {
                    c = 25;
                    break;
                }
            case 872275968:
                if (name.equals("CROSSHATCH")) {
                    c = 23;
                    break;
                }
            case 1133254737:
                break;
            case 1150304043:
                if (name.equals("HIGHLIGHT_SHADOW")) {
                    c = 6;
                    break;
                }
            case 1164994789:
                if (name.equals("BULGE_DISTORTION")) {
                    c = 20;
                    break;
                }
            case 1298349628:
                if (name.equals("GAUSSIAN_FILTER")) {
                    c = 24;
                    break;
                }
            case 1697191332:
                if (name.equals("LUMINANCE_THRESHOLD")) {
                    c = 26;
                    break;
                }
            case 1851383025:
                if (name.equals("FILTER_GROUP_SAMPLE")) {
                    c = 2;
                    break;
                }
            case 1888358930:
                if (name.equals("VIBRANCE")) {
                    c = 31;
                    break;
                }
            case 2027936058:
                if (name.equals("VIGNETTE")) {
                    c = ' ';
                    break;
                }
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
                customViewHolder.ivPhoto.setFilter(new GPUImageBrightnessFilter());
                break;
            case 1:
                customViewHolder.ivPhoto.setFilter(new GPUImageExposureFilter());
                break;
            case 2:
                customViewHolder.ivPhoto.setFilter(new GPUImageVignetteFilter());
                break;
            case 3:
                customViewHolder.ivPhoto.setFilter(new GPUImageGammaFilter());
                break;
            case 4:
                customViewHolder.ivPhoto.setFilter(new GPUImageGrayscaleFilter());
                break;
            case 5:
                customViewHolder.ivPhoto.setFilter(new GPUImageHazeFilter());
                break;
            case 6:
                customViewHolder.ivPhoto.setFilter(new GPUImageHighlightShadowFilter());
                break;
            case 7:
                customViewHolder.ivPhoto.setFilter(new GPUImageHueFilter());
                break;
            case 8:
                customViewHolder.ivPhoto.setFilter(new GPUImageColorInvertFilter());
                break;
            case 9:
                customViewHolder.ivPhoto.setFilter(new GPUImageLuminanceFilter());
                break;
            case 10:
                customViewHolder.ivPhoto.setFilter(new GPUImageMonochromeFilter());
                break;
            case 11:
                customViewHolder.ivPhoto.setFilter(new GPUImageOpacityFilter());
                break;
            case 12:
                customViewHolder.ivPhoto.setFilter(new GPUImagePixelationFilter());
                break;
            case 13:
                customViewHolder.ivPhoto.setFilter(new GPUImagePosterizeFilter());
                break;
            case 14:
                customViewHolder.ivPhoto.setFilter(new GPUImageRGBFilter());
                break;
            case 15:
                customViewHolder.ivPhoto.setFilter(new GPUImageSaturationFilter());
                break;
            case 16:
                customViewHolder.ivPhoto.setFilter(new GPUImageSepiaToneFilter());
                break;
            case 17:
                customViewHolder.ivPhoto.setFilter(new GPUImageSharpenFilter());
                break;
            case 18:
                customViewHolder.ivPhoto.setFilter(new GPUImageBilateralBlurFilter());
                break;
            case 19:
                customViewHolder.ivPhoto.setFilter(new GPUImageBoxBlurFilter());
                break;
            case 20:
                customViewHolder.ivPhoto.setFilter(new GPUImageLuminanceFilter());
                break;
            case 21:
                customViewHolder.ivPhoto.setFilter(new GPUImageCGAColorspaceFilter());
                break;
            case 22:
                customViewHolder.ivPhoto.setFilter(new GPUImageContrastFilter());
                break;
            case 23:
                customViewHolder.ivPhoto.setFilter(new GPUImageCrosshatchFilter());
                break;
            case 24:
                customViewHolder.ivPhoto.setFilter(new GPUImageGaussianBlurFilter());
                break;
            case 25:
                customViewHolder.ivPhoto.setFilter(new GPUImageHalftoneFilter());
                break;
            case 26:
                customViewHolder.ivPhoto.setFilter(new GPUImageLuminanceThresholdFilter());
                break;
            case 27:
                customViewHolder.ivPhoto.setFilter(new GPUImageSolarizeFilter());
                break;
            case 28:
                customViewHolder.ivPhoto.setFilter(new GPUImageSphereRefractionFilter());
                break;
            case 29:
                customViewHolder.ivPhoto.setFilter(new GPUImageSwirlFilter());
                break;
            case 30:
                customViewHolder.ivPhoto.setFilter(new GPUImageToneCurveFilter());
                break;
            case 31:
                customViewHolder.ivPhoto.setFilter(new GPUImageBilateralBlurFilter());
                break;
            case ' ':
                customViewHolder.ivPhoto.setFilter(new GPUImageVignetteFilter());
                break;
            case '!':
                customViewHolder.ivPhoto.setFilter(new GPUImageWeakPixelInclusionFilter());
                break;
            case '\"':
                customViewHolder.ivPhoto.setFilter(new GPUImageBoxBlurFilter());
                break;
        }
        customViewHolder.bind(i, (FilterType) this.datalist.get(i), this.listener);
    }
}
