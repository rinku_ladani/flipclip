package com.india.flipclip.Home.Botttom.Home;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.india.flipclip.DataBase.DatabaseHandler;
import com.india.flipclip.R;
import com.india.flipclip.Model.Customer;
import com.india.flipclip.Util.PrefManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * ARPITA AKBARI
 **/

public class Home_Adapter extends RecyclerView.Adapter<Home_Adapter.CustomViewHolder> {
    public Context context;
    private ArrayList<Customer> dataList;
    private OnItemClickListener listener;
    PrefManager prefManager;
    private DatabaseHandler databaseHandler;
    private SQLiteDatabase sqLiteDatabase;

    class CustomViewHolder extends RecyclerView.ViewHolder {
        ImageView comment_image;
        LinearLayout comment_layout;
        TextView comment_txt;
        TextView desc_txt;
        ImageView like_image;
        LinearLayout like_layout;
        TextView like_txt;
        LinearLayout shared_layout;
        ImageView sound_image;
        TextView sound_name;
        ImageView user_pic;
        TextView username;

        public CustomViewHolder(View view) {
            super(view);
            this.username = (TextView) view.findViewById(R.id.username);
            this.user_pic = (ImageView) view.findViewById(R.id.user_pic);
            this.sound_name = (TextView) view.findViewById(R.id.sound_name);
            this.sound_image = (ImageView) view.findViewById(R.id.sound_image);
            this.like_layout = (LinearLayout) view.findViewById(R.id.like_layout);
            this.like_image = (ImageView) view.findViewById(R.id.like_image);
            this.like_txt = (TextView) view.findViewById(R.id.like_txt);
            this.desc_txt = (TextView) view.findViewById(R.id.desc_txt);
            this.comment_layout = (LinearLayout) view.findViewById(R.id.comment_layout);
            this.comment_image = (ImageView) view.findViewById(R.id.comment_image);
            this.comment_txt = (TextView) view.findViewById(R.id.comment_txt);
            this.shared_layout = (LinearLayout) view.findViewById(R.id.shared_layout);
        }

        public void bind(final int i, final Customer home_Get_Set, final OnItemClickListener onItemClickListener) {
            this.itemView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    onItemClickListener.onItemClick(i, home_Get_Set, view);
                }
            });
            this.user_pic.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    onItemClickListener.onItemClick(i, home_Get_Set, view);
                }
            });
            this.username.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    onItemClickListener.onItemClick(i, home_Get_Set, view);
                }
            });
            this.like_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    onItemClickListener.onItemClick(i, home_Get_Set, view);
                }
            });
            this.comment_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    onItemClickListener.onItemClick(i, home_Get_Set, view);
                }
            });
            this.shared_layout.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    onItemClickListener.onItemClick(i, home_Get_Set, view);
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int i, Customer home_Get_Set, View view);
    }

    public Home_Adapter(Context context2, ArrayList<Customer> arrayList, OnItemClickListener onItemClickListener) {
        this.context = context2;
        this.dataList = arrayList;
        this.listener = onItemClickListener;
    }

    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.video_item, null);
        inflate.setLayoutParams(new RecyclerView.LayoutParams(-1, -1));
        return new CustomViewHolder(inflate);
    }

    public int getItemCount() {
        return this.dataList.size();
    }


    public void onBindViewHolder(CustomViewHolder customViewHolder, int i) {
        Customer home_Get_Set = (Customer) this.dataList.get(i);
        customViewHolder.setIsRecyclable(false);


        databaseHandler = new DatabaseHandler(context);
        sqLiteDatabase = databaseHandler.getWritableDatabase();

//        Log.e("VideoADAPETR==", home_Get_Set.getVideoid());

        prefManager = new PrefManager(context);
        String videoid = home_Get_Set.getVideoid();
        if (prefManager.getUserFav().equalsIgnoreCase(videoid)) {
            customViewHolder.like_image.setTag("red");
            customViewHolder.like_image.setBackground(context.getResources().getDrawable(R.drawable.ic_likefill));
        } else {
            customViewHolder.like_image.setTag("grey");
            customViewHolder.like_image.setBackground(context.getResources().getDrawable(R.drawable.ic_like));
        }
        try {
            customViewHolder.bind(i, home_Get_Set, this.listener);
            TextView textView = customViewHolder.username;
            StringBuilder sb = new StringBuilder();
            sb.append(home_Get_Set.getUname());
            textView.setText(sb.toString());
            customViewHolder.like_txt.setText(dataList.get(i).getLikes());
            customViewHolder.comment_txt.setText(dataList.get(i).getComment());
            TextView textView2 = customViewHolder.sound_name;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("original sound ");
            textView2.setText(sb2.toString());
            customViewHolder.sound_name.setSelected(true);
            customViewHolder.desc_txt.setText(Html.fromHtml(home_Get_Set.getDescription()));
        } catch (Exception unused) {
        }

        Glide.with(context).load(home_Get_Set.getProfilePic()).placeholder(context.getDrawable(R.drawable.profile_image_placeholder)).into(customViewHolder.user_pic);

    }
}
