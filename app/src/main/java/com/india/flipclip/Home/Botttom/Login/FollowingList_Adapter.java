package com.india.flipclip.Home.Botttom.Login;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.india.flipclip.Home.Botttom.Home.Home_Adapter;
import com.india.flipclip.Home.Botttom.Home.Profile.All_userProfileActivity;
import com.india.flipclip.Model.Customer;
import com.india.flipclip.Model.Example;
import com.india.flipclip.R;
import com.india.flipclip.Rest.API;
import com.india.flipclip.Util.Common;
import com.india.flipclip.Util.PrefManager;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * ARPITA AKBARI
 **/

public class FollowingList_Adapter extends RecyclerView.Adapter<FollowingList_Adapter.MyHolder> {

    Context context;
    ArrayList<Customer> customers;
    private PrefManager prefManager;


    public FollowingList_Adapter(FollowingListActivity followingListActivity, ArrayList<Customer> following_list) {
        context = followingListActivity;
        customers = following_list;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.following_list_item, null);
        return new MyHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {

        prefManager = new PrefManager(context);
        Customer datalist = customers.get(position);
        Glide.with(context).load(datalist.getProfile()).placeholder(context.getDrawable(R.drawable.profile_image_placeholder)).into(holder.user_pic);

        holder.tv_username.setText(datalist.getUsername());

        holder.btn_following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Common.showProgressDialog(context);
                API.user().UserFollwingDelete(prefManager.getRegId(), datalist.getUserRegistationId()).enqueue(new retrofit2.Callback<Example>() {
                    @RequiresApi(api = Build.VERSION_CODES.M)
                    @Override
                    public void onResponse(Call<Example> call, Response<Example> response) {
                        Example videoResponse = response.body();
                        if (videoResponse.getSuccess() == 1) {
                            holder.btn_following.setCardBackgroundColor(context.getColor(R.color.colorPrimary));
                            holder.tv_following.setText("Following");
                            holder.tv_following.setTextColor(context.getColor(R.color.white));
                            prefManager.ClearUserFollwing();

                            holder.btn_following.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Common.showProgressDialog(context);
                                    API.user().UserFollwing(prefManager.getRegId(), datalist.getUserRegistationId()).enqueue(new retrofit2.Callback<Example>() {
                                        @Override
                                        public void onResponse(Call<Example> call, Response<Example> response) {
                                            Example videoResponse = response.body();
                                            if (videoResponse.getSuccess() == 1) {
                                                holder.btn_following.setCardBackgroundColor(context.getColor(R.color.white));
                                                holder.tv_following.setText("Follower");
                                                holder.tv_following.setTextColor(context.getColor(R.color.black));
                                                prefManager.SetFollwingUser(datalist.getUserRegistationId());
                                                Common.dismissDialog();
                                            } else {
                                                if (videoResponse.getSuccess() == 0) {
                                                    Common.dismissDialog();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<Example> call, Throwable t) {
                                            Common.dismissDialog();
                                        }
                                    });
                                }
                            });
                            Common.dismissDialog();
                        } else {
                            if (videoResponse.getSuccess() == 0) {
                                Common.dismissDialog();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Example> call, Throwable t) {
                        Common.dismissDialog();
                    }
                });
//                follwing_api_delete(datalist.getUserRegistationId(), prefManager.getRegId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return customers.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        CircleImageView user_pic;
        TextView tv_username;
        CardView btn_following;
        TextView tv_following;

        public MyHolder(@NonNull View itemView) {
            super(itemView);

            user_pic = itemView.findViewById(R.id.user_pic);
            tv_username = itemView.findViewById(R.id.tv_username);
            btn_following = itemView.findViewById(R.id.btn_following);
            tv_following = itemView.findViewById(R.id.tv_following);
        }
    }
}
