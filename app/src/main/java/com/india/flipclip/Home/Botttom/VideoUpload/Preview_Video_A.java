package com.india.flipclip.Home.Botttom.VideoUpload;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.daasuu.gpuv.composer.GPUMp4Composer;
import com.daasuu.gpuv.egl.filter.GlFilterGroup;
import com.daasuu.gpuv.player.GPUPlayerView;
import com.daasuu.gpuv.player.PlayerScaleType;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.india.flipclip.Glob;
import com.india.flipclip.R;
import com.india.flipclip.Util.MovieWrapperView;

import java.util.List;

import static com.india.flipclip.Glob.output_filter_file;

public class Preview_Video_A extends AppCompatActivity implements Player.EventListener {
    public static int select_postion;
    Filter_Adapter adapter;
    final List<FilterType> filterTypes = FilterType.createFilterList();
    GPUPlayerView gpuPlayerView;
    SimpleExoPlayer player;
    RecyclerView recylerview;
    String video_url;

    public /* synthetic */ void onIsPlayingChanged(boolean z) {
//        CC.$default$onIsPlayingChanged(this, z);
    }

    public void onLoadingChanged(boolean z) {
    }

    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
    }

    public /* synthetic */ void onPlaybackSuppressionReasonChanged(int i) {
//        CC.$default$onPlaybackSuppressionReasonChanged(this, i);
    }

    public void onPlayerError(ExoPlaybackException exoPlaybackException) {
    }

    public void onPlayerStateChanged(boolean z, int i) {
    }

    public void onPositionDiscontinuity(int i) {
    }

    public void onRepeatModeChanged(int i) {
    }

    public void onSeekProcessed() {
    }

    public void onShuffleModeEnabledChanged(boolean z) {
    }

    public /* synthetic */ void onTimelineChanged(Timeline timeline, int i) {
//        CC.$default$onTimelineChanged(this, timeline, i);
    }

    public void onTimelineChanged(Timeline timeline, @Nullable Object obj, int i) {
    }

    public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_preview__video_);
        select_postion = 0;
        video_url=getIntent().getStringExtra("path");
//        this.video_url = Glob.outputfile2;
        findViewById(R.id.Goback).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Preview_Video_A.this.finish();
//                Preview_Video_A.this.overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
            }
        });
        findViewById(R.id.next_btn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Preview_Video_A.this.Save_Video(video_url, output_filter_file);
            }
        });
        Set_Player(this.video_url);
        this.recylerview = (RecyclerView) findViewById(R.id.recylerview);
        this.adapter = new Filter_Adapter(this, this.filterTypes, new Filter_Adapter.OnItemClickListener() {
            public void onItemClick(View view, int i, FilterType filterType) {
                Preview_Video_A.select_postion = i;
                Preview_Video_A.this.gpuPlayerView.setGlFilter(FilterType.createGlFilter((FilterType) Preview_Video_A.this.filterTypes.get(i), Preview_Video_A.this.getApplicationContext()));
                Preview_Video_A.this.adapter.notifyDataSetChanged();
            }
        });
        this.recylerview.setLayoutManager(new LinearLayoutManager(this, 0, false));
        this.recylerview.setAdapter(this.adapter);
    }

    public void Set_Player(String str) {
        this.player = ExoPlayerFactory.newSimpleInstance(this, new DefaultTrackSelector());
        this.player.prepare(new ExtractorMediaSource.Factory(new DefaultDataSourceFactory((Context) this, Util.getUserAgent(this, getResources().getString(R.string.app_name)))).createMediaSource(Uri.parse(str)));
        this.player.setRepeatMode(2);
        this.player.addListener(this);
        this.player.setPlayWhenReady(true);
        this.gpuPlayerView = new GPUPlayerView(this);
        this.gpuPlayerView.setPlayerScaleType(PlayerScaleType.RESIZE_NONE);
        this.gpuPlayerView.setSimpleExoPlayer(this.player);
        this.gpuPlayerView.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        ((MovieWrapperView) findViewById(R.id.layout_movie_wrapper)).addView(this.gpuPlayerView);
        this.gpuPlayerView.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.player != null) {
            this.player.setPlayWhenReady(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.player != null) {
            this.player.setPlayWhenReady(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        if (this.player != null) {
            this.player.setPlayWhenReady(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.player != null) {
            this.player.removeListener(this);
            this.player.release();
            this.player = null;
        }
    }

    public void Save_Video(String str, String str2) {
        Log.e("DA",str);
        Log.e("DA1",str2);
        Glob.Show_determinent_loader(this, false, false);
        new GPUMp4Composer(str, str2).size(540, 960).videoBitrate(2073600).filter(new GlFilterGroup(FilterType.createGlFilter((FilterType) this.filterTypes.get(select_postion), getApplicationContext()))).listener(new GPUMp4Composer.Listener() {
            public void onProgress(double d) {
                StringBuilder sb = new StringBuilder();
                sb.append("");
                int i = (int) (d * 100.0d);
                sb.append(i);
                Log.d("resp", sb.toString());
                Glob.Show_loading_progress(i);
            }

            public void onCompleted() {
                Preview_Video_A.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Glob.cancel_determinent_loader();
                        Preview_Video_A.this.GotopostScreen(output_filter_file);
                    }
                });
            }

            public void onCanceled() {
                Log.d("resp", "onCanceled");
            }

            public void onFailed(Exception exc) {
                Log.d("resp", exc.toString());
                Preview_Video_A.this.runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            Glob.cancel_determinent_loader();
                            Toast.makeText(Preview_Video_A.this, "Try Again", 0).show();
                        } catch (Exception unused) {
                        }
                    }
                });
            }
        }).start();
    }

    public void GotopostScreen(String output_filter_file) {
        Log.e("NEXT",output_filter_file);
        Intent intent=new Intent(getApplicationContext(), VideoPostingActivity.class);
        intent.putExtra("path1",output_filter_file);
        startActivity(intent);
//        startActivity(new Intent(this, Post_Video_A.class));
//        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
    }

    public void onBackPressed() {
        finish();
//        overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
    }
}