package com.india.flipclip.Home.Botttom.Home.Profile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.india.flipclip.Home.Botttom.Login.FollowingListActivity;
import com.india.flipclip.Home.Botttom.Login.FollowingList_Adapter;
import com.india.flipclip.Model.Customer;
import com.india.flipclip.Model.Example;
import com.india.flipclip.R;
import com.india.flipclip.Rest.API;
import com.india.flipclip.Util.Common;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

public class Followers_ListActivity extends AppCompatActivity {

    String userid;
    String username;
    TextView tv_username;
    ImageView img_back;
    RecyclerView rcy_followinglist;
    RelativeLayout no_data_layout;
    Followers_listAdapter followingList_adapter;
    private LinearLayoutManager layoutManager;
    ArrayList<Customer> following_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_followers__list);

        username = getIntent().getStringExtra("username");
        userid = getIntent().getStringExtra("userid");

        img_back = findViewById(R.id.img_back);
        tv_username = findViewById(R.id.tv_username);
        rcy_followinglist = findViewById(R.id.rcy_followinglist);
        no_data_layout = findViewById(R.id.no_data_layout);
        rcy_followinglist.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rcy_followinglist.setLayoutManager(layoutManager);

        tv_username.setText(username);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        getFollowingUserList(userid);
    }


    private void getFollowingUserList(String user_id) {
        Common.showProgressDialog(this);
        API.user().getUserFollowwersList(user_id).enqueue(new retrofit2.Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                Example videoResponse = response.body();
                if (videoResponse.getSuccess() == 1) {
                    following_list = videoResponse.getCustomer();
                    no_data_layout.setVisibility(View.GONE);
                    rcy_followinglist.setVisibility(View.VISIBLE);

                    Log.e("WWW", String.valueOf(following_list.size()));
                    followingList_adapter = new Followers_listAdapter(Followers_ListActivity.this, following_list, username);
                    rcy_followinglist.setAdapter(followingList_adapter);

                    Common.dismissDialog();
                } else {
                    if (videoResponse.getSuccess() == 0) {
                        no_data_layout.setVisibility(View.VISIBLE);
                        rcy_followinglist.setVisibility(View.GONE);
                        Common.dismissDialog();
                    }
                }
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
//                Common.showToast(getActivity(), "Password does not match");
                Common.dismissDialog();
            }
        });

    }

}


