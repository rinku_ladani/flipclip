package com.india.flipclip.Home.Botttom.Login;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.Wave;
import com.india.flipclip.Glob;
import com.india.flipclip.R;
import com.india.flipclip.Util.AndroidMultiPartEntity;
import com.india.flipclip.Util.Common;
import com.india.flipclip.Util.PrefManager;
import com.india.flipclip.Util.VolleyMultipartRequest;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileEditActivity extends AppCompatActivity {

    private static final int RESULT_LOAD_IMAGE = 101;
    private static final String TAG = "AKABRi";
    PrefManager prefManager;
    String user_id;
    String username;
    String emailid;
    String password;
    String gender;
    String birthdate;
    String finaluserpic;

    String currentDate = "";
    TextView tv_changeprofile;
    CircleImageView img_profile;
    EditText etFirstName;
    RadioButton btn_male;
    RadioButton btn_female;
    EditText etEmail;
    EditText etDob;
    private RadioGroup sg_gender;
    ImageView img_back;
    ImageView img_save;
    private ProgressBar progress_bar1;
    private Dialog dialog;
    private int mYear, mMonth, mDay;
    private DatePickerDialog datePickerDialog;
    private String imgprofile = "";
    private long totalSize = 0;
    private String finauid;
    private String finaluname;
    private String finalemail;
    private String finalpass;
    private String finalgender;
    private String finalbirth;
    private String finalupic;
    //    private UploadUsereditProfile uploadUsereditProfile;
    private String currentDatepss;
    private Bitmap bitmap;
    private Bitmap finalupic1;
    private Bitmap bitmap1;
    private byte[] theByteArray;


    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);

        prefManager = new PrefManager(this);

        user_id = getIntent().getStringExtra("user_id");
        username = getIntent().getStringExtra("username");
        emailid = getIntent().getStringExtra("emailid");
        password = getIntent().getStringExtra("password");
        gender = getIntent().getStringExtra("gender");
        birthdate = getIntent().getStringExtra("birthdate");
        finaluserpic = getIntent().getStringExtra("finaluserpic");


        Log.e("sdsdsds", user_id);
        Log.e("s", username);
        Log.e("s", emailid);
        Log.e("s", password);
        Log.e("s", gender);
        Log.e("s", birthdate);
        Log.e("s", finaluserpic);

        img_save = findViewById(R.id.img_save);
        img_back = findViewById(R.id.img_back);
        tv_changeprofile = findViewById(R.id.tv_changeprofile);
        img_profile = findViewById(R.id.img_profile);
        etFirstName = findViewById(R.id.etFirstName);
        btn_male = findViewById(R.id.btn_male);
        btn_female = findViewById(R.id.btn_female);
        etEmail = findViewById(R.id.etEmail);
        etDob = findViewById(R.id.etDob);
        sg_gender = (RadioGroup) findViewById(R.id.sg_gender);

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.video_upload_progress);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawableResource(17170445);

        progress_bar1 = dialog.findViewById(R.id.progress_bar1);
        Sprite doubleBounce = new Wave();
        progress_bar1.setIndeterminateDrawable(doubleBounce);


        Picasso.get().load(finaluserpic).into(img_profile);

        etFirstName.setText(username);
        etEmail.setText(emailid);
        etDob.setText(birthdate);


        tv_changeprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        sg_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId) {
                    case R.id.btn_male:
                        gender = "Male";
//                        Common.showToast (context, "Male");
                        break;
                    case R.id.btn_female:
                        gender = "Female";
//                        Common.showToast (context, "Female");
                        break;
                    default:
                        // Nothing to do
                }
            }
        });


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        etDob.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                etDob.setRawInputType(InputType.TYPE_NULL);
                etDob.setFocusable(true);
                if (b)
                    selDate();
            }
        });


        etDob.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
            @Override
            public void onClick(View view) {
                etDob.setRawInputType(InputType.TYPE_NULL);
                etDob.setFocusable(true);
                selDate();
            }
        });

        img_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String username = etFirstName.getText().toString();
                String email = etEmail.getText().toString();
                String birth = etDob.getText().toString();
                if (checkValidations()) {
                    if (Common.isConnectingToInternet(getApplicationContext())) {
                        if (imgprofile.isEmpty()) {
                            profileupdate(user_id, username, email, password, gender, birth, finaluserpic);
                        } else {
                            profileupdate1(user_id, username, email, password, gender, birth, bitmap);
                        }
                    } else {
                        Common.showToast(getApplicationContext(), getResources().getString(R.string.no_internet_available));
                    }
                }

            }
        });


    }

    private void profileupdate1(String user_id, String username, String email, String password, String gender, String birth, Bitmap finaluserpic) {
        Log.e("djdjdksdjs", user_id);
        Log.e("1", username);
        Log.e("1", email);
        Log.e("1", password);
        Log.e("1", gender);
        Log.e("1", birth);
        Log.e("1", String.valueOf(finaluserpic));

        finauid = user_id;
        finaluname = username;
        finalemail = email;
        finalpass = password;
        finalgender = gender;
        finalbirth = birth;
        finalupic1 = finaluserpic;


        Common.showProgressDialog(this);
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, Glob.USERPROFILEDIT_UPLOAD_URL,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        Common.dismissDialog();
                        try {
                            JSONObject obj = new JSONObject(new String(response.data));
                            Log.e("DDDDD", obj.toString());
                            prefManager.clearUserDetail();
                            prefManager.setUserData(finaluname, finalgender,
                                    finalemail, prefManager.getRegId(), finalbirth, finalpass, imgprofile);
                            finish();
//                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Common.dismissDialog();
//                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                entity.addPart("id", new StringBody(finauid));
//                entity.addPart("username", new StringBody(finaluname));
//                entity.addPart("emailid", new StringBody(finalemail));
//                entity.addPart("password", new StringBody(finalpass));
//                entity.addPart("gender", new StringBody(finalgender));
//                entity.addPart("dob", new StringBody(finalbirth));
//                entity.addPart("uploaded_file", new FileBody(sourceFile));
//
                params.put("id", finauid);
                params.put("username", finaluname);
                params.put("emailid", finalemail);
                params.put("password", finalpass);
                params.put("gender", finalgender);
                params.put("dob", finalbirth);
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                Log.e("VALUE+++", String.valueOf(finalupic1));
                params.put("uploaded_file", new DataPart(imagename + ".png", getFileDataFromDrawable(finalupic1)));
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);

        requestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {

                Log.e("WQQQ", request.toString());
                Common.dismissDialog();


            }
        });

//        uploadUsereditProfile = new UploadUsereditProfile();
//        uploadUsereditProfile.execute();


    }

    private void profileupdate(String user_id, String username, String email, String password, String gender, String birth, String finaluserpic) {
        Log.e("uid", user_id);
        Log.e("1", username);
        Log.e("1", email);
        Log.e("1", password);
        Log.e("1", gender);
        Log.e("1", birth);
        Log.e("1", finaluserpic);

        finauid = user_id;
        finaluname = username;
        finalemail = email;
        finalpass = password;
        finalgender = gender;
        finalbirth = birth;
        finalupic = finaluserpic;

        byte[] encodeByte = Base64.decode(finalupic, Base64.DEFAULT);

        theByteArray = finalupic.getBytes();

        Bitmap bitmap = StringToBitMap(finalupic);

        Log.e("ARPI", String.valueOf(theByteArray.toString()));
        Log.e("APPU", String.valueOf(theByteArray.length));

        Common.showProgressDialog(this);
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, Glob.USERPROFILEDIT_UPLOAD_URL,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        Common.dismissDialog();
                        try {
                            JSONObject obj = new JSONObject(new String(response.data));
                            Log.e("DDDDD", obj.toString());
                            prefManager.clearUserDetail();
                            prefManager.setUserData(finaluname, finalgender,
                                    finalemail, prefManager.getRegId(), finalbirth, finalpass, finalupic);
                            finish();

//                            Toast.makeText(getApplicationContext(), obj.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Common.dismissDialog();
//                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
//                entity.addPart("id", new StringBody(finauid));
//                entity.addPart("username", new StringBody(finaluname));
//                entity.addPart("emailid", new StringBody(finalemail));
//                entity.addPart("password", new StringBody(finalpass));
//                entity.addPart("gender", new StringBody(finalgender));
//                entity.addPart("dob", new StringBody(finalbirth));
//                entity.addPart("uploaded_file", new FileBody(sourceFile));
//
                params.put("id", finauid);
                params.put("username", finaluname);
                params.put("emailid", finalemail);
                params.put("password", finalpass);
                params.put("gender", finalgender);
                params.put("dob", finalbirth);
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();

                File file = new File(finalupic);

                Log.e("NAME", file.getName());
                params.put("uploaded_file", new DataPart(file.getName(), theByteArray));
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);

        requestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<String>() {
            @Override
            public void onRequestFinished(Request<String> request) {

                Log.e("WQQQ", request.toString());
                Common.dismissDialog();


            }
        });


    }

//    public Bitmap StringToBitMap(String encodedString) {
//        try {
//            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
//            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
//            return bitmap;
//        } catch (Exception e) {
//            e.getMessage();
//            return null;
//        }
//    }

    public Bitmap StringToBitMap(String image) {
        try {
            byte[] encodeByte = Base64.decode(image, Base64.URL_SAFE);

            InputStream inputStream = new ByteArrayInputStream(encodeByte);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }


    private void selDate() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        String month = null, date;
                        if (monthOfYear < 10) {
                            month = "0" + (monthOfYear + 1);
                        } else {
                            month = "" + (monthOfYear + 1);
                        }
                        if (dayOfMonth < 10) {
                            date = "0" + dayOfMonth;
                        } else {
                            date = "" + dayOfMonth;
                        }
                        currentDatepss = year + "-" + month + "-" + date;
                        currentDate = year + "-" + month + "-" + date;
                        etDob.setText(currentDate);
                        Log.e("DATE", "onDateSet: " + etDob.getText().toString().trim());
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }


    private boolean checkValidations() {

//        if (bitmap == null) {
//            etFirstName.requestFocus();
//            Common.showToast(ProfileEditActivity.this, "Please Profile Upload");
//            return false;
//        } else if (!Common.isValidName(etFirstName.getText().toString())) {
//            Common.showToast(ProfileEditActivity.this, "Something Wrong");
//            return false;
//        }

        if (!Common.isNotNullEditTextBox(etFirstName)) {
            etFirstName.requestFocus();
            Common.showToast(ProfileEditActivity.this, "Please Enter Users Name");
            return false;
        } else if (!Common.isValidName(etFirstName.getText().toString())) {
            Common.showToast(ProfileEditActivity.this, "First Name not allow space");
            return false;
        }

       /* if (!Common.isNotNullEditTextBox (etDob)) {
            etDob.requestFocus ();
            Common.showToast (context, "Please Select DOB");
            return false;
        } else if (!Common.isValidName (dobInput)) {
            Common.showToast (context, "Please Date of Birth");
            return false;
        }*/

        if (!Common.isNotNullEditTextBox(etEmail)) {
            etEmail.requestFocus();
            Common.showToast(ProfileEditActivity.this, "Please Enter EmailId");
            return false;
        } else if (!Common.isValidateEmail(etEmail.getText().toString())) {
            Common.showToast(ProfileEditActivity.this, "Please Insert Valid EmailId");
            return false;
        }

//        if (!Common.isNotNullEditTextBox(etPassword)) {
//            etPassword.requestFocus();
//            Common.showToast(ProfileEditActivity.this, "Please Enter Password");
//            return false;
//        } else {
//            if (etPassword.getText().toString().trim().length() != 8 && etPassword.getText().toString().trim().length() < 8) {
//                etPassword.requestFocus();
//                Common.showToast(context, "You must have 8 characters in your password");
//                return false;
//            }
//        }

//        if (!Common.isNotNullEditTextBox(etConfPassword)) {
//            etConfPassword.requestFocus();
//            Common.showToast(context, "Please Enter Confirm Password");
//            return false;
//        } else {
//            if (!etPassword.getText().toString().trim().equals(etConfPassword.getText().toString().trim())) {
//                etConfPassword.requestFocus();
//                Common.showToast(context, "password and confirm password must be same!!!");
//                return false;
//            }
//        }

//        if (img == null) {
//            Toast.makeText(context, "Please Profile Pic Upload", Toast.LENGTH_SHORT).show();
//            return false;
//        }

        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri imageUri = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                img_profile.setImageBitmap(bitmap);

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(imageUri, filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imgprofile = cursor.getString(columnIndex);
                cursor.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private class UploadUsereditProfile extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e("DDD1", "1");
//            progres_bar.setVisibility(View.VISIBLE);
            progress_bar1.setProgress(0);
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            progress_bar1.setProgress(progress[0]);
            Log.e("DDD1", String.valueOf(progress[0]));
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        private String uploadFile() {
            Log.e("DDD1", "3");
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Glob.USERPROFILEDIT_UPLOAD_URL);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
//                                Common.showProgressDialog(Post_Video_A.this);
                                progress_bar1.setProgress((int) ((num / (float) totalSize) * 100));
//                                tv_num.setText(String.valueOf((int) ((num / (float) totalSize) * 100)));
                                Log.e("FILE", String.valueOf((int) ((num / (float) totalSize) * 100)));
                            }
                        });

                File sourceFile = new File(finalupic);
//                prefManager.setUserData(etUserName.getText().toString().trim(),
//                        gender,
//                        etEmail.getText().toString().trim(),
//                        "", etDob.getText().toString().trim(),
//                        etPassword.getText().toString().trim());

                // Adding file data to http body
                entity.addPart("id", new StringBody(finauid));
                entity.addPart("username", new StringBody(finaluname));
                entity.addPart("emailid", new StringBody(finalemail));
                entity.addPart("password", new StringBody(finalpass));
                entity.addPart("gender", new StringBody(finalgender));
                entity.addPart("dob", new StringBody(finalbirth));
                entity.addPart("uploaded_file", new FileBody(sourceFile));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e(TAG, "Response from server: " + result);

            prefManager.clearUserDetail();
            prefManager.setUserData(finaluname, finalgender,
                    finalemail, "", finalbirth, finalpass, finalupic);
//            if (folder.isDirectory()) {
//                String[] children = folder.list();
//                for (int i = 0; i < children.length; i++) {
//                    new File(folder, children[i]).delete();
//                }
//            }
            finish();
            // showing the server response in an alert dialog
//            showAlert(result);
            dialog.dismiss();
            super.onPostExecute(result);
//            Common.dismissDialog();
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        if (uploadUsereditProfile != null) {
//            Toast.makeText(this, "Please Wait Profile UpDate", Toast.LENGTH_SHORT).show();
//        } else {
//            finish();
//        }
    }
}
