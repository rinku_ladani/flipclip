package com.india.flipclip.BackPress;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

/**
 * ARPITA AKBARI
 **/

public class BackPressImplimentation implements OnBackPressListener {
    private Fragment parentFragment;

    public BackPressImplimentation(Fragment fragment) {
        this.parentFragment = fragment;
    }

    public boolean onBackPressed() {
        if (this.parentFragment == null || this.parentFragment.getChildFragmentManager().getBackStackEntryCount() == 0) {
            return false;
        }
        FragmentManager childFragmentManager = this.parentFragment.getChildFragmentManager();
        if (!((OnBackPressListener) childFragmentManager.getFragments().get(0)).onBackPressed()) {
            childFragmentManager.popBackStackImmediate();
        }
        return true;
    }
}
