package com.india.flipclip.BackPress;

/**
 * ARPITA AKBARI
 **/

public interface OnBackPressListener {
    boolean onBackPressed();

}
