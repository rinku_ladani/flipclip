//package com.india.flipclip;
//
//import androidx.appcompat.app.AppCompatActivity;
//
//import android.os.Bundle;
//import android.widget.Toast;
//
//import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
//import com.india.flipclip.Home.MainMenuFragment;
//
//public class StartActivity extends AppCompatActivity {
//
//    private MainMenuFragment mainMenuFragment;
//    long mBackPressed;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_start);
//
//        this.mainMenuFragment = new MainMenuFragment();
//        getSupportFragmentManager().beginTransaction().replace(R.id.container, this.mainMenuFragment).commit();
//    }
//
//    public void onBackPressed() {
//        if (!this.mainMenuFragment.onBackPressed()) {
//            if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
//                super.onBackPressed();
//            } else if (this.mBackPressed + AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS > System.currentTimeMillis()) {
//                super.onBackPressed();
//            } else {
//                Toast.makeText(getBaseContext(), "Tap Again To Exit", 0).show();
//                this.mBackPressed = System.currentTimeMillis();
//            }
//        }
//    }
//}
